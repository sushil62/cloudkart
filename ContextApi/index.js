import React, { Component, createContext } from 'react'
import * as Font from 'expo-font';
import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage';
import _ from "underscore";
import { YellowBox, ToastAndroid } from 'react-native';

export const CKart = createContext()

export class CKartProvider extends Component {
    state = {
        login_username: '',
        login_password: '',
        email: "",
        password: '',
        signInToken: null,
        signInError: true,
        offerData: {},
        vendor_list: [],
        brand_list: [],
        cat_list: [],
        product_list: [],
        page_details: {},
        facetednav: {},
        product_count: null,
        previous_url: null,
        next_url: null,
        select_mode: 'offers',
        t_filter: null,
        page: 1,
        t_filter_p: null,
        scrollMenuSelection: '',
        select_cat: null,
        toggle_dropdown: true,
        product_dtl_data: {},
        product_offers_rec: [],
        cart_data: {},
        addressListData: [],
        addressSaved: false,
        addressType: null,
        set_default_address: false,
        filterId: null,
        // filterValue: null,
        wishlist_list: [],
        order_list: [],
        filterValue: [],
        filterType: '',
        filterData: {},
        quantity: 1,
        orderDetails: true,
        viewItems: false,
        vp_id: null,
        qty: null,
        search_query: '',
        suggestions: [],
        modalVisible: false,
        bottomModal: null,
        base_url:"http://142bdc3a62a8.ngrok.io",
        captch_text: '',
        captcha_input: '',
        userdetail_data: {},
        review_rating: 0,
        signin_error: {},
        currency: '',
        loading: false,
        delivery_option: null,
        delivery_choice: null,
        delivery_slot: null,
        address_type: '', address_1: '', address_2: '', pincode: '', city: '', country: '', name: '', phone_no: '',
        itemQty: {},
        valid_postcode: '',
        // base_url:"http://142bdc3a62a8.ngrok.io"
    }

    getItmQty = (name) => {
        const qty = this.state.itemQty[name]
        if (qty) {
            return qty
        } else {
            return null
        }
    }
    incItmQty = (p_slug, v_id) => {
        let qty = this.state.itemQty[p_slug]
        let itemQty = this.state.itemQty
        if (qty) {
            if (qty < 10) {
                itemQty[p_slug] = qty + 1
                this.quantityValidation(v_id, p_slug, itemQty)
            }
        } else {
            itemQty[p_slug] = 2
            this.quantityValidation(v_id, p_slug, itemQty)

        }
    }
    decItmQty = (p_slug, v_id) => {
        let qty = this.state.itemQty[p_slug]
        let itemQty = this.state.itemQty
        if (qty && qty > 1) {
            itemQty[p_slug] = qty - 1
            this.quantityValidation(v_id, p_slug, itemQty)
        }
    }


    quantityValidation = async (v_id, product_slug, itemQty) => {
        var p_data = {
            "vendor_id": v_id,
            "product_slug": product_slug,
            "quantity": itemQty[product_slug],
        }
        console.log(p_data)
        await axios.post(`${this.state.base_url}/api/replace_cart_item/`, p_data, {
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((res) => {
                if (res.status === 201) {
                    this.setState({ itemQty: itemQty })
                } else {
                    ToastAndroid.show('Product Is Not Serviceable To This Postcode.', ToastAndroid.SHORT);
                }
            })
            .catch((error) => {
                ToastAndroid.show('Product Is Not Serviceable To This Postcode.', ToastAndroid.SHORT);
                // console.error("error validate", error)
            })

    }

    resetInvalidCredential = (val) => {
        this.setState({ invalidCredential: val })
    }

    bottomModalFunc = (value) => {
        this.setState({
            bottomModal: value
        })

    }
    loading_true = (value) => {
        this.setState({
            loading: true,
        })

    }
    loading_false = (value) => {
        this.setState({
            loading: false,
        })

    }
    LogoutHandle = () => {
        this.setState({
            login_username: '',
            login_password: '',
            signInToken: null,
            wishlist_list: [],
            order_list: [],
        })

    }

    delivery_option_change = (value) => {
        this.setState({
            delivery_option: value,
        })

    }
    delivery_choice_change = (value) => {
        this.setState({
            delivery_choice: value,
            delivery_slot: null,
        })

    }
    delivery_slot_change = (value) => {
        this.setState({
            delivery_slot: value,
            delivery_choice: null,
        })

    }
    captcha = () => {
        var alpha = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        var i;
        for (i = 0; i < 6; i++) {
            var a = alpha[Math.floor(Math.random() * alpha.length)];
            var b = alpha[Math.floor(Math.random() * alpha.length)];
            var c = alpha[Math.floor(Math.random() * alpha.length)];
            var d = alpha[Math.floor(Math.random() * alpha.length)];
            var e = alpha[Math.floor(Math.random() * alpha.length)];
            var f = alpha[Math.floor(Math.random() * alpha.length)];

        }
        var code = a + ' ' + b + ' ' + c + ' ' + d + ' ' + e + ' ' + f;
        this.setState({ captch_text: code })
        // return code
    }

    validCaptcha = (captch_text, captcha_input) => {
        var string1 = this.removeSpaces(captch_text);
        var string2 = this.removeSpaces(captcha_input);
        // console.log(string1, "dscsvsvs", string2)
        if (string1 == string2) {
            return true;
        } else {
            return false;
        }
    }
    removeSpaces = (string) => {
        return string.split(' ').join('');
    }

    clearData = () => {
        // console.log("dslvnsldkvnsv")
        // this.setState(
        //     initialState
        // )
        this.initialFunc()
    }



    setModalVisible = (value) => {
        this.setState({ modalVisible: value })
    }


    setCheckoutData = (vp_id, qty) => {
        this.setState({ vp_id: vp_id, qty: qty })
    }

    addToCartQty = () => {
        const { product_dtl_data } = this.state
        const product_id = product_dtl_data.vendor_product.id
    }

    quantity_change = (value) => {
        var { product_dtl_data } = this.state
        product_dtl_data.quantity = product_dtl_data.quantity + value
        this.setState({ product_dtl_data: product_dtl_data })

    }

    fatchFilteredDataApi = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        var url = `${this.state.base_url}` + '/api/search/?exf';
        var temp = url;
        const filterData = this.state.filterData
        // console.log(filterData, "filterData")

        if (filterData === {}) {
            url = url + '&ref_ct_so_ord=ven_rnk_srt';
        }
        if (filterData.price_range_facet) {
            for (let i = 0; i < filterData.price_range_facet.length; i++) {
                const element = filterData.price_range_facet[i];
                // console.log(element, "element")
                url = url + '&min-price=' + element['max-price'] + '&max-price=' + element['min-price'];
                // console.log(url, "priceRange")
            }
        }
        if (filterData.category) {
            for (let i = 0; i < filterData.category.length; i++) {
                const element = filterData.category[i];
                url = url + '&' + element.url.key + '=' + element.url.value
            }
        }
        if (filterData.brand) {
            for (let i = 0; i < filterData.brand.length; i++) {
                const element = filterData.brand[i];
                url = url + '&' + element.url.key + '=' + element.url.value
            }
        }
        if (filterData.vendor) {
            for (let i = 0; i < filterData.vendor.length; i++) {
                const element = filterData.vendor[i];
                url = url + '&' + element.url.key + '=' + element.url.value
            }
        }
        if (filterData.availability_facet) {
            for (let i = 0; i < filterData.availability_facet.length; i++) {
                const element = filterData.availability_facet[i];
                url = url + '&' + element.url.key + '=' + element.url.value
            }
        }
        if (filterData.discount_percent_facet) {
            for (let i = 0; i < filterData.discount_percent_facet.length; i++) {
                const element = filterData.discount_percent_facet[i];
                url = url + '&' + element.url.key + '=' + element.url.value
            }
        }
        if (filterData.review_rating_facet) {
            for (let i = 0; i < filterData.review_rating_facet.length; i++) {
                const element = filterData.review_rating_facet[i];
                url = url + '&' + element.url.key + '=' + element.url.value
            }
        }
        if (filterData.sort_facet) {
            for (let i = 0; i < filterData.sort_facet.length; i++) {
                const element = filterData.sort_facet[i];
                url = url + '&' + element.url.key + '=' + element.url.value
            }
        }
        axios.get(url, {
            headers: {
                'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data, "filterData")
                this.setState({
                    product_list: res.data.results.products,
                    facetednav: res.data.results.facetednav,
                    product_count: res.data.count,
                    previous_url: res.data.previous,
                    next_url: res.data.next,
                })
                // this.bottomModalFunc(null)                
                this.loading_false()                
            })
            .catch((error) => {
                console.error("error ")
            })
    }

    resetFilter = () => {
        this.setState({ filterData: {} })
        this.fatchFilteredDataApi()
    }
    resetSortFilter = (filterType) => {
        var filterData = this.state.filterData
        filterData[filterType] =[]
        this.setState({ filterData: filterData,"sort_text": null })
        // this.setState({ filterData: {},"sort_text": null })
        this.fatchFilteredDataApi()
    }

    setFilterData = (filterType, val) => {

        var filterData = this.state.filterData
        var temp = filterData[filterType]
        if (temp && temp.includes(val)) {
            temp.remove(val)
        } else if (temp) {
            temp.push(val)
        } else {
            temp = [val]
        }
        filterData[filterType] = temp
        // console.log(filterType,val, "dakdcsdkcndskcn")

        this.setState({ filterData: filterData })
    }

    setSortFilterData = (filterType, value,text) => {
        
        var val = _.find(value, (v) => {
            return v.text == text;
        }); 
        // if (true) {}       

        var filterData = this.state.filterData
        var temp = filterData[filterType]
        if (temp && temp.includes(val)) {
            temp.remove(val)
        } else if (temp) {
            temp.push(val)
        } else {
            temp = [val]
        }
        filterData[filterType] = temp
        // console.log(filterType,val, "dakdcsdkcndskcn")

        this.setState({ filterData: filterData ,"sort_text":text})
    }

    filterDataFunc = (id, value, filterType) => {
        this.setState({
            filterId: id,
            filterValue: value,
            filterType: filterType
        })
    }

    chengeSave = () => {
        this.setState({ addressSaved: false })
    }

    scrollfunc = (value) => {
        this.setState({ scrollMenuSelection: value });

    }

    inc_qty = (itm_id) => {
        var cart = this.state.cart_data;


        this.state.cart_data.cart_items.map((itm) => {
            if (itm.id == itm_id) {
                itm.quantity += 1;
            }
        });
        this.setState({ cart_data: cart }, () => {
            this.updateCart();
        });
    };
    remove_cart_item = (itm_id) => {
        var cart = this.state.cart_data;


        this.state.cart_data.cart_items.map((itm) => {
            if (itm.id == itm_id) {
                itm.quantity = 0;
            }
        });

        this.setState({ cart_data: cart }, () => {
            this.updateCart();
        });
    };
    dec_qty = (itm_id) => {
        var cart = this.state.cart_data;


        this.state.cart_data.cart_items.map((itm) => {
            if (itm.id == itm_id) {
                if (itm.quantity > 1) {
                    itm.quantity -= 1;
                }
            }
        });

        this.setState({ cart_data: cart }, () => {
            this.updateCart();
        });
    };

    addToCartValidation = async (v_id, vp_id, product_slug, qty) => {
        var p_data = {
            "vendor_id": v_id,
            "product_slug": product_slug,
            "quantity": qty,
            "postcode": this.state.postcode
        }
        // console.log(p_data)
        await axios.post(`${this.state.base_url}/api/add/cart/`, p_data, {
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((res) => {
                if (res.status === 201) {
                    this.addToCart(vp_id, qty)
                }
            })
            .catch((error) => {
                ToastAndroid.show('Product Is Not Serviceable To This Postcode.', ToastAndroid.SHORT);
                // console.error("error validate", error)
            })

    }

    addToCart = async (vp_id, qty) => {
        var cart = this.state.cart_data;
        var itms = cart.cart_items.map((itm) => {
            return { quantity: itm.quantity, vendor_product: itm.vendor_product.id };
        });
        var cart_itm = _.find(itms, (itm) => {
            return itm.vendor_product == vp_id;
        });
        // var cart_itm = _.find(itms, (itm) => itm.vendor_product.id == vp_id);
        // console.log(cart_itm, 'cart_itm');
        if (cart_itm) {
            cart_itm.quantity += qty;
        } else {
            itms.push({ quantity: qty, vendor_product: vp_id });
        }
        var coupon_code = '';
        var update_cart = {};
        var temp = {};
        temp['coupon_code'] = coupon_code;
        temp['items'] = itms;
        update_cart['cart'] = temp;
        // console.log(update_cart,"iiiiiiiiiiiiiiiiiiiiiiiiiiii")
        await axios.post(`${this.state.base_url}/api/cart/`, update_cart, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            },

        })
            .then((res) => {
                // console.log(res.data,"update_cart")

                this.setState({
                    cart_data: res.data
                })
                ToastAndroid.show('Item added to Cart.', ToastAndroid.SHORT);
            })
            .catch((error) => {
                console.error("error update_cart")
            })


    };

    updateCart = async () => {
        // const signToken = await AsyncStorage.getItem('signToken')

        var itms = this.state.cart_data.cart_items.map((itm) => {
            return { quantity: itm.quantity, vendor_product: itm.vendor_product.id };
        });
        var coupon_code = '';
        var update_cart = {};
        var temp = {};
        temp['coupon_code'] = coupon_code;
        temp['items'] = itms;
        update_cart['cart'] = temp;


        axios.post(`${this.state.base_url}/api/cart/`, update_cart, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            },

        })
            .then((res) => {
                // console.log(res.data,"update_cart")
                this.setState({
                    cart_data: res.data
                })
                this.loading_false()

            })
            .catch((error) => {
                console.error("error update_cart")
            })

    };


    cartListApi = async () => {
        // const signToken = await AsyncStorage.getItem('signToken')
        var  cart_id = ''
        axios.get(`${this.state.base_url}/api/cart/`, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data, "cartListApi")
                this.setState({
                    cart_data: res.data
                })
                // if (res.data.cart_id !== '') {
                //     cart_id = res.data.cart_id
                // }
            })
            .catch((error) => {
                console.error("error cartListApi")
            })
        //     console.log("cartListApi",cart_id)
        // await AsyncStorage.setItem('cart_id', cart_id)
    }

    buyNow = async () => {
        var itms = this.state.cart_data.cart_items.map((itm) => {
            return { quantity: itm.quantity, vendor_product: itm.vendor_product.id };
        });
        var cart_itm = _.find(itms, (itm) => {
            return itm.vendor_product === this.state.vp_id;
        });
        // var cart_itm = _.find(itms, (itm) => itm.vendor_product.id == this.state.vp_id);
        // console.log(cart_itm, 'cart_itm');
        if (cart_itm) {
            cart_itm.quantity += 1;
        } else {
            itms.push({ quantity: this.state.qty, vendor_product: this.state.vp_id });
        }

        var coupon_code = this.state.coupon_code;
        var update_cart = {};
        var temp = {};
        // temp['coupon_code'] = coupon_code;
        temp['items'] = itms;
        // update_cart['cart'] = temp;

        // console.log('update_cart', update_cart, temp);
        // console.log('loading products', token);
        const signToken = await AsyncStorage.getItem('signToken')

        axios.post(`${this.state.base_url}/api/buy_now/`, temp, {
            headers: {
                'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res,"cartListApi")
                // this.setState({
                //     cart_data: res.data
                // })
                // console.log(res, "sjfvnfslvnl=====")
            })
            .catch((error) => {
                console.error("error buy now", error)
            })

        // fetch(`${this.satate.base_url}/api/buy_now/`, {
        //     method: 'POST',
        //     headers: {
        //         'Content-Type': 'application/json',
        //         Authorization: `Token ${token}`,
        //     },

        //     body: JSON.stringify(temp),
        // })
        //     .then((response) => response.json())
        //     .then((data) => {
        //         console.log('buynow response :', data);
        //         if (!_.has(data, 'status_code')) {
        //             var buy_now = this.state.buy_now;
        //             buy_now = {};
        //             this.setState({ buynow: data });
        //             this.props.history.push('/checkout/');
        //             //
        //             // show toast message
        //             // Alert.alert(`${vp_id} added to cart successfully !`);
        //             // console.log(`${vp_id} added to cart successfully !`);

        //             succ_cb();
        //         } else {
        //             console.log('error :', data);
        //         }
        //     })
        //     .catch((error) => {
        //         console.error('Error:', error);
        //         err_cb();
        //     });

    }

    offerApi = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        // console.log(signToken, "signToken")
        axios.get(`${this.state.base_url}/api/offers/`, {
            // headers: {
            //     'Authorization': `Token ${signToken}`
            // }
        })
            .then((res) => {
                // console.log(res,"offerApi")
                this.setState({
                    offerData: res.data
                })

            })
            .catch((error) => {
                console.error("error offerApi")
            })
    }

    currencyApi = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        // console.log(signToken, "signToken")
        axios.get(`${this.state.base_url}/api/currency/`, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res,"offerApi")
                this.setState({
                    currency: res.data.currency
                })

            })
            .catch((error) => {
                console.error("error currencyApi")
            })
    }
    checkoutGetApi = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        // console.log(signToken, "signToken")
        axios.get(`${this.state.base_url}/api/checkout/`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data, "checkoutGetApi")
                this.setState({
                    delivery_options: res.data.delivery_options
                })

            })
            .catch((error) => {
                console.error("error checkoutGetApi", error)
            })
    }

    wishlistApi = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        axios.get(`${this.state.base_url}/api/wishlist/`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res,"wishlistApi")
                if (res.data.rows.length > 0) {
                    this.setState({
                        wishlist_list: res.data.rows
                    })
                }
            })
            .catch((error) => {
                console.error("error wishlistApi")
            })
    }
    addProductwishlistApi = async (p_id) => {
        const signToken = await AsyncStorage.getItem('signToken')
        var temp = {}
        temp["vendor_product"] = p_id
        axios.post(`${this.state.base_url}/api/wishlist/`, JSON.stringify(temp), {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${signToken}`
            },

        })
            .then((res) => {
                if (res.status == 201) {
                    ToastAndroid.show('Item added to WishList.', ToastAndroid.SHORT);

                    this.wishlistApi()
                    this.loading_false()

                }

            })
            .catch((error) => {
                console.error("error addProductwishlistApi")
            })
    }

    addPaypalButtonApi = async () => {
        // const signToken = await AsyncStorage.getItem('signToken')
        const { cart_data,delivery_option,delivery_choice, delivery_slot } = this.state
        var temp = {}
        temp["cart_id"] = cart_data.cart_id
        temp["delivery_option"] = delivery_option
        temp["delivery_type"] = delivery_choice
        temp["delivery_slot"] = delivery_slot
        console.log(JSON.stringify(temp),"addPaypalButtonApi ")
        axios.post(`${this.state.base_url}/api/paypal/button/`, JSON.stringify(temp), {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            },

        })
            .then((res) => {
                // console.log(res.data,"addPaypalButtonApi",res)
                if (res.status == 201) {
                    this.setState({paypalbtn_data : res.data})
                //     ToastAndroid.show('Item added to WishList.', ToastAndroid.SHORT);

                //     this.wishlistApi()
                //     this.loading_false()

                }

            })
            .catch((error) => {
                console.error("error addPaypalButtonApi",error)
            })
    }

    orderListApi = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        axios.get(`${this.state.base_url}/api/userprofile/myaccount/`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data, "orderListApi")
                this.setState({
                    order_list: res.data.my_orders
                })
            })
            .catch((error) => {
                console.error("error orderListApi")
            })
    }
    ViewOrderApi = async (order_id) => {
        const signToken = await AsyncStorage.getItem('signToken')
        axios.get(`${this.state.base_url}/api/order/${order_id}/`, {
            headers: {
                'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data,"view_order")
                this.setState({
                    view_order: res.data
                })
            })
            .catch((error) => {
                console.error("error view_orderApi")
            })
    }

    handleFilterChange = (viewDetailsValue) => {
        if (viewDetailsValue === 'Order Details') {
            this.setState({ orderDetails: true, viewItems: false });
        } else {
            this.setState({ orderDetails: false, viewItems: true });
        }
    }


    handleRemovewishlist = async (p_id) => {
        const signToken = await AsyncStorage.getItem('signToken')
        const { wishlist_list } = this.state

        var temp_wishlist = _.find(wishlist_list, (a) => a.vendor_product_id === p_id);
        if (temp_wishlist !== undefined) {
            this.removeWishlistItemApi(temp_wishlist.id)
        }
    }

    removeWishlistItemApi = async (id) => {
        // console.log(id,"---------------")
        const signToken = await AsyncStorage.getItem('signToken')

        axios.delete(`${this.state.base_url}/api/wishlist/${id}/`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res, "removeWishlistItemApi")
                if (res.status === 204) {
                    ToastAndroid.show('Item removed from WishList.', ToastAndroid.SHORT);

                    this.wishlistApi()
                    this.loading_false();

                }

            })
            .catch((error) => {
                console.error("error removeWishlistItemApi")
            })
    }


    redirectpageApi = async (url) => {
        console.log(url, "redirectpageApi")
        const signToken = await AsyncStorage.getItem('signToken')
        // var page = this.state.page;

        // const url = `${this.state.base_url}/api/category/${id}/catalog/`;
        if (url !== null) {

            axios.get(url, {
                headers: {
                    'Content-Type': 'application/json',
                    // 'Authorization': `Token ${signToken}`
                }
                // headers: {
                //     'Authorization': `Token ${signToken}`
                // }
            })
                .then((res) => {
                    // console.log(res.data, "redirectpageApi")
                    this.setState({

                        product_list: res.data.results.products,
                        facetednav: res.data.results.facetednav,
                        product_count: res.data.count,
                        previous_url: res.data.previous,
                        next_url: res.data.next,
                    })
                    this.loading_false()
                })
                .catch((error) => {
                    console.error("error redirectpageApi")
                })
        }
        // this.fetchCatOffersApi(id);
    }


    addAddressApi = async () => {

        const signToken = await AsyncStorage.getItem('signToken')
        const newData = {
            "address_1": this.state.address_1,
            "address_2": this.state.address_2,
            "pincode": this.state.pincode,
            // "city": this.state.city,
            // "state": this.state.state,
            "country": this.state.country,
            "name": this.state.name,
            "phone_no": this.state.phone_no,
            "set_default_address": this.state.set_default_address,
            "address_type": this.state.address_type
        }
        axios.post(`${this.state.base_url}/api/user/address/all/`, JSON.stringify(newData), {
            headers: {

                'Content-Type': 'application/json',
                'Authorization': `Token ${signToken}`
            },
            // addr1:'test',
        })
            .then((res) => {
                console.log(res.data,"addAddressApi")
                if (res.status === 201) {

                    this.setState({
                        postNewAddress: res.data
                    });
                    this.fetchAllAddressApi();

                }


            })
            .catch((error) => {
                console.error("error addAddressApi", error)
            })
    }


    fetchAllAddressApi = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        axios.get(`${this.state.base_url}/api/user/address/all/`, {
            headers: {
                'Content-Type': 'application/json',

                'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data,"fetchAllAddressApi, res")
                this.setState({
                    addressListData: res.data.address,
                    address_types: res.data.address_types,
                })
            })
            .catch((error) => {
                console.error("error fetchAllAddress")
            })
    }

    deleteAddressApi = async (addr_id) => {
        // const url = `${this.state.base_url}/api/user/address/${addr_id}/update/`;
        const signToken = await AsyncStorage.getItem('signToken')

        axios.delete(`${this.state.base_url}/api/user/address/${addr_id}/update/`, {
            headers: {
                'Authorization': `Token ${signToken}`
            },
            // addr1:'test',
        })
            .then((res) => {
                if (res.status === 204) {
                    this.fetchAllAddressApi();
                }
            })
            .catch((error) => {
                console.error("error DeleteAddress", error)
            })
    }

    // defaultgetAddressApi = async (addr_id) => {

    //     const signToken = await AsyncStorage.getItem('signToken')

    //     axios.get(`${this.state.base_url}/api/user/address/${addr_id}/update/`, {
    //         headers: {
    //             'Content-Type': 'application/json',
    //             'Authorization': `Token ${signToken}`
    //         },
    //         // addr1:'test',
    //     })
    //         .then((res) => {
    //             if (res.status === 200) {
    //                 var default_addr = res.data.address
    //                 if (default_addr.set_default_address === false ) {
    //                     default_addr["set_default_address"] = true
    //                 }
    //                 // this.setState({
    //                 //     default_addr:default_addr,

    //                 // })
    //                 this.defaultpostAddressApi(addr_id,default_addr)

    //             }
    //         })
    //         .catch((error) => {
    //             console.error("error defaultgetAddressApi", error)
    //         })
    // }
    defaultpostAddressApi = async (addr_id) => {
        // const url = `${this.state.base_url}/api/user/address/${addr_id}/update/`;

        const signToken = await AsyncStorage.getItem('signToken')
        var default_dict = _.find(this.state.addressListData, (a) => {
            return a.id === addr_id;
        });
        var data = {};
        data['id'] = default_dict.id;
        data['address_type'] = default_dict.address_type;
        data['phone_no'] = default_dict.phone_no;
        data['state'] = default_dict.state;
        if (default_dict.set_default_address === false) {
            data['set_default_address'] = true;
        }

        axios.put(`${this.state.base_url}/api/user/address/${addr_id}/update/`, JSON.stringify(data), {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${signToken}`
            },
            // addr1:'test',
        })
            .then((res) => {

                if (res.status === 201) {


                    this.fetchAllAddressApi();

                }


            })
            .catch((error) => {
                console.error("error defaultpostAddressApi", error)
            })
    }

    editgetAddressApi = async (addr_id) => {

        // const signToken = await AsyncStorage.getItem('signToken')

        // axios.get(`${this.state.base_url}/api/user/address/${addr_id}/update/`, {
        //     headers: {
        //         'Content-Type': 'application/json',
        //         'Authorization': `Token ${signToken}`
        //     },
        //     // addr1:'test',
        // })
        //     .then((res) => {
        // if (res.status === 200) {
        // console.log(res.data.address,"get response")
        var default_dict = _.find(this.state.addressListData, (a) => {
            return a.id === addr_id;
        });
        this.setState({

            id_addr_edit: default_dict.id,
            address_1_addr_edit: default_dict.address_1,
            address_2_addr_edit: default_dict.address_2,
            pincode_addr_edit: default_dict.pincode,
            city_addr_edit: default_dict.city,
            country_addr_edit: default_dict.country,
            name_addr_edit: default_dict.name,
            phone_no_addr_edit: default_dict.phone_no,
            set_default_address_addr_edit: default_dict.set_default_address,
            state_addr_update: default_dict.state,
            address_type_addr_edit: default_dict.address_type



        })
        // }
        // })
        // .catch((error) => {
        //     console.error("error DeleteAddress", error)
        // })
    }

    updateAddressApi = async (addr_id) => {
        // const url = `${this.state.base_url}/api/user/address/${addr_id}/update/`;
        const newData = {
            "address_1": this.state.address_1_addr_edit,
            "address_2": this.state.address_2_addr_edit,
            "pincode": this.state.pincode_addr_edit,
            // "city": this.state.city_addr_edit,
            // "state": this.state.state,
            "country": this.state.country_addr_edit,
            "name": this.state.name_addr_edit,
            "phone_no": this.state.phone_no_addr_edit,
            "set_default_address": this.state.set_default_address_addr_edit,
            "address_type": this.state.address_type_addr_edit,
        }

        const signToken = await AsyncStorage.getItem('signToken')

        axios.put(`${this.state.base_url}/api/user/address/${addr_id}/update/`, JSON.stringify(newData), {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${signToken}`
            },
            // addr1:'test',
        })
            .then((res) => {
                if (res.status === 201) {
                    this.fetchAllAddressApi();
                }
            })
            .catch((error) => {
                console.error("error DeleteAddress", error)
            })
    }

    shpbyvendorApi = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        axios.get(`${this.state.base_url}/api/vendor_list/`, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data,"shpbyvendorApi")
                this.setState({
                    vendor_list: res.data.rows
                })
            })
            .catch((error) => {
                console.error("error shpbyvendorApi")
            })
    }

    shpbyBrandApi = async (vendor_slug) => {
        const signToken = await AsyncStorage.getItem('signToken')
        axios.get(`${this.state.base_url}/api/brand_list/${vendor_slug}/`, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data,"shpbyBrandApi")
                this.setState({
                    brand_list: res.data.rows
                })
            })
            .catch((error) => {
                console.error("error shpbyBrandApi")
            })
    }
    shpbyCatApi = async (vendor_slug) => {
        // console.log(this.state.vendor_slug,"vendor_slug")
        const signToken = await AsyncStorage.getItem('signToken')
        axios.get(`${this.state.base_url}/api/category_list/${vendor_slug}/`, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data, "shpbyCatApi")
                this.setState({
                    cat_list: res.data.shop_by_categories
                })
            })
            .catch((error) => {
                console.error("error shpbyCatApi")
            })
    }

    fetchProductDtlApi = async (id) => {
        console.log(id, "product_id")
        const signToken = await AsyncStorage.getItem('signToken')
        const url = `${this.state.base_url}/api/vendorproduct/${id}/`;
        axios.get(url, {
            // headers: {
            //     'Authorization': `Token ${signToken}`
            // }
        })
            .then((res) => {
                var { product_dtl_data } = this.state
                product_dtl_data = res.data
                product_dtl_data['quantity'] = 1
                this.setState({

                    product_dtl_data: product_dtl_data,
                })
                // console.log(res.data, "fetchProductDtlApi----------")
                this.loading_false()
            })
            .catch((error) => {
                console.error("error fetchProductDtlApi")
            })
        // this.fetchCatOffersApi(id);
    }
    fetchCatProductApi = async (id) => {
        // console.log(id, "------------------- fetchCatProductApi")

        const signToken = await AsyncStorage.getItem('signToken')
        const page = this.state.page;
        const url = `${this.state.base_url}/api/category/${id}/catalog/`;
        axios.get(url, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data, "fetchProductsApi")
                this.setState({

                    page_details: res.data.results.details,
                    product_list: res.data.results.products,
                    facetednav: res.data.results.facetednav,
                    product_count: res.data.count,
                    previous_url: res.data.previous,
                    next_url: res.data.next,
                    t_filter:'category',
                })
                this.shpbyBrandApi(res.data.results.vendor_slug)
                this.shpbyCatApi(res.data.results.vendor_slug)
                this.loading_false();
            })
            .catch((error) => {
                console.error("error fetchProductsApi")
            })
        this.fetchCatOffersApi(id);
    }
    fetchBrandProductApi = async (id) => {
        // console.log(id, "------------------- fetchBrandProductApi")

        const signToken = await AsyncStorage.getItem('signToken')
        const page = this.state.page;
        const url = `${this.state.base_url}/api/brand/${id}/catalog/`;
        axios.get(url, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data, "fetchProductsApi")
                this.setState({

                    page_details: res.data.results.details,
                    product_list: res.data.results.products,
                    facetednav: res.data.results.facetednav,
                    product_count: res.data.count,
                    previous_url: res.data.previous,
                    next_url: res.data.next,
                    t_filter:'Brand',
                })
                this.shpbyBrandApi(res.data.results.vendor_slug)
                this.shpbyCatApi(res.data.results.vendor_slug)
                this.loading_false()
            })
            .catch((error) => {
                console.error("error fetchProductsApi")
            })
        this.fetchBrandOffersApi(id);
    }
    fetchSearchApi = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        const page = this.state.page;
        const q = this.state.search_query
        const url = `${this.state.base_url}/api/search/?q=${q}&searchText=${q}`;
        axios.get(url, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data, "fetchSearchApi")
                this.setState({

                    product_list: res.data.results.products,
                    facetednav: res.data.results.facetednav,
                    product_count: res.data.count,
                    previous_url: res.data.previous,
                    next_url: res.data.next,
                })
            })
            .catch((error) => {
                console.error("error fetchSearchApi")
            })
        // this.fetchBrandOffersApi(id);
    }



    autocompleteApi = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        // const page = this.state.page;
        // const q = this.state.search_query
        const url = `${this.state.base_url}/api/autocomplete/`;
        axios.get(url, {
            // headers: {
            //     'Authorization': `Token ${signToken}`
            // }
        })
            .then((res) => {
                // console.log(res.data.suggestions, "autocompleteApi")
                this.setState({

                    suggestions: res.data.suggestions
                })
            })
            .catch((error) => {
                console.error("error autocompleteApi")
            })
        // this.fetchBrandOffersApi(id);
    }

    fetchVendorProductApi = async (id) => {
        this.fetchVendorOffersApi(id);
        const signToken = await AsyncStorage.getItem('signToken')
        const page = this.state.page;
        // console.log(id,"------------------- fetchVendorProductApi vendor_slug") 

        const url = `${this.state.base_url}/api/vendor/${id}/catalog/`;
        axios.get(url, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data,"fetchVendorProductApi")
                this.setState({
                    page_details: res.data.results.details,
                    product_list: res.data.results.products,
                    facetednav: res.data.results.facetednav,
                    product_count: res.data.count,
                    previous_url: res.data.previous,
                    next_url: res.data.next,
                    vendor_slug: res.data.results.details.slug,
                    t_filter:'Store',
                })
                this.shpbyBrandApi(res.data.results.details.slug)
                this.shpbyCatApi(res.data.results.details.slug)
                this.loading_false()
            })
            .catch((error) => {
                console.error("error fetchProductsApi")
            })
    }



    fetchCatOffersApi = async (id) => {
        const signToken = await AsyncStorage.getItem('signToken')
        const url = `${this.state.base_url}/api/category/${id}/offers/`;

        axios.get(url, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data,"res.data---------------")
                this.setState({
                    product_offers_rec: res.data,
                })
            })
            .catch((error) => {
                console.error("error fetchcatProductsOffersApi")
            })
    }
    fetchBrandOffersApi = async (id) => {
        const signToken = await AsyncStorage.getItem('signToken')
        const url = `${this.state.base_url}/api/brand/${id}/offers/`;

        axios.get(url, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                this.setState({
                    product_offers_rec: res.data,
                })
            })
            .catch((error) => {
                console.error("error fetchbrandProductsOffersApi")
            })
    }
    fetchVendorOffersApi = async (id) => {
        const signToken = await AsyncStorage.getItem('signToken')
        const url = `${this.state.base_url}/api/vendor/${id}/offers/`;

        axios.get(url, {
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res.data.rows,"offer")
                this.setState({
                    product_offers_rec: res.data.rows,
                })
            })
            .catch((error) => {
                console.error("error fetchvendorProductsOffersApi")
            })
    }



    handleInputBox = (text, stateProp) => {
        this.setState({
            [stateProp]: text,
        })
        // console.log("sdafcsdfcsd",text,stateProp)
    }
    selectModeChange = (text) => {
        this.setState({
            select_mode: text,
        })
    }

    selectDropDown = (value) => {
        if (value === this.state.select_cat) {
            this.toggleDropDown()
            this.toggleChildrenDropDown()
        } else {
            this.setState({
                select_cat: value,
                toggle_dropdown: false,
            })
        }

    }

    toggleDropDown = () => {

        this.setState({
            select_cat: null,

            toggle_dropdown: true,
        })

    }

    selectChildrenDropDown = (value) => {
        if (value === this.state.select_pdct) {
            this.toggleChildrenDropDown()
        } else {
            this.setState({
                select_pdct: value,
                toggle_children_dropdown: false,
            })
        }
    }

    toggleChildrenDropDown = () => {
        this.setState({
            select_pdct: null,
            toggle_children_dropdown: true,
        })
    }


    // headers: {
    //            'Content-Type': 'application/json',
    //            'Authorization': `Token ${signToken}`
    //        },

    handleSignIn = async () => {
        const { login_username, login_password } = this.state
        const data = {
            // username: 'iammrjyoti@gmail.com',
            // password: "asdfasdf11"
            username: login_username,
            password: login_password
        }
        await axios.post(`${this.state.base_url}/api/token/`, JSON.stringify(data), {
            headers: {
                'Content-Type': 'application/json',
            },

        })

            .then(res => {
                if (res.status === 200) {

                    this.setState({
                        signInToken: res.data.token,
                        signInError: false,
                        invalidCredential: false
                    })

                }
                else {
                    this.setState({
                        signin_error: res.data
                    })
                }
            })
            .catch((error) => {
                this.setState({
                    signInError: true,
                    invalidCredential: true
                })
                console.log(error, "error")
            })

        const { signInToken } = this.state
        // console.log('signInToken', signInToken)

        if (signInToken !== null) {
            await AsyncStorage.setItem('signToken', signInToken)
                .then(() => {
                    console.log('data saved');
                    this.userDetail()
                    this.orderListApi()
                    this.wishlistApi()
                    this.fetchAllAddressApi()
                    this.bottomModalFunc(null)
                })
                .catch((error) => {
                    console.log(error);
                })
        }
        else {

            this.bottomModalFunc("signIn")
        }

    }

    signin_changes = () => {
        this.setState({ signInError: false, })
        setTimeout(() => this.setState({ signInError: true }), 2000)
    }

    handleSignUp = async () => {
        const { signup_username, signup_password, signup_zipcode } = this.state
        const data = {
            "user": { "email": signup_username, "password": signup_password },
            // "phone": "7008621597",
            "email": signup_username,
            "shipping_zip": signup_zipcode
            // "shipping_zip": "ha2 9nh"
            // "shipping_zip": "HA2 9NH"
        }
        // console.log(data,"-------------")
        await axios.post(`${this.state.base_url}/api/newuserprofile/register/`, JSON.stringify(data),
            {
                headers: {
                    'Content-Type': 'application/json',
                    // 'Authorization': `Token ${signToken}`
                }
            })
            .then(res => {

                if (res.status === 201) {

                    this.bottomModalFunc("signIn")
                    ToastAndroid.show('Thank you for register cloudkart360', ToastAndroid.SHORT);

                }
                // console.log(res, "handleSignUp response")
            }
            )
            .catch(err => console.log("Sign Up Error", err))
    }
    handleResetPwd = async () => {
        const { forgot_pwd_email } = this.state
        // console.log(forgot_pwd_email,"-------------")
        const data = {

            "email": forgot_pwd_email,


        }

        await axios.post(`${this.state.base_url}/api/password_reset/`, JSON.stringify(data),
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .then(res =>
                console.log(res, "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
            )
            .catch(err => console.log("Sign Up Error", err))
    }

    handlePostcode = async () => {
        const { postcode } = this.state
        // console.log(forgot_pwd_email,"-------------")
        const data = {

            "user_pref_pincode": postcode,

        }
        // console.log(data,"test")

        await axios.post(`${this.state.base_url}/api/service_zipcode_api/`, JSON.stringify(data),
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .then(res => {
                // console.log(res.data, "handlePostcode")
                this.setState({ Postcode_data: res.data.result })
                setTimeout(() => {
                    this.bottomModalFunc(null)
                    Postcode_data["message"] = null
                    this.setState({ Postcode_data: Postcode_data })
                }, 2000)
                if (res.data.result.success) {
                    this.setState({ valid_postcode: postcode })
                }
            })
            .catch(err => console.log("handlePostcode", err))

        var { Postcode_data,valid_postcode } = this.state
        if (valid_postcode !== undefined) {
            await AsyncStorage.setItem('postcode', valid_postcode)
            .then(() => {

            })
            .catch((error) => {
                console.log(error);
            })
        }

    }

    writeAReviewApi = async (vp_id) => {
        const { review_title, review_rating, review_text } = this.state
        // console.log(forgot_pwd_email,"-------------")
        const data = {

            title: review_title,
            rating: review_rating,
            content: review_text,


        }

        const signToken = await AsyncStorage.getItem('signToken')
        // console.log(JSON.stringify(data),"------------------------",vp_id)

        await axios.post(`${this.state.base_url}/api/vendorproduct/${vp_id}/add_review/`, JSON.stringify(data),
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${signToken}`
                }
            })
            .then(res => {
                if (res.status === 201) {
                    this.setState({ review_rating: 0 })
                }
            }

            )
            .catch(err => console.log("writeAReviewApi Error", err))
    }


    // fetchProfileInfo = async () => {
    //     const signToken = await AsyncStorage.getItem('signToken')
    //     axios.get(`${this.state.base_url}/api/user_list/`, {
    //         headers: {
    //             'Authorization': `Token ${signToken}`
    //         }
    //     })
    //         .then((res) => {
    //             console.log(res, "profile Info", this.state.suggestions)
    //             // this.setState({
    //             //     view_order: res.data
    //             // })
    //         })
    //         .catch((error) => {
    //             console.error("error view_orderApi")
    //         })
    // }

    updateName = async (user_id) => {
        const { first_name, last_name } = this.state
        var temp = {}
        temp['first_name'] = first_name
        temp['last_name'] = last_name
        const signToken = await AsyncStorage.getItem('signToken')
        // console.log(JSON.stringify(temp),user_id,"---------------")
        axios.put(`${this.state.base_url}/api/update_user/${user_id}/`, JSON.stringify(temp), {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res, "updateName")
                if (res.status === 200) {
                    this.userDetail()
                }

            })
            .catch((error) => {
                console.error("error updateName")
            })
    }

    updateUserMobileNumber = async (user_id) => {
        const { new_mobile_number } = this.state
        const signToken = await AsyncStorage.getItem('signToken')
        var temp = {}
        temp['phone'] = new_mobile_number
        axios.put(`${this.state.base_url}/api/user_mobile_update/${user_id}/`, JSON.stringify(temp), {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res, "updateUserMobileNumber")
                if (res.status === 200) {
                    this.userDetail()
                }

            })
            .catch((error) => {
                console.error("error updateUserMobileNumber")
            })

        // console.log(this.state.new_mobile_number, "new_mobile_number")
    }


    updateUserEmail = async (user_id) => {


        if (!this.state.captcha_input) {
            this.setState({ captcha_err: "Enter Captcha!!!", captcha_input: '', })
        } else {
            // console.log(this.state.captcha_input,
            //     "lvdnslvknslvnsdlvndslvndvlk"
            //     , this.state.captch_text)
            const is_human = this.validCaptcha(this.state.captch_text, this.state.captcha_input)
            // console.log(is_human, "sdjvbnsodvhsoivnsvln")
            if (!is_human) {
                this.captcha()
                this.setState({ captcha_err: "Entered Captcha is wrong!!!", captcha_input: '', })
            } else {
                const signToken = await AsyncStorage.getItem('signToken')
                var temp = {}
                const { new_email } = this.state
                temp['email'] = new_email
                axios.put(`${this.state.base_url}/api/update_user_email/${user_id}/`, JSON.stringify(temp), {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${signToken}`
                    }
                })
                    .then((res) => {
                        // console.log(res, "updateUserEmail")
                        if (res.status === 200) {
                            this.userDetail()
                        }


                    })
                    .catch((error) => {
                        console.error("error updateUserEmail")
                    })
            }
        }


    }

    changePassword = async () => {
        const { curr_password, new_password, re_new_password } = this.state
        const signToken = await AsyncStorage.getItem('signToken')
        var temp = {}
        temp['old_password'] = curr_password
        temp['new_password1'] = new_password
        temp['new_password2'] = re_new_password
        axios.put(`${this.state.base_url}/api/changepasswordview/`, JSON.stringify(temp), {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res, "changePassword")
                if (res.status === 200) {
                    this.userDetail()
                }

            })
            .catch((error) => {
                console.error("error changePassword")
            })
    }

    userDetail = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        axios.get(`${this.state.base_url}/api/user_list/`, {
            headers: {
                'Authorization': `Token ${signToken}`
            }
        })
            .then((res) => {
                // console.log(res, "userDetail")
                this.setState({
                    userdetail_data: res.data.data,
                })

            })
            .catch((error) => {
                console.error("error userDetail")
            })
    }




    async componentDidMount() {
        await Font.loadAsync({
            pf: require('../assets/fonts/pf.ttf'),
            Roboto: require('native-base/Fonts/Roboto.ttf'),
            Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
        });
        this.initialFunc()
        const signToken = await AsyncStorage.getItem('signToken')
        if (signToken !== undefined) {
            this.setState({ signInToken: signToken })
            this.userDetail()
            this.orderListApi()
            this.wishlistApi()
            this.fetchAllAddressApi()
        }

        const postcode = await AsyncStorage.getItem('postcode')
        if (postcode !== undefined) {
            this.setState({ postcode: postcode,valid_postcode:postcode })
            this.handlePostcode()
        }
        // const cart_id = await AsyncStorage.getItem('cart_id')
        // console.log(cart_id,"asnc")
        // if (cart_id !== undefined) {
        //     this.setState({ cart_id: cart_id })
        //     this.cartDtaList()
        // }
        // else{

        // }

        YellowBox.ignoreWarnings(['Animated: `useNativeDriver`']);
    }


    initialFunc = async () => {

        this.setState({ isReady: true });
        this.offerApi()
        this.currencyApi()
        this.shpbyvendorApi()
        this.cartListApi()
        // this.shpbyBrandApi()
        // this.shpbyCatApi()
        this.autocompleteApi()
    }



    render() {
        // console.log(this.state.signInError, "signInError")

        return (
            <CKart.Provider value={{
                ...this.state,
                handleInputBox: this.handleInputBox,
                handleSignIn: this.handleSignIn,
                offerApi: this.offerApi,
                selectModeChange: this.selectModeChange,
                // catalogChange: this.catalogChange,
                scrollfunc: this.scrollfunc,
                fetchCatProductApi: this.fetchCatProductApi,
                fetchBrandProductApi: this.fetchBrandProductApi,
                fetchVendorProductApi: this.fetchVendorProductApi,
                fetchProductDtlApi: this.fetchProductDtlApi,
                // cartListApi: this.cartListApi,
                selectDropDown: this.selectDropDown,
                toggleDropDown: this.toggleDropDown,
                selectChildrenDropDown: this.selectChildrenDropDown,
                inc_qty: this.inc_qty,
                dec_qty: this.dec_qty,
                fetchAllAddressApi: this.fetchAllAddressApi,
                addAddressApi: this.addAddressApi,
                remove_cart_item: this.remove_cart_item,
                filterDataFunc: this.filterDataFunc,
                addToCart: this.addToCart,
                setFilterData: this.setFilterData,
                setSortFilterData: this.setSortFilterData,
                resetFilter: this.resetFilter,
                resetSortFilter: this.resetSortFilter,
                removeWishlistItemApi: this.removeWishlistItemApi,
                redirectpageApi: this.redirectpageApi,
                fatchFilteredDataApi: this.fatchFilteredDataApi,
                quantity_change: this.quantity_change,
                handleFilterChange: this.handleFilterChange,
                ViewOrderApi: this.ViewOrderApi,
                deleteAddressApi: this.deleteAddressApi,
                buyNow: this.buyNow,
                setCheckoutData: this.setCheckoutData,
                fetchSearchApi: this.fetchSearchApi,
                setModalVisible: this.setModalVisible,
                clearData: this.clearData,
                addProductwishlistApi: this.addProductwishlistApi,
                handleRemovewishlist: this.handleRemovewishlist,
                orderListApi: this.orderListApi,
                wishlistApi: this.wishlistApi,
                LogoutHandle: this.LogoutHandle,
                updateName: this.updateName,
                updateUserMobileNumber: this.updateUserMobileNumber,
                updateUserEmail: this.updateUserEmail,
                changePassword: this.changePassword,
                bottomModalFunc: this.bottomModalFunc,
                handleSignUp: this.handleSignUp,
                handleResetPwd: this.handleResetPwd,
                userDetail: this.userDetail,
                captcha: this.captcha,
                writeAReviewApi: this.writeAReviewApi,
                signin_changes: this.signin_changes,
                resetInvalidCredential: this.resetInvalidCredential,
                editgetAddressApi: this.editgetAddressApi,
                updateAddressApi: this.updateAddressApi,
                defaultpostAddressApi: this.defaultpostAddressApi,
                loading_true: this.loading_true,
                loading_false: this.loading_false,
                handlePostcode: this.handlePostcode,
                delivery_option_change: this.delivery_option_change,
                delivery_choice_change: this.delivery_choice_change,
                delivery_slot_change: this.delivery_slot_change,
                checkoutGetApi: this.checkoutGetApi,
                getItmQty: this.getItmQty,
                incItmQty: this.incItmQty,
                decItmQty: this.decItmQty,
                addPaypalButtonApi: this.addPaypalButtonApi,
                addToCartValidation: this.addToCartValidation
                // fetchProfileInfo: this.fetchProfileInfo

            }}>
                {this.props.children}
            </CKart.Provider>
        )
    }
}

Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};