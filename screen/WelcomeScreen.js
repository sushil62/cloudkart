import React from 'react'
import { View, Text, Button,Image } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient';
import { Styles } from '../Stylesheet'
import LgButton from '../Common/LgButton'
import Br from '../Common/Br'
import Circle from '../Common/Circle'
import CircleTr from '../Common/CircleTr'



const WelcomeScreen = ({ navigation }) => {
    const { pColorOne, pColorTwo, fontWeight, HFontsize, pFontColor, fontColor, pColorThree } = Styles
    return (
        <View style={{ flex: 1 }}>

            <LinearGradient colors={[pColorOne, pColorTwo]} style={{ flex: 1, paddingHorizontal: 20, justifyContent: "space-around" }} >
                <View style={{ zIndex: 10 }}>
                <Image source={require('../Images/logoTrans.png')} style={{width:120,height:120}}/>
                    <Text style={{ fontSize: HFontsize * 1.4, color: pFontColor, fontFamily: "pf" }}>Welcome</Text>
                    <Text style={{ fontSize: 24, color: pFontColor, fontFamily: "pf" }}>Smart Academy</Text>
                    <Text />
                    <Text style={{ color: "#dcdcdc" }}>Running to migrate libraries to AndroidX. You can disable it using "--no-jetifier" flag.
                    Accessing non-existent property 'padLevels' of module exports inside circular dependency</Text>
                </View>





                <View style={{ zIndex: 1 }}>
                    <View style={{ alignItems: "center", marginBottom: 8 }}>
                        <Circle CColor={pColorThree} size={20} />
                    </View>

                    <LgButton title="Sign In" color={pFontColor} pressFunc={() => navigation.navigate('SignIn')} />
                    <Br />
                    <LgButton title="Register" bgColor="#fff" color={fontColor} pressFunc={() => navigation.navigate('SignUp')} />
                </View>
                <View style={{ position: "absolute", bottom: -30, left: -20 }}>
                    <Circle CColor={pColorThree} size={120} />
                </View>
                <View style={{ position: "absolute", bottom: 350, left: -40 }}>
                    <Circle CColor="#000" size={100} />
                </View>
                <View style={{ position: "absolute", top: 70, right: -40 }}>
                    <Circle CColor={pColorThree} size={70} />
                </View>
                <View style={{ position: "absolute", top: 70, left: -30 }}>
                    <CircleTr CColor="#000" size={120} bWidth={20} />
                </View>
                <View style={{ position: "absolute", bottom: 150, right: -30 }}>
                    <CircleTr CColor={pColorThree} size={140} bWidth={15} />
                </View>
                <View style={{ position: "absolute", top: 400, left: 120 }}>
                    <CircleTr CColor={pColorThree} size={40} bWidth={4} />
                </View>





            </LinearGradient>
        </View>
    )
}

export default WelcomeScreen
