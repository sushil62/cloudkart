import React from 'react'
import { View, Text } from 'react-native'
import { Styles } from '../../Stylesheet'
import { Item, Label, Input } from 'native-base'
import Br from '../../Common/Br'

const SignInForm = () => {
    const { HFontsize, SFontsize, borderRadius } = Styles
    return (
        <View style={{ backgroundColor: "#f5f5f5", padding: 20, borderRadius,paddingVertical:40 }}>
            <Item floatingLabel>
                <Label>Contact Person Name</Label>
                <Input/>
            </Item>
            <Br/>
            <Item floatingLabel>
                <Label>Title</Label>
                <Input/>
            </Item>
            <Br/>
            <Item floatingLabel>
                <Label>Contact Email</Label>
                <Input keyboardType="email-address"/>
            </Item>
            <Br/>
            <Item floatingLabel>
                <Label>Contact Mobile Number</Label>
                <Input keyboardType="number-pad"/>
            </Item>
            <Br/>
            <Item floatingLabel>
                <Label>Username</Label>
                <Input/>
            </Item>
        </View>
    )
}

export default SignInForm
