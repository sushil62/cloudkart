import React from 'react'
import { View, Text, Button, TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import BackButton from '../../Common/BackButton'
import LgButton from '../../Common/LgButton'

import Br from '../../Common/Br'
import { Styles } from '../../Stylesheet'
import Circle from '../../Common/Circle'
import CircleTr from '../../Common/CircleTr'
import SignUpForm from './SignUpForm'


const SignUp = ({ navigation }) => {
    const { HFontsize, SFontsize, pColorTwo, pFontColor, pColorThree, pColorOne } = Styles



    return (
        <View style={{ paddingTop: 50, paddingHorizontal: 30, flex: 1 }}>
            <BackButton title="Back" />

            <View style={{ position: "absolute", top: 380, left: 4 }}>
                <Circle CColor="#000" size={20} />
            </View>
            <View style={{ position: "absolute", top: 400, left: 4 }}>
                <CircleTr CColor={pColorThree} size={40} bWidth={4} />
            </View>
            <View style={{ position: "absolute", top: 70, right: -20 }}>
                <CircleTr CColor="#000" size={120} bWidth={16} />
            </View>
            <View style={{ position: "absolute", top: 80, right: 100 }}>
                <Circle CColor={pColorThree} size={20} />
            </View>
            <View style={{ position: "absolute", bottom: 100, right: -20 }}>
                <CircleTr CColor={pColorThree} size={70} bWidth={10} />
            </View>
            <View style={{ position: "absolute", bottom: 130, left: 30 }}>
                <CircleTr CColor={pColorTwo} size={20} bWidth={4} />
            </View>
            <View style={{ flex: 1, justifyContent: "space-around" }}>
                <View>
                    <Text style={{ fontFamily: "pf", fontSize: HFontsize }}>Join the</Text>
                    <Text style={{ fontFamily: "pf", fontSize: SFontsize }}>Community</Text>
                    <Br />
                    <SignUpForm />
                    <View style={{marginTop:20}}>
                        <LgButton title="Join The Community" bgColor={pColorTwo} color={pFontColor} />
                        <Br num={2} />
                        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
                            <View style={{ height: 1, backgroundColor: "#dcdcdc", width: "47%" }}></View>
                            <Text style={{ fontWeight: "700" }}>Or</Text>
                            <View style={{ height: 1, backgroundColor: "#dcdcdc", width: "47%" }}></View>
                        </View>
                        <Br num={2} />
                        <Text style={{ textAlign: "center" }}>Already Have an Account ? <Text style={{ fontWeight: "700", color: pColorTwo }}>Sign In</Text></Text>
                    </View>
                </View>

            </View>
        </View>
    )
}

export default SignUp
