import React, { Component } from 'react'
import { View, Text, ActivityIndicator} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import Test from '../Components/Test'



export default class AuthLoadingScreen extends Component {
    constructor(props) {
        super(props);
        this.loadApp()
    }
    
    loadApp = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        this.props.navigation.navigate('Home')
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Text>Loading</Text>
                <ActivityIndicator />
                
            </View>
        );
    }
}

