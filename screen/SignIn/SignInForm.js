import React, { useContext } from 'react'
import { View, Text, StyleSheet, TextInput } from 'react-native'
import { Styles } from '../../Stylesheet'
import { Item, Label, Input } from 'native-base'
import Br from '../../Common/Br'
import { CKart } from '../../ContextApi'

const SignInForm = () => {
    const { email, password, handleInputBox } = useContext(CKart)
    const { HFontsize, SFontsize, borderRadius } = Styles
    return (
        <View style={{ backgroundColor: "#f5f5f5", padding: 20, borderRadius, paddingVertical: 40 }}>
            <TextInput keyboardType="email-address" placeholder="email" onChangeText={text => handleInputBox(text, "email")} />
            <Br />
            <Item floatingLabel>
                <Label>Password</Label>
                <Input secureTextEntry onChangeText={text => handleInputBox(text, "password")} />
            </Item>
            <Item floatingLabel>
                <Label>Area Code</Label>
                <Input secureTextEntry onChangeText={text => handleInputBox(text, "AreaCode")} />
            </Item>
        </View>
    )
}

export default SignInForm

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: '#ffffff',
        paddingHorizontal: 35
    },
    textInput: {
        height: 50,
        padding: 10,
        backgroundColor: '#dfdfdf',
        color: '#494d5a',
        borderColor: '#499142',
        borderWidth: 1,
        borderStyle: 'solid',
        marginTop: 30
    },
    signIn: {
        marginTop: 50,
        fontSize: 18
    },
    logo: {
        fontSize: 25
    },
    emailSection: {
        width: '100%'
    },
    passwordSection: {
        width: '100%'
    },
    buttonSection: {
        width: '100%'
    },
    button: {
        width: '100%',
        height: 50,
        borderRadius: 4,
        backgroundColor: '#499142',
        marginTop: 30
    },
    buttonText: {
        textAlign: 'center',
        fontSize: 17,
        paddingVertical: 12,
        color: '#ffffff'
    },
    bottomSection: {
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginTop: 15
    },
    bottomLinks: {
        marginTop: 15
    },
    copyright: {
        marginTop: 40
    },
    link: {
        color: 'blue'
    }
});
