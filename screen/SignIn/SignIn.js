import React, { useContext, useState } from 'react'
import { View, Text,TextInput, Button, TouchableOpacity, StyleSheet,ActivityIndicator,TouchableHighlight } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import BackButton from '../../Common/BackButton'
import LgButton from '../../Common/LgButton'

import Br from '../../Common/Br'
import Circle from '../../Common/Circle'
import CircleTr from '../../Common/CircleTr'
// import { Styles } from '../../Stylesheet'
import { CKart } from '../../ContextApi'



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: '#ffffff',
        paddingHorizontal: 35
    },
    textInput: {
        height: 50,
        padding: 10,
        backgroundColor: '#dfdfdf',
        color: '#494d5a',
        borderColor: 'orange',
        borderWidth: 1,
        borderStyle: 'solid',
        marginTop: 30
    },
    signIn: {
        marginTop: 50,
        fontSize: 18
    },
    logo: {
        fontSize: 25
    },
    emailSection: {
        width: '100%'
    },
    passwordSection: {
        width: '100%'
    },
    buttonSection: {
        width: '100%'
    },
    button: {
        width: '100%',
        height: 50,
        borderRadius: 4,
        backgroundColor: 'orange',
        marginTop: 30
    },
    buttonText: {
        textAlign: 'center',
        fontSize: 17,
        paddingVertical: 12,
        color: '#ffffff'
    },
    bottomSection:{
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginTop: 15
    },
    bottomLinks: {
        marginTop: 15
    },
    copyright: {
        marginTop: 40
    },
    link: {
        color: 'blue'
    }
});


const SignIn = ({ navigation }) => {
    const { email, password, handleSignIn,signInError,handleInputBox} = useContext(CKart)
    // const { HFontsize, SFontsize, pColorTwo, pFontColor, pColorThree, pColorOne } = Styles
    const [loading, setLoading] = useState(false)


    const loadingFunc = () => {
        setLoading(true)
    }
    const loadingFalse = () => {
        setLoading(false)
    }


    const loadApp = async () => {
        const signToken = await AsyncStorage.getItem('signToken')
        navigation.navigate(signToken  ? 'Home' : 'Auth')
        // console.log("dhfbzxjzxfsdgsdfgdsgsdgdsgdsgds")
    }

    



    return (
        <View style={styles.container}>
                <Text style={styles.logo}>LOGO</Text>
                <Text style={styles.signIn}>Sign In</Text>
                <View style={styles.emailSection}>
                    <TextInput
                        placeholder='Phone Number'
                        style={styles.textInput}

                        onChangeText={text=>handleInputBox(text,"login_username")}
                        keyboardType="number-pad"
                    />
                </View>
                <View style={styles.passwordSection}>
                    <TextInput
                        secureTextEntry={true}
                        placeholder='Password'
                        style={styles.textInput}
                        onChangeText={text=>handleInputBox(text,"login_password")}
                    />
                </View>
                <TouchableOpacity style={styles.buttonSection}  onPress={()=>{handleSignIn();setTimeout(()=>loadApp(),1000)}}>
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>SIGN IN</Text>
                    </View>
                </TouchableOpacity>
                <View style={styles.bottomSection}>
                    <TouchableOpacity style={styles.bottomLinks}  onPress={() => navigation.navigate('SignUp')}>
                        <Text style={styles.link}>Don't have account? Sign Up</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bottomLinks}  onPress={() => navigation.navigate('ForgotPasswordPage')}>
                        <Text style={styles.link}>Forgot Password?</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.copyright}>
                    <Text>&copy; All Rights Reserved</Text>
                </View>
            </View>


    )
}

export default SignIn










        // <View style={{ paddingTop: 50, paddingHorizontal: 30, flex: 1 }}>
        //     <BackButton title="Back" />


        //     <View style={{ position: "absolute", top: 350, left: 4 }}>
        //         <Circle CColor={pColorThree} size={20} />
        //     </View>
        //     <View style={{ position: "absolute", top: 380, left: 4 }}>
        //         <CircleTr CColor={pColorTwo} size={40} bWidth={4} />
        //     </View>
        //     <View style={{ position: "absolute", top: 100, right: -20 }}>
        //         <CircleTr CColor={pColorOne} size={120} bWidth={16} />
        //     </View>
        //     <View style={{ position: "absolute", top: 80, right: 100 }}>
        //         <Circle CColor={pColorThree} size={20} />
        //     </View>
        //     <View style={{ position: "absolute", bottom: 100, right: -20 }}>
        //         <CircleTr CColor={pColorThree} size={70} bWidth={10} />
        //     </View>
        //     <View style={{ position: "absolute", bottom: 130, left: 30 }}>
        //         <CircleTr CColor={pColorTwo} size={20} bWidth={4} />
        //     </View>


        //     <View style={{ flex: 1, justifyContent: "space-around" }}>
        //         <View>
        //             <Br num={1} />
        //             <Text style={{ fontFamily: "pf", fontSize: HFontsize }}>Welcome to</Text>
        //             <Text style={{ fontFamily: "pf", fontSize: SFontsize }}>CloudKart360</Text>
        //             <Br />
        //             <SignInForm />
        //             <Br />
        //             <Text style={{color:"red",fontSize:12}}>{signInError?"username/ password mismatch":null}</Text>
        //         </View>
        //         <View style={{ marginTop: 8 }}>
        //         {loading?<ActivityIndicator/>:
        //             <LgButton  title="Sign In" bgColor={pColorTwo} color={pFontColor} pressFunc={()=>{handleSignIn();setTimeout(()=>loadApp(),1000)}} />}
        //             <Br num={2} />
        //             <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
        //                 <View style={{ height: 1, backgroundColor: "#dcdcdc", width: "47%" }}></View>
        //                 <Text style={{ fontWeight: "700" }}>Or</Text>
        //                 <View style={{ height: 1, backgroundColor: "#dcdcdc", width: "47%" }}></View>
        //             </View>
        //             <Br num={2} />
        //             <Text style={{ textAlign: "center" }}>Don't Have an Account ? <Text style={{ fontWeight: "700", color: pColorTwo }}>Register</Text></Text>
        //         </View>
        //     </View>
        // </View>