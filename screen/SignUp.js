import React, { useContext } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, TextInput, TouchableHighlight ,AsyncStorage} from 'react-native'
import { Icon } from 'native-base'
import { CKart } from '../ContextApi'

const SignUp = () => {
    const { bottomModalFunc,handleInputBox,handleSignUp,signInError,signInToken } = useContext(CKart)

    const handleModel=async()=>{
        const signToken = await AsyncStorage.getItem('signToken')
        console.log("signToken",signToken)
    }

    handleModel()
    return (
        <View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                <Text style={{fontWeight:"700"}}>Sign Up </Text>
                <TouchableOpacity onPress={() => bottomModalFunc(null)}>
                    <Icon name="close" type="AntDesign" style={{ color: "red" }} />
                </TouchableOpacity>
            </View>

            <View style={styles.signUpSection}>
                <TextInput
                    placeholder='Enter Email-Id / Username'
                    style={styles.textInput1}
                    keyboardType="email-address"
                    onChangeText={(text) => handleInputBox(text, "signup_username")}
                />
                <TextInput
                    placeholder='Enter Password'
                    style={styles.otpTextInput}
                    secureTextEntry
                    onChangeText={(text) => handleInputBox(text, "signup_password")}
                />
                <TextInput
                    placeholder='Enter Your Postcode'
                    style={{marginTop: 15,height: 50,padding: 10,backgroundColor: '#dfdfdf',color: '#494d5a'}}
                    onChangeText={(text) => handleInputBox(text, "signup_zipcode")}

                    
                /> 
                

            </View>
            
            <TouchableOpacity onPress={()=>{handleSignUp();  }} style={{ backgroundColor: "#F26D21", marginTop: 12 }} >
                <Text style={styles.buttonText}>Sign Up </Text>
            </TouchableOpacity>
            <View style={{ alignItems: 'flex-end', marginTop: 15 }}>
                 <TouchableOpacity   onPress={() => bottomModalFunc('signIn')}>
                        <Text style={styles.link}>Existing User? LOGIN</Text>
                    </TouchableOpacity>
                
            </View>
            <Text style={styles.termsConditions}>By signing in you agree to our <Text style={styles.termsConditionsSub}>terms & conditions</Text></Text>
        </View>
    )
}

export default SignUp





const styles = StyleSheet.create({
    textInput: {
        marginTop: 30,
        height: 50,
        padding: 10,
        backgroundColor: '#dfdfdf',
        color: '#494d5a'
    },
    textInput1: {
        height: 50,
        padding: 10,
        backgroundColor: '#dfdfdf',
        color: '#494d5a'
    },
    button: {
        width: '100%',
        height: 50,
        alignItems: 'center',
        borderRadius: 4,
        marginTop: 20,
    },
    buttonText: {
        color: '#ffffff',
        textAlign: 'center',
        fontSize: 17,
        paddingVertical: 12,
    },

    otpTextInput: {
        marginTop: 15,
        height: 50,
        padding: 10,
        backgroundColor: '#dfdfdf',
        color: '#494d5a'
    },
    termsConditions: {
        marginTop: 30,
        fontSize: 12,
        textAlign: 'center'
    },
    termsConditionsSub: {
        color: '#F26D21',
        fontWeight: '700'
    },
    signUpSection: {
        marginTop: 20
    },
    link: {
        color: 'blue'
    }
});






// import React, { Component } from 'react';
// import { Text, View, TextInput, StyleSheet, TouchableHighlight,TouchableOpacity } from 'react-native';

// const styles = StyleSheet.create({
//     textInput: {
//         marginTop: 30,
//         height: 50,
//         padding: 10,
//         backgroundColor: '#dfdfdf',
//         color: '#494d5a'
//     },
//     textInput1: {
//         height: 50,
//         padding: 10,
//         backgroundColor: '#dfdfdf',
//         color: '#494d5a'
//     },
//     button: {
//         width: '100%',
//         height: 50,
//         alignItems: 'center',
//         borderRadius: 4,
//         marginTop: 20,
//     },
//     buttonText: {
//         color: '#ffffff',
//         textAlign: 'center',
//         fontSize: 17,
//         paddingVertical: 12,
//     },
//     buttonsSection: {
//         width: '100%',
//         flexDirection: 'row',
//         justifyContent: 'space-between',
//         marginTop: 15
//     },
//     button1: {
//         width: 156,
//         height: 50,
//         alignItems: 'center',
//         borderRadius: 4,
//         backgroundColor: '#499142'
//     },
//     buttonText1: {
//         color: '#ffffff',
//         textAlign: 'center',
//         fontSize: 15,
//         paddingVertical: 14,
//     },
//     otpTextInput: {
//         marginTop: 15,
//         height: 50,
//         padding: 10,
//         backgroundColor: '#dfdfdf',
//         color: '#494d5a'
//     },
//     termsConditions: {
//         marginTop: 30,
//         fontSize: 12,
//         textAlign: 'center'
//     },
//     termsConditionsSub: {
//         color: '#F26D21',
//         fontWeight: '700'
//     },
//     signUpSection: {
//         marginTop: 20
//     },
//     mobileNoText: {
//         fontSize: 11,
//         fontWeight: '300',
//         color: '#7a7d88'
//     },
//     mobileNoDisplaySection: {
//         flexDirection: 'row',
//         justifyContent: "space-between",
//         paddingVertical: 8
//     },
//     mobileNoValue: {
//         fontWeight: '500',
//         color: '#494d5a',
//     },
//     editText: {
//         fontSize: 12,
//         fontWeight: '500',
//         color: '#af0007'
//     },
//     link: {
//         color: 'blue'
//     }
// });

// export default class SignUp extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             email: '',
//             zipcode: 0,
//             password: '',
            
//         }
//     }

//     handleMobileNumber = (email) => {
//         this.setState({ email });
//     }

//     // handleSignIn = () => {
//     //     this.setState({ sign_in_mode:true,forgot_password_mode: false,login_mode: false, });
//     // }
//     // handleforgotPwd = () => {
//     //     this.setState({ sign_in_mode:true,forgot_password_mode: false,login_mode: false, });
//     // }
//     // handleLogIn = () => {
//     //     this.setState({ sign_in_mode:true,forgot_password_mode: false,login_mode: false, });
//     // }

//     handlePassword = (password) => {
//         this.setState({ password });
//     }
//     handleZipcode = (zipcode) => {
//         this.setState({ zipcode });
//     }


//     // handleOTPButton = () => {
//     //     this.setState({ passwordLogin: false });
//     // }

//     // handlePasswordButton = () => {
//     //     this.setState({ passwordLogin: true });
//     // }

//     render() {
//         const { email, passwordLogin, zipcode, password,bottomModalFunc } = this.state;
//         const { handleProceed, proceedButton, editButton, handleEdit,handleLogIn } = this.props;
//         const buttonbackgroundStyle = {
//             backgroundColor: email !='' && password != '' && zipcode != 0 ? '#F26D21' : '#afafaf',
//         }
//         const otpButtonbackgroundStyle = {
//             backgroundColor: !passwordLogin ? '#F26D21' : '#afafaf',
//         }
//         const passwordButtonbackgroundStyle = {
//             backgroundColor: passwordLogin ? '#F26D21' : '#afafaf',
//         }
//         return (
            
//                 <View>
//                                 <Text>SignUp </Text>
//                                 <View style={styles.signUpSection}>
                                        
//                                         <TextInput
//                                             placeholder='Enter Email-Id / Username'
//                                             style={styles.textInput1}
//                                             keyboardType="email-address"
//                                             onChangeText={this.handleMobileNumber}
//                                             value={email}
//                                         />
//                                         <TextInput
//                                             placeholder='Enter Password'
//                                             secureTextEntry
//                                             style={{marginTop: 15,height: 50,padding: 10,backgroundColor: '#dfdfdf',color: '#494d5a'}}
                                            
//                                             onChangeText={this.handlePassword}
//                                             value={password}
//                                         />
//                                         <TextInput
//                                             placeholder='Enter Your Postcode'
//                                             keyboardType="numeric"
//                                             maxLength={6}
//                                             style={{marginTop: 15,height: 50,padding: 10,backgroundColor: '#dfdfdf',color: '#494d5a'}}
//                                             onChangeText={this.handleZipcode}
//                                             value={zipcode}
//                                         /> 
                                        
//                                     </View>
                                              
//                                 <TouchableHighlight underlayColor="white" onPress={() => { (email !='' && password !='') ? handleProceed() : alert('Sign Up') }}>
//                                     <View style={[styles.button, buttonbackgroundStyle]}>
//                                         <Text style={styles.buttonText}>Sign Up </Text>
//                                     </View>
//                                 </TouchableHighlight>
//                                 <View style={{alignItems: 'flex-end',marginTop:15}}>
//                     <TouchableOpacity   onPress={() => bottomModalFunc('signIn')}>
//                         <Text style={styles.link}>Existing User? LOGIN</Text>
//                     </TouchableOpacity>
//                 </View>
                                
//                                 <Text style={styles.termsConditions}>By signing in you agree to our <Text style={styles.termsConditionsSub}>terms & conditions</Text></Text>
//                                 </View>
            
//         )
//     }
// }