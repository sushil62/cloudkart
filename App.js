import React, { useState } from 'react'
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import { CKartProvider } from './ContextApi'
import { Ionicons } from '@expo/vector-icons';
import { createAppContainer, createSwitchNavigator, createStackNavigator, createBottomTabNavigator, createDrawerNavigator } from 'react-navigation'
import AuthLoadingScreen from './screen/AuthLoadingScreen';
import Welcome from './screen/WelcomeScreen';
import SignIn from './screen/SignIn/SignIn';
import SignUp from './screen/SignUp/SignUp';
import HomeScreen from './Components/HomeScreen/HomeScreen';
import SelectButton from './Components/AccountScreen/SelectButton';
import ProfileInfo from './Components/AccountScreen/ProfileInfo';
import ManageAddress from './Components/AccountScreen/ManageAddress/ManageAddress';
import NewAddress from './Components/AccountScreen/ManageAddress/NewAddress';
import UpdateAddress from './Components/AccountScreen/ManageAddress/UpdateAddress';

import SettingScreen from './Components/SettingScreen/SettingScreen'
import SideBar from './Components/DrawerNav/Sidebar'

import ProductDetailScreen from './Components/HomeScreen/ProductDetailScreen';
import ProductListScreen from './Components/HomeScreen/ProductListScreen';
import ShpByVendorScreen from './Components/HomeScreen/ShpByVendorScreen';
import ShpByCatScreen from './Components/HomeScreen/ShpByCatScreen';
import ShpByBrandScreen from './Components/HomeScreen/ShpByBrandScreen';
import Filter from './Components/HomeScreen/Filter';
import Cart from './Components/HomeScreen/Cart/Cart';
import Checkout from './Components/HomeScreen/Cart/Checkout';
import Payment from './Components/HomeScreen/Cart/Payment';
import AddNewCard from './Components/HomeScreen/Cart/AddNewCard';

import ReviewScreen from './Components/HomeScreen/ProductDetailScreen/ReviewScreen';

import WishListScreen from './Components/HomeScreen/WishList/WishListScreen';
import OrderListScreen from './Components/HomeScreen/OrderList/OrderListScreen';
import ViewDetailScreen from './Components/HomeScreen/OrderList/ViewDetailScreen';
import OrderSuccess from './Components/HomeScreen/OrderSuccess';
import SearchField from './Components/HomeScreen/SearchField';

const fetchFonts = () => {
  return Font.loadAsync({
    pf: require('./assets/fonts/pf.ttf'),
  })
}

const App = () => {
  const [dataLoaded, setDataLoaded] = useState(false)

  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setDataLoaded(true)}
      />
    )

  }

  return (
    <CKartProvider>
      <SwitchNavigators />
    </CKartProvider>
  )
}

const HomeStackNavigator = createStackNavigator({
  HomeScreen,
  ShpByCatScreen,
  ShpByVendorScreen,
  ShpByBrandScreen,
  ProductListScreen,
  ProductDetailScreen,
  ReviewScreen,
  Cart,
  Payment,
  AddNewCard,
  Filter,
  Checkout,
  OrderSuccess,
  SearchField,
  // HomeScreen,
  ManageAddress,
  NewAddress,
  UpdateAddress,
  ProfileInfo,
  WishListScreen
}, {
  navigationOptions: {
    gesturesEnabled: false
  }, headerMode: "none"
})

HomeStackNavigator.navigationOptions = ({ navigation }) => {

  let tabBarVisible = true;

  let routeName = navigation.state.routes[navigation.state.index].routeName

  const nonTabBarPages = ['ProductDetailScreen', 'Cart', 'Checkout', 'Payment', 'AddNewCard', 'Filter', 'OrderSuccess', 'SearchField']
  if (nonTabBarPages.includes(routeName)) {
    tabBarVisible = false
  }
  if (routeName == 'ReviewScreen') {
    tabBarVisible = false
  }
  // if (routeName == 'Checkout' ) {
  //   tabBarVisible = false
  // }

  return {
    tabBarVisible,
  }
}


const AccountStackNavigator = createStackNavigator({
  SelectButton,
  HomeScreen,
  ManageAddress,
  NewAddress,
  UpdateAddress,
  ProfileInfo,
  

}, {
  navigationOptions: {
    gesturesEnabled: false
  }, headerMode: "none"
})

AccountStackNavigator.navigationOptions = ({ navigation }) => {

  let tabBarVisible = true;

  let routeName = navigation.state.routes[navigation.state.index].routeName

  if (routeName == 'ProductDetailScreen') {
    tabBarVisible = false
  }
  if (routeName == 'Cart') {
    tabBarVisible = false
  }

  return {
    tabBarVisible,
  }
}

const OrderListStackNavigator = createStackNavigator({
  OrderListScreen,
  ViewDetailScreen,


}, {
  navigationOptions: {
    gesturesEnabled: false
  }, headerMode: "none"
})

OrderListStackNavigator.navigationOptions = ({ navigation }) => {

  let tabBarVisible = true;

  let routeName = navigation.state.routes[navigation.state.index].routeName

  if (routeName == 'ProductDetailScreen') {
    tabBarVisible = false
  }
  if (routeName == 'Cart') {
    tabBarVisible = false
  }

  return {
    tabBarVisible,
  }
}



// const AppTabNavigator = createBottomTabNavigator({
//   Homes: {
//     screen: HomeStackNavigator,
//     navigationOptions: {
//       tabBarLabel: 'Home',
//       tabBarIcon: ({ tintColor }) => (
//         <Ionicons name="ios-home" color={tintColor} size={22} />
//       )
//     }
//   },
//   OrderList: {
//     screen: OrderListStackNavigator,
//     navigationOptions: {
//       tabBarLabel: 'OrderList',
//       tabBarIcon: ({ tintColor }) => (
//         <Ionicons name="ios-cart" color={tintColor} size={22} />
//       )
//     }
//   },

//   Wishlist: {
//     screen: WishListScreen,
//     navigationOptions: {
//       tabBarLabel: 'Wishlist',
//       tabBarIcon: ({ tintColor }) => (
//         <Ionicons name="ios-heart" color={tintColor} size={22} />
//       )
//     }
//   },
//   Account: {
//     screen: AccountStackNavigator,
//     navigationOptions: {
//       tabBarLabel: 'Account',
//       tabBarIcon: ({ tintColor }) => (
//         <Ionicons name="ios-person" color={tintColor} size={22} />
//       )
//     }
//   },
// })

const AppStackNavigator = createStackNavigator({
  Home: HomeStackNavigator,
}, {
  navigationOptions: {
    gesturesEnabled: false
  }, headerMode: "none"
})

const AppDrawerNavigator = createDrawerNavigator({
  Dashboard: {
    screen: AppStackNavigator,
  }
},
  {
    contentComponent: props => <SideBar {...props} />,
    drawerLockMode: 'locked-closed',
  })


const AuthStackNavigator = createStackNavigator({
  Welcome,
  SignIn,
  SignUp,

}, {
  navigationOptions: {
    gesturesEnabled: false
  }, headerMode: "none"
})

const SwitchNav = createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  Auth: AuthStackNavigator,
  App: AppDrawerNavigator
})

const SwitchNavigators = createAppContainer(SwitchNav)

export default App
