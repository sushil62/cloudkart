import React from 'react'
import {Text,TouchableOpacity } from 'react-native'
import { Icon } from 'native-base'
import {Styles} from '../Stylesheet'
import {withNavigation} from 'react-navigation'

const BackButton=({navigation,title})=>{
    const {BackIconName,BackIconType} = Styles
    return (
        <TouchableOpacity onPress={()=>navigation.goBack()} style={{flexDirection:"row",alignItems:"center"}}>
            <Icon type={BackIconType} name={BackIconName} style={{fontSize:24}}/>
            <Text style={{fontWeight:"700"}}>{title}</Text>
        </TouchableOpacity>
    )
}
export default withNavigation(BackButton)
