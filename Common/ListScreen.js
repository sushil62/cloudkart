import React, { useContext } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import ScrollAbleMenu from '../Components/HomeScreen/ScrollAbleMenu'
import SearchBox from '../Components/HomeScreen/SearchBox'
import { Icon } from 'native-base'


import { withNavigation } from 'react-navigation'
import { CKart } from '../ContextApi'

const ListScreen = ({ children, navigation, pressFunc, iconType, iconName, headerTitle, iconSize }) => {
  const { modalVisible, setModalVisible, signInToken, bottomModalFunc, cart_data,vendor_slug } = useContext(CKart)
  
  const cart_length = cart_data !== undefined ? cart_data.cart_items !== undefined ? cart_data.cart_items : null : null

  return (
    <>
      <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>

    
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <TouchableOpacity onPress={pressFunc}>
          <Icon type={iconType} name={iconName} style={{ color: "#fff", fontSize: iconSize }} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Homes')}>
          <Text style={{ color: "#fff", paddingLeft: 12, fontSize: 16, fontWeight: "700" }}>{headerTitle}</Text>
        </TouchableOpacity>
      </View>
        <View style={{ flexDirection: "row", alignItems: "center" }}>

      <TouchableOpacity onPress={() => navigation.navigate('Cart')} style={{ marginHorizontal: 24, position: 'relative' }}>
            <Icon type="Feather" name="shopping-cart" style={{ color: "#fff", fontSize: 19 }} />
            {cart_length ? <View style={{ backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center', padding: 4, borderRadius: 10, position: "absolute", right: -10, top: -10, width: 20, height: 20 }}><Text style={{ fontSize: 10 }}>{cart_data.cart_items.length}</Text></View> : null}
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
            <Icon type="AntDesign" name="bells" style={{ color: "#fff", fontSize: 18 }} />
          </TouchableOpacity>
        </View>
      
      </View>
      <View style={{ flex: 1 }}>
        {children}
      </View>
    </>

  )
}
export default withNavigation(ListScreen)

