import React, { useContext } from 'react';
import {StyleSheet, View} from 'react-native';
import Modal from 'react-native-modal';
import { CKart } from '../ContextApi'



const CustomModal = ({children,modalNumber,justifyCt,btmTopLeftRad,btmTopRightRad,btmBottomRightRad,btmBottomLeftRad,mrgn,animIn,animDown,cPadding}) => {
    const { bottomModal, bottomModalFunc } = useContext(CKart)
    return (
        <Modal isVisible={bottomModal === modalNumber} style={[styles.bottomModal,{justifyContent: justifyCt,margin:mrgn}]}
            animationIn={animIn}
            animationOut={animDown}
            backdropOpacity={0.5}
            backdropTransitionOutTiming={30}
            onBackdropPress={()=>bottomModalFunc(null)}
        >
            <View style={[styles.modalContent,{borderTopLeftRadius: btmTopLeftRad,
            borderTopRightRadius: btmTopRightRad,borderBottomLeftRadius:btmBottomLeftRad,borderBottomRightRadius:btmBottomRightRad,padding:cPadding}]}>
                {children}
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create({
    modalContent: {
        backgroundColor: 'white',
        borderColor: 'rgba(160, 160, 160, 0.1)',
        paddingTop:30
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    }
});

export default CustomModal;