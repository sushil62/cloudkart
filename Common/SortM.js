import React, { useContext } from 'react';
import { Text, View, TextInput, StyleSheet, TouchableHighlight,TouchableOpacity,Dimensions } from 'react-native';
import { CKart } from '../ContextApi'
// import { Icon } from 'native-base'
import { Icon, Button, CheckBox } from 'native-base'

import { Styles } from '../Stylesheet'


const styles = StyleSheet.create({
    textInput: {
        marginTop: 30,
        height: 50,
        padding: 10,
        backgroundColor: '#dfdfdf',
        color: '#494d5a'
    },
    textInput1: {
        height: 50,
        padding: 10,
        backgroundColor: '#dfdfdf',
        color: '#494d5a'
    },
    button: {
        width: '100%',
        height: 50,
        alignItems: 'center',
        borderRadius: 4,
        marginTop: 20,
    },
    buttonText: {
        color: '#ffffff',
        textAlign: 'center',
        fontSize: 17,
        paddingVertical: 12,
    },
    buttonsSection: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 15
    },
    button1: {
        width: 156,
        height: 50,
        alignItems: 'center',
        borderRadius: 4,
        backgroundColor: '#499142'
    },
    buttonText1: {
        color: '#ffffff',
        textAlign: 'center',
        fontSize: 15,
        paddingVertical: 14,
    },
    otpTextInput: {
        marginTop: 15,
        height: 50,
        padding: 10,
        backgroundColor: '#dfdfdf',
        color: '#494d5a'
    },
    termsConditions: {
        marginTop: 30,
        fontSize: 12,
        textAlign: 'center'
    },
    termsConditionsSub: {
        color: '#F26D21',
        fontWeight: '700'
    },
    signUpSection: {
        marginTop: 20
    },
    mobileNoText: {
        fontSize: 11,
        fontWeight: '300',
        color: '#7a7d88'
    },
    mobileNoDisplaySection: {
        flexDirection: 'row',
        justifyContent: "space-between",
        paddingVertical: 8
    },
    mobileNoValue: {
        fontWeight: '500',
        color: '#494d5a',
    },
    editText: {
        fontSize: 12,
        fontWeight: '500',
        color: '#af0007'
    },
    link: {
        color: 'blue'
    }
});

const SortM = () => {
    const { bottomModalFunc,loading_true,handleInputBox,handleSignIn,signInError,signInToken,resetSortFilter,sort_text,handlePostcode,Postcode_data,setSortFilterData,fatchFilteredDataApi ,setFilterData,facetednav} = useContext(CKart)

    // const handleModel=async()=>{
    //     const signToken = await AsyncStorage.getItem('signToken')
    //     console.log("signToken",signToken)
    // }

    // handleModel()
    const { pColorOne } = Styles
       const { width, height } = Dimensions.get('window')
    const sWidth = width / 2 - (width / 20)

    return (
        <View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                <Text style={{fontWeight:"700"}}>Sort By</Text>
                <TouchableOpacity onPress={() => bottomModalFunc(null)}>
                    <Icon name="close" type="AntDesign" style={{ color: "red" }} />
                </TouchableOpacity>
            </View>

            {/*<View style={styles.signUpSection}>
            
            
                        
                            <TextInput
                                placeholder='Enter Your Postcode'
                                style={styles.textInput1}
                                // keyboardType="email-address"
                                onChangeText={(text) => handleInputBox(text, "postcode")}
                            />
                            
                        </View>
                       
                        <Text style={styles.termsConditionsSub}>{Postcode_data !== undefined ?  Postcode_data.message  :null}</Text>
                        <TouchableOpacity onPress={() => handlePostcode() } style={{ backgroundColor: "#F26D21", marginTop: 12 }} >
                            <Text style={styles.buttonText}>DELIVER ? </Text>
                                <CheckBox onPress={() => setFilterData(filterType, val)} style={{ borderColor: "black", backgroundColor: filterData[filterType] && filterData[filterType].includes(val) ? "black" : "#fff" }} checked={filterData[filterType] && filterData[filterType].includes(val)} />
                        </TouchableOpacity>*/}
             {facetednav.sort_facet.map(val => {
                        return (
                            <TouchableOpacity style={{  backgroundColor: val.text == sort_text? "#F26D21":null,flexDirection: "row", paddingHorizontal: 5, paddingVertical: 10, }}
                             onPress={() => {setSortFilterData("sort_facet", facetednav.sort_facet,val.text);fatchFilteredDataApi();loading_true();bottomModalFunc(null) }}
                                    // {filterType === 'price_range_facet' && `${val["min-price"]} - ${val["max-price"]}`}
                                    //  
                             >
                                <Text style={{ paddingHorizontal: 20, }}>
                                    {val.name}
                                    {val.title}
                                    {val.text}

                                </Text>
                            </TouchableOpacity>
                        )
                    })}
            { /* */}<View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", borderTopWidth: 0, marginVertical: 10, marginHorizontal: 15 }}>
                    <Button onPress={() => {resetSortFilter("sort_facet");loading_true();bottomModalFunc(null)}} style={{ backgroundColor: pColorOne, width: sWidth, height: 40, justifyContent: "center" }}>
                        <Text style={{ fontSize: 13, }}>RESET</Text>
                    </Button>
        </View>
        </View>
    )
}

export default SortM