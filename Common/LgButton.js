import React from 'react'
import { View, Text,TouchableOpacity } from 'react-native'
import {Styles} from '../Stylesheet'

export default function LgButton({title,bgColor,color,pressFunc,disable}) {
    const {pFontColor} = Styles
    return (
        <TouchableOpacity disabled={disable} onPress={pressFunc} style={{padding:12, borderWidth:1,borderColor:"#ffff",borderRadius:8,alignItems:"center",backgroundColor:bgColor}}>
            <Text style={{color}}>{title}</Text>
        </TouchableOpacity>
    )
}
