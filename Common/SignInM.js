import React,{useContext} from 'react'
import { View, Text } from 'react-native'
import CustomModal from './CustomModal'
import SignInOrSignUp from '../screen/SignInOrSignUp'
import { CKart } from '../ContextApi'
import SignUp from '../screen/SignUp'
import ForgotPwd from '../screen/SignUp/ForgotPwd'
import Zipcode from './Zipcode'
import SortM from './SortM'


const SignInM = () => {
    return (
    	<>
        <CustomModal cPadding={40} modalNumber={"signIn"} justifyCt="flex-end" btmTopLeftRad={30} btmTopRightRad={30}>
            <SignInOrSignUp/>
        </CustomModal>
        <CustomModal cPadding={40} modalNumber={"signUp"} justifyCt="flex-end" btmTopLeftRad={30} btmTopRightRad={30}>
            <SignUp/>
        </CustomModal>
        <CustomModal cPadding={40} modalNumber={"forgotpwd"} justifyCt="flex-end" btmTopLeftRad={30} btmTopRightRad={30}>
            <ForgotPwd/>
        </CustomModal>
        <CustomModal cPadding={40} modalNumber={"zipcode"} justifyCt="flex-end" btmTopLeftRad={30} btmTopRightRad={30}>
            <Zipcode />
        </CustomModal>
         <CustomModal cPadding={40} modalNumber={"sorting"} justifyCt="flex-end" btmTopLeftRad={30} btmTopRightRad={30}>
            <SortM />
        </CustomModal>
    	</>
    )
}

export default SignInM
