import React from 'react'
import { View, Text } from 'react-native'
import {Styles} from '../Stylesheet'

export default function Circle({CColor,size=50}) {
    const {pColorThree} = Styles
    return (
        <View style={{width:size,height:size,backgroundColor:CColor,borderRadius:size/2}}/>
    )
}
