import React, { useState, useContext } from 'react'
import { View, Text, TouchableOpacity, Modal, StyleSheet, Image, TouchableHighlight } from 'react-native'
import ScrollAbleMenu from '../Components/HomeScreen/ScrollAbleMenu'
import StoreList from '../Components/HomeScreen/StoreList'

import SearchBox from '../Components/HomeScreen/SearchBox'
import { Icon } from 'native-base'
import SignInM from './SignInM'

import { CKart } from '../ContextApi'

import { withNavigation } from 'react-navigation'

const HeaderMenu = ({ children,children2, navigation, pressFunc, iconType, iconName, headerTitle, iconSize, }) => {
  const { modalVisible, setModalVisible, signInToken, bottomModalFunc, cart_data,vendor_slug,valid_postcode } = useContext(CKart)
  // console.log(vendor_slug,"vendor_slug")

  const cart_length = cart_data !== undefined ? cart_data.cart_items !== undefined ? cart_data.cart_items : null : null

  return (
    <>
      <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>

        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <TouchableOpacity onPress={pressFunc}>
            <Icon type={iconType} name={iconName} style={{ color: "#fff", fontSize: iconSize }} />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate('Homes')}>
            <Text style={{ color: "#fff", paddingLeft: 12, fontSize: 16, fontWeight: "700" }}>{headerTitle}</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          {signInToken === null &&
            <TouchableOpacity style={{}} onPress={() => bottomModalFunc("signIn")}>
              <Icon type="AntDesign" name="user" style={{ color: "#fff", fontSize: 19 }} />
            </TouchableOpacity>
          }

          <TouchableOpacity onPress={() => navigation.navigate('Cart')} style={{ marginHorizontal: 24, position: 'relative' }}>
            <Icon type="Feather" name="shopping-cart" style={{ color: "#fff", fontSize: 19 }} />
            {cart_length ? <View style={{ backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center', padding: 4, borderRadius: 10, position: "absolute", right: -10, top: -10, width: 20, height: 20 }}><Text style={{ fontSize: 10 }}>{cart_data.cart_items.length}</Text></View> : null}
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
            <Icon type="AntDesign" name="bells" style={{ color: "#fff", fontSize: 18 }} />
          </TouchableOpacity>
        </View>
      </View>
      <View style={{ flex: 1 }}>
        {children2}
        <SearchBox />
         <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 14, backgroundColor: "#F9F8F8" }}>
                <Icon type="MaterialIcons" name="location-on" style={{ fontSize: 17, color: '#999', paddingLeft: 10, }} />
                <Text style={{ fontSize: 17, color: '#777' }}> Deliver to <Text style={{color:'#FF69B4'}}>{valid_postcode}</Text></Text>
                <TouchableOpacity onPress={() => bottomModalFunc("zipcode")}>
                  <Icon type="Entypo" name="chevron-down" style={{ fontSize: 23, marginTop: 5 }} />
                </TouchableOpacity>
              </View>
        {children}
      </View>
      <SignInM />
    </>

  )
}

const styles = StyleSheet.create({
  rightIcon: {
    marginRight: 15
  },
  rightIconSection: {
    flexDirection: 'row',
    marginTop: 10
  },
  closeIcon: {
    marginVertical: 15,
    marginHorizontal: 30
  },
  logo: {
    marginVertical: 10,
    marginHorizontal: 30
  },
  cartCount: {
    width: 22,
    height: 22,
    borderRadius: 50,
    textAlign: 'center',
    color: '#ffffff',
    backgroundColor: '#499142',
    position: 'absolute',
    top: -12,
    right: 8
  },
  cartCountRed: {
    backgroundColor: '#ff555c'
  },
  cartCountText: {
    textAlign: 'center',
    color: '#ffffff'
  },
  modalTransperency: {
    flex: 1,
    justifyContent: 'center',
    alignItems: "center"
  },
  modalSection: {
    backgroundColor: "white",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 35,
    fontSize: 16,
    width: '100%',
    height: 400
  },
  modalLogoClose: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  message: {
    backgroundColor: '#f3f3f3',
    borderColor: '#F26D21',
    borderWidth: 1,
    borderStyle: 'solid',
    width: 354,
    height: 53,
    borderRadius: 26,
    marginVertical: 20,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  messageText: {
    fontSize: 12,
    fontWeight: '500',
    color: '#F26D21'
  }

});
export default withNavigation(HeaderMenu)

