import React, { useContext } from 'react';
import { Text, View, TextInput, StyleSheet, TouchableHighlight,TouchableOpacity } from 'react-native';
import { CKart } from '../ContextApi'
import { Icon } from 'native-base'

const styles = StyleSheet.create({
    textInput: {
        marginTop: 30,
        height: 50,
        padding: 10,
        backgroundColor: '#dfdfdf',
        color: '#494d5a'
    },
    textInput1: {
        height: 50,
        padding: 10,
        backgroundColor: '#dfdfdf',
        color: '#494d5a'
    },
    button: {
        width: '100%',
        height: 50,
        alignItems: 'center',
        borderRadius: 4,
        marginTop: 20,
    },
    buttonText: {
        color: '#ffffff',
        textAlign: 'center',
        fontSize: 17,
        paddingVertical: 12,
    },
    buttonsSection: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 15
    },
    button1: {
        width: 156,
        height: 50,
        alignItems: 'center',
        borderRadius: 4,
        backgroundColor: '#499142'
    },
    buttonText1: {
        color: '#ffffff',
        textAlign: 'center',
        fontSize: 15,
        paddingVertical: 14,
    },
    otpTextInput: {
        marginTop: 15,
        height: 50,
        padding: 10,
        backgroundColor: '#dfdfdf',
        color: '#494d5a'
    },
    termsConditions: {
        marginTop: 30,
        fontSize: 12,
        textAlign: 'center'
    },
    termsConditionsSub: {
        color: '#F26D21',
        fontWeight: '700'
    },
    signUpSection: {
        marginTop: 20
    },
    mobileNoText: {
        fontSize: 11,
        fontWeight: '300',
        color: '#7a7d88'
    },
    mobileNoDisplaySection: {
        flexDirection: 'row',
        justifyContent: "space-between",
        paddingVertical: 8
    },
    mobileNoValue: {
        fontWeight: '500',
        color: '#494d5a',
    },
    editText: {
        fontSize: 12,
        fontWeight: '500',
        color: '#af0007'
    },
    link: {
        color: 'blue'
    }
});

const Zipcode = () => {
    const { bottomModalFunc,handleInputBox,handleSignIn,signInError,signInToken,handlePostcode,Postcode_data } = useContext(CKart)

    // const handleModel=async()=>{
    //     const signToken = await AsyncStorage.getItem('signToken')
    //     console.log("signToken",signToken)
    // }

    // handleModel()
    return (
        <View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                <Text style={{fontWeight:"700"}}>To see delivery options for your location </Text>
                <TouchableOpacity onPress={() => bottomModalFunc(null)}>
                    <Icon name="close" type="AntDesign" style={{ color: "red" }} />
                </TouchableOpacity>
            </View>

            <View style={styles.signUpSection}>
                <TextInput
                    placeholder='Enter Your Postcode'
                    style={styles.textInput1}
                    // keyboardType="email-address"
                    onChangeText={(text) => handleInputBox(text, "postcode")}
                />
                
            </View>
           
            <Text style={styles.termsConditionsSub}>{Postcode_data !== undefined ?  Postcode_data.message  :null}</Text>
            <TouchableOpacity onPress={() => handlePostcode() } style={{ backgroundColor: "#F26D21", marginTop: 12 }} >
                <Text style={styles.buttonText}>DELIVER ? </Text>
            </TouchableOpacity>
            
        </View>
    )
}

export default Zipcode
