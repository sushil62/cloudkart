import React from 'react'
import { View, Text } from 'react-native'
import {Styles} from '../Stylesheet'

export default function CircleTr({CColor="#000",size=50,bWidth=4}) {
    const {pColorThree} = Styles
    return (
        <View style={{width:size,height:size,borderRadius:size/2,borderWidth:bWidth,borderColor:CColor}}/>
    )
}
