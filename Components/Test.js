import React,{useContext} from 'react'
import { View, Text,TouchableOpacity } from 'react-native'
import {CKart} from '../ContextApi'

export default function Test() {
    const {handleSignIn} = useContext(CKart)
    return (
        <View>
        <TouchableOpacity onPress={()=>handleSignIn()}>
            <Text>Sign in data</Text>
        </TouchableOpacity>
        </View>
    )
}
