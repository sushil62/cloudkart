import React, { useContext } from "react";
import { View, Text, StatusBar, SafeAreaView, StyleSheet, TouchableOpacity, Image } from "react-native";
import { Thumbnail, Icon } from 'native-base'
import { CKart } from '../../ContextApi'
import { Styles } from '../../Stylesheet'
import AsyncStorage from '@react-native-community/async-storage';


const SideBar = ({ navigation }) => {
    const { email, clearData, LogoutHandle, signInToken } = useContext(CKart)
    const { pColorOne } = Styles

    const logout = async () => {
        // clearData();
        LogoutHandle();
        navigation.navigate('HOME');
        await AsyncStorage.clear();
        navigation.closeDrawer();
    }

    return (
        <View style={styles.container}>
            <View style={styles.navigationBar}>
                <View style={{ paddingLeft: 20, paddingTop: 25 }}>
                    <Image
                        source={require('../../assets/icon.png')}
                        style={{ width: 30, height: 20, resizeMode: "stretch", }}
                    />
                </View>
                <Text style={styles.navigationText}>Cloudkart360</Text>
            </View>
            {/* <View style={styles.locationSection}>
                <Icon type="MaterialIcons" name="location-on" style={styles.locationIcon} />
                <Text style={styles.locationText}><Text style={styles.locationTextBold}>HOME:</Text> #30, 1st cross Hennur, Bangalore..</Text>
                <TouchableOpacity underlayColor="white">
                    <Icon type="AntDesign" name="edit" style={styles.editIcon} />

                </TouchableOpacity>
            </View> */}
            <View style={styles.menuSectionContainer}>
                <TouchableOpacity underlayColor="white" onPress={() => { navigation.navigate('HomeScreen'); navigation.closeDrawer(); }}>
                    <View style={styles.menuSection}>

                        <Icon type="FontAwesome" name="home" style={styles.menuImage} />
                        <Text style={styles.menuText}>HOME</Text>
                    </View>
                </TouchableOpacity>
                {signInToken !== null &&
                    <>

                        <View style={styles.profileSection}>
                            <TouchableOpacity underlayColor="white" onPress={() => navigation.navigate('ProfileInfo')}>
                                <View style={styles.profileMobileSection}>

                                    <Icon type="FontAwesome" name="user" style={styles.menuImage} />
                                    <Text style={styles.menuText}>PROFILE</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity underlayColor="white" >
                                <Text style={styles.menuTextRight}>UPDATE</Text>
                            </TouchableOpacity>
                        </View>
                        {/*<TouchableOpacity underlayColor="white" onPress={() => navigation.navigate('OrderListScreen')}>
                                                    <View style={styles.menuSection}>
                                                        <Icon type="Feather" name="package" style={styles.menuImage} />
                                                        <Text style={styles.menuText}>ORDERS</Text>
                                                    </View>
                                                </TouchableOpacity>*/}

                        <TouchableOpacity underlayColor="white" onPress={() => navigation.navigate('OrderListScreen')}>
                            <View style={styles.menuSection}>
                                <Icon type="Feather" name="package" style={styles.menuImage} />
                                <Text style={styles.menuText}>ORDERS</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity underlayColor="white" onPress={() => navigation.navigate('WishListScreen')}>
                            <View style={styles.menuSection}>
                                <Icon type="Feather" name="heart" style={styles.menuImage} />
                                <Text style={styles.menuText}>WishList</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity underlayColor="white" onPress={() => navigation.navigate('ManageAddress')}>
                            <View style={styles.menuSection}>
                                <Icon type="Entypo" name="location" style={styles.menuImage} />
                                <Text style={styles.menuText}>ADDRESS</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity underlayColor="white" onPress={() => navigation.navigate('HomeScreen')}>
                            <View style={styles.menuSection}>

                                <Icon type="AntDesign" name="wallet" style={styles.menuImage} />

                                <Text style={styles.menuText}>PAYMENTS</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity underlayColor="white" onPress={() => navigation.navigate('HomeScreen')}>
                            <View style={styles.menuSection}>

                                <Icon type="FontAwesome5" name="user-astronaut" style={styles.menuImage} />

                                <Text style={styles.menuText}>SUPPORT</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity underlayColor="white" onPress={() => navigation.navigate('HomeScreen')}>
                            <View style={styles.menuSection}>

                                <Icon type="Octicons" name="comment-discussion" style={styles.menuImage} />
                                <Text style={styles.menuText}>FAQS</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity underlayColor="white" onPress={() => logout()}>
                            <View style={styles.menuSection}>

                                <Icon type="AntDesign" name="logout" style={styles.menuImage} />
                                <Text style={styles.menuText}>Log out</Text>
                            </View>
                        </TouchableOpacity>
                    </>

                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff'
    },
    navigationBar: {
        flexDirection: 'row',
        width: '100%',
        height: 70,
        backgroundColor: '#494d5a',
        alignItems: 'center'
    },
    backArrow: {
        marginTop: 5
    },
    navigationText: {
        fontSize: 18,
        fontWeight: '500',
        color: '#ffffff',
        marginHorizontal: 10,
        marginTop: 25
    },
    locationSection: {
        backgroundColor: '#F26D21',
        flexDirection: 'row',
        paddingHorizontal: 15,
        paddingVertical: 20
    },
    locationIcon: {
        marginRight: 10,
        fontSize: 23,
    },
    locationText: {
        width: 150,
        color: '#ffffff'
    },
    locationTextBold: {
        fontWeight: '700',
    },
    editIcon: {
        marginVertical: 10,
        marginLeft: 20,
        fontSize: 23,
    },
    menuSectionContainer: {
        marginTop: 12
    },
    menuSection: {
        flexDirection: 'row',
        paddingVertical: 18,
        marginHorizontal: 20,
        borderBottomColor: '#cfcfcf',
        borderBottomWidth: 1,
        borderStyle: 'solid',
    },
    profileSection: {
        flexDirection: 'row',
        marginHorizontal: 20,
        borderBottomColor: '#cfcfcf',
        borderBottomWidth: 1,
        borderStyle: 'solid',
    },
    profileMobileSection: {
        flexDirection: 'row',
        paddingVertical: 18,
    },
    menuImage: {
        fontSize: 20,
        marginRight: 10,
        color: '#F26D21'
    },
    menuText: {
        color: '#313131'
    },
    menuTextRight: {
        marginLeft: 80,
        fontSize: 12,
        fontWeight: '500',
        color: '#ff555c',
        paddingVertical: 18,
    }
});


export default SideBar

// <>
//             <View style={{ marginTop: 30 }}>
//                 <View style={{ backgroundColor: '#494d5a',minHeight: 60, justifyContent: "center", alignItems: "center", padding: 4, borderBottomWidth: 1, borderBottomColor: "#464B4E" }}>
//                     {/*<Thumbnail style={{ height: 100,width:150 }} square large source={require('../../Images/ck360.png')} />*/}
//                     <Text style={{ color: "#fff", fontWeight: "700", fontSize: 20, marginVertical: 12 }}>MENU</Text>
//                 </View>
//                 </View>
//                   <TouchableOpacity style={{ borderBottomWidth:1, }} >

//                 <View style={{paddingHorizontal: 15,
//         paddingVertical: 20,flexDirection:'row',paddingBottom:10,justifyContent:'space-between',backgroundColor: pColorOne}} >
//                 <View style={{ flexDirection:'row'}}>
//                     <Icon type="MaterialIcons" name="location-on" style={{ fontSize: 28, color: "#fff",paddingTop:5 }} />
//                     <View style={{flexDirection:'column'}}>
//                     <Text style={{ fontSize: 17, color: '#fff' }}>Home:#30, 1st cross </Text>
//                     <Text style={{ fontSize: 17, color: '#fff' }}>Hennur, Bangalore.</Text>

//                 </View>
//                 </View>
//                 <View>
//                         <Icon type="AntDesign" name="edit" style={{ fontSize: 23, marginTop: 5 }} />
//                 </View>
//                 </View>
//                 </TouchableOpacity>
//         <View style={{  backgroundColor: '#fff',flex: 1, justifyContent: "space-between" }}>
//                 {/* //----------------------- */}
//                 <TouchableOpacity onPress={()=>navigation.navigate('HomeScreen')}>
//                 <View style={{flexDirection:'row',
//         paddingVertical: 15,
//         marginHorizontal: 20,
//         borderBottomColor: '#cfcfcf',
//         borderBottomWidth: 1,
//         borderStyle: 'solid',}} >
//                     <Icon type="FontAwesome" name="home" style={{ fontSize: 20style={styles.menuImage},paddingRight:8 }} />
//                     <Text style={{ fontWeight: "700", color: "#464B4E" }}>HOME</Text>
//                 </View>
//                 </TouchableOpacity>
//                 <TouchableOpacity onPress={()=>navigation.navigate('ProfileInfo')} >
//                 <View style={{
//         paddingVertical: 15,
//         marginHorizontal: 20,
//         borderBottomColor: '#cfcfcf',
//         borderBottomWidth: 1,
//         borderStyle: 'solid',}}>
//                 <View style={{flexDirection:'row',justifyContent:'space-between'}} >
//                 <View style={{ flexDirection:'row'}}>
//                     <Icon type="FontAwesome" name="user" style={{ fontSize: 20,paddingRight:8 }} />
//                     <Text style={{ fontWeight: "700", color: "#464B4E" }}>PROFILE</Text>
//                 </View>
//                 <View>
//                     <Text style={{ fontWeight: "700", color: "#464B4E" }}>UPDATE</Text>
//                 </View>
//                 </View>
//                 </View>
//                 </TouchableOpacity>
//                 <TouchableOpacity onPress={()=>navigation.navigate('OrderListScreen')} style={{ padding: 10 }} >
//                 <View style={{flexDirection:'row',borderBottomWidth:1,paddingBottom:10}} >
//                     <Icon type="Feather" name="package" style={{ fontSize: 20,paddingRight:8 }} />
//                     <Text style={{ fontWeight: "700", color: "#464B4E" }}>ORDERS</Text>
//                 </View>
//                 </TouchableOpacity>
//                 <TouchableOpacity onPress={()=>navigation.navigate('ManageAddress')} style={{ padding: 10 }} >
//                 <View style={{flexDirection:'row',borderBottomWidth:1,paddingBottom:10}} >
//                     <Icon type="Entypo" name="location" style={{ fontSize: 20,paddingRight:8 }} />
//                     <Text style={{ fontWeight: "700", color: "#464B4E" }}>ADDRESS</Text>
//                 </View>
//                 </TouchableOpacity>
//                 <TouchableOpacity onPress={()=>navigation.navigate('OrderListScreen')} style={{ padding: 10 }} >
//                     <View style={{flexDirection:'row',borderBottomWidth:1,paddingBottom:10}} >
//                     <Icon type="AntDesign" name="wallet" style={{ fontSize: 20,paddingRight:8 }} />
//                     <Text style={{ fontWeight: "700", color: "#464B4E" }}>PAYMENTS</Text>
//                 </View>
//                 </TouchableOpacity>
//                 <TouchableOpacity onPress={()=>navigation.navigate('OrderListScreen')} style={{ padding: 10 }} >
//                     <View style={{flexDirection:'row',borderBottomWidth:1,paddingBottom:10}} >
//                     <Icon type="FontAwesome5" name="user-astronaut" style={{ fontSize: 20,paddingRight:8 }} />
//                     <Text style={{ fontWeight: "700", color: "#464B4E" }}>SUPPORT</Text>
//                 </View>
//                 </TouchableOpacity>
//                 <TouchableOpacity onPress={()=>navigation.navigate('OrderListScreen')} style={{ padding: 10 }} >
//                     <View style={{flexDirection:'row',borderBottomWidth:1,paddingBottom:10}} >
//                     <Icon type="Octicons" name="comment-discussion" style={{ fontSize: 20,paddingRight:8 }} />
//                     <Text style={{ fontWeight: "700", color: "#464B4E" }}>FAQS</Text>
//                 </View>
//                 </TouchableOpacity>
//             {/* //----------------------- */}
//             <View style={{ marginBottom: 10 }}>
//                 <TouchableOpacity style={{ padding: 16, flexDirection: "row", alignItems: "center" }} onPress={()=> logout()}>
//                     <Icon type="AntDesign" name="logout" style={{ marginRight: 8, color: "#464B4E" }} />
//                     <Text style={{ fontWeight: "400", color: "#464B4E" }}>Log out</Text>
//                 </TouchableOpacity>
//             </View>
//         </View>
//             </>