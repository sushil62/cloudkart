import React, { useContext } from 'react'
import { View,  TouchableOpacity } from 'react-native'
import { CKart } from '../../../ContextApi'
import { Icon } from 'native-base'
import { Styles } from '../../../Stylesheet'
import AddressListScreen from './AddressListScreen';

const ManageAddress = ({ navigation }) => {
    const { selectModeChange } = useContext(CKart)

    const { pColorOne } = Styles
    return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
            <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center" }}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Icon type="AntDesign" name="left" style={{ color: "#fff", fontSize: 20 }} />
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>
                <AddressListScreen />
            </View>
        </View>
    )
}

export default ManageAddress
