import React, { useContext, useState } from 'react'
import { View, Text, TouchableOpacity, ScrollView, Dimensions, TextInput, Picker } from 'react-native'
import { CKart } from '../../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Button, Item, Label, Input, CheckBox } from 'native-base'
import { Styles } from '../../../Stylesheet'
import { withNavigation } from 'react-navigation'


const NewAddressForm = ({ navigation }) => {
    const { addressListData, addAddressApi,address_types, addressSaved, handleInputBox, address_type, set_default_address,address_1,address_2,pincode,city,country,name,phone_no
 } = useContext(CKart)
    // let state_qs = []
    // if (addressListData.state_qs) {
    //     state_qs = addressListData.state_qs
    // }
    // console.log(addressListData, "addressListData")
    // console.log(state_qs, "fetchAllAddress")
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 2 - (width / 17)
    const { pColorOne } = Styles
    const addTypes = [{ id: 1, name: "Home" }, { id: 2, name: "Office" }, { id: 3, name: "Others" }]

    const isEnabled = address_type.length > 0 && address_1.length > 0 && address_2.length > 0 && pincode.length > 0 &&  country.length > 0 && name.length > 0 && phone_no.length > 5

    return (
        <ScrollView style={{ flex: 1, backgroundColor: "#fff" }}>
            <View style={{ marginVertical: 5, borderWidth: 0, height: 40, }}>
                <TouchableOpacity style={{ paddingVertical: 10 }} onPress={() => navigation.navigate('NewAddress')}>
                    <Text style={{ marginHorizontal: 20, fontSize: 17, fontWeight: "bold" }}>Add delivery address</Text>
                </TouchableOpacity>
            </View>

            <View style={{ marginVertical: 10, marginHorizontal: 20, flex: 1, borderWidth: 0 }}>

                <View style={{ paddingVertical: 5, }}>
                    <Label>Select Address Type</Label>
                    <Picker style={{
                        height: 40,
                        width: sWidth * 2,
                        borderColor: '#dfdfdf',
                        borderWidth: 1
                    }}
                        underlineColorAndroid="transparent"

                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        onValueChange={(itemValue, itemPosition) =>
                            handleInputBox(itemValue, "address_type")
                        }
                        // value={address_type}
                        selectedValue={address_type}

                    >
                        <Picker.Item label="------" value="" />
                        {address_types.map(st =>
                            <Picker.Item label={st.name} value={st.value} />
                        )}
                    </Picker>
                </View>

                <View style={{ paddingVertical: 10 }}>
                    <Label>Area / Locality</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1

                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. #30, 2nd Floor, 1 Cross"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        onChangeText={(text) => handleInputBox(text, "address_1")}
                    />
                </View>
                <View style={{ paddingVertical: 10 }}>
                    <Label>Landmark</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1
                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. Opposite Of Post Office"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        onChangeText={(text) => handleInputBox(text, "address_2")}
                    />
                </View>
                <View style={{ paddingVertical: 10 }}>
                    <Label>Postcode</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1
                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. 111111"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        onChangeText={(text) => handleInputBox(text, "pincode")}
                    />
                </View>
                {/* <View style={{ paddingVertical: 10 }}>
                    <Label>City</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1
                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. Bengaluru"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        onChangeText={(text) => handleInputBox(text, "city")}
                    />
                </View>
                <View style={{ paddingVertical: 5, }}>
                    <Label>State</Label>
                    <Picker style={{
                        height: 40,
                        width: sWidth * 2,
                        borderColor: '#dfdfdf',
                        borderWidth: 1
                    }}
                        underlineColorAndroid="transparent"

                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        onValueChange={(itemValue, itemPosition) =>
                            handleInputBox(itemValue, "state")
                        }
                    // selectedValue={state}

                    >
                        <Picker.Item label="------" value="" />
                        {state_qs.map(st =>
                            <Picker.Item label={st.name} value={st.iso_code} />
                        )}
                    </Picker>
                </View> */}
                {/*<View style={{ paddingVertical: 10 }}>
                                    <Label>State</Label>
                                    <TextInput style={{
                                        height: 40,
                                        borderColor: '#dfdfdf',
                                        borderBottomWidth: 1
                                    }}
                                        underlineColorAndroid="transparent"
                                        placeholder="E.g. Karnataka"
                                        placeholderTextColor="#dfdfdf"
                                        autoCapitalize="none"
                                        onChangeText={(text) => handleInputBox(text, "state")}
                                    />
                                </View>*/}
                <View style={{ paddingVertical: 10 }}>
                    <Label>Country</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1
                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. India"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        onChangeText={(text) => handleInputBox(text, "country")}
                    />
                </View>
                <View style={{ paddingVertical: 10 }}>
                    <Label>Contact Person Name</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1
                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. John Deo"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        onChangeText={(text) => handleInputBox(text, "name")}
                    />
                </View>
                <View style={{ paddingVertical: 10 }}>
                    <Label>Mobile Number</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1
                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. 9876543210"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        onChangeText={(text) => handleInputBox(text, "phone_no")}
                    />
                </View>
            </View>

            <View style={{ marginVertical: 5, borderWidth: 0, height: 40, }}>
                <Text style={{ paddingVertical: 20, marginHorizontal: 20, fontSize: 17, fontWeight: "bold" }}>Save address as</Text>
            </View>
            {/*<View style={{ flexDirection: "row", justifyContent: "space-between", marginVertical: 20, marginHorizontal: 20, }}>
                            {addTypes.map((ard ,indx) => {
                                return (
                                    <TouchableOpacity key={indx} onPress={() => handleInputBox(ard.id, "addressType")}>
                                        <View style={{ borderWidth: addressType === ard.id ? 1.5 : 0, borderColor: pColorOne, height: 40, width: 90, borderRadius: 20, justifyContent: "center", paddingHorizontal: 22.5 }}>
                                            <Text>{ard.name}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })}
                        </View>*/}

            <TouchableOpacity onPress={() => handleInputBox(!set_default_address, "set_default_address")} style={{ flexDirection: "row", paddingHorizontal: 5, paddingBottom: 20 }}>
                <CheckBox style={{ borderColor: "black", backgroundColor: set_default_address ? "black" : "#fff" }} value={set_default_address}
                    checked={set_default_address} />
                <Text style={{ paddingHorizontal: 20, fontSize: 16 }}>Set as default address</Text>
            </TouchableOpacity>
            <View style={{ paddingHorizontal: 10, paddingBottom: 10 }}>
                <Button disabled={!isEnabled} style={{ backgroundColor: isEnabled  ? pColorOne : "#bbb", justifyContent: "center", }} onPress={() => {
                    addAddressApi();navigation.navigate('ManageAddress');
                }}>
                    <Text style={{ color: "#fff", fontSize: 16 }}>Save</Text>
                </Button>
            </View>
        </ScrollView>
    )
}

export default withNavigation(NewAddressForm)
