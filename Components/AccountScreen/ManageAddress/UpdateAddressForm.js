import React, { useContext, useState } from 'react'
import { View, Text, TouchableOpacity, ScrollView, Dimensions, TextInput, Picker } from 'react-native'
import { CKart } from '../../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Button, Item, Label, Input, CheckBox } from 'native-base'
import { Styles } from '../../../Stylesheet'
import { withNavigation } from 'react-navigation'


const UpdateAddressForm = ({ navigation }) => {
    
    const { addressListData, addAddressApi, addressSaved,address_types,address_type_addr_edit, handleInputBox, addressType, set_default_address_addr_edit,address_1_addr_edit,address_2_addr_edit,pincode_addr_edit,city_addr_edit,country_addr_edit,name_addr_edit,phone_no_addr_edit,id_addr_edit,updateAddressApi } = useContext(CKart)
    let state_qs = []
    if (addressListData.state_qs) {
        state_qs = addressListData.state_qs
    }
    // console.log(state_qs, "fetchAllAddress")
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 2 - (width / 17)
    const { pColorOne } = Styles
    const addTypes = [{ id: 1, name: "Home" }, { id: 2, name: "Office" }, { id: 3, name: "Others" }]

    return (
        <ScrollView style={{ flex: 1, backgroundColor: "#fff" }}>
            <View style={{ marginVertical: 5, borderWidth: 0, height: 40, }}>
                <TouchableOpacity style={{ paddingVertical: 10 }} onPress={() => navigation.navigate('NewAddress')}>
                    <Text style={{ marginHorizontal: 20, fontSize: 17, fontWeight: "bold" }}>Update delivery address</Text>
                </TouchableOpacity>
            </View>

            <View style={{ marginVertical: 10, marginHorizontal: 20, flex: 1, borderWidth: 0 }}>

                 <View style={{ paddingVertical: 5, }}>
                    <Label>Select Address Type</Label>
                    <Picker style={{
                        height: 40,
                        width: sWidth * 2,
                        borderColor: '#dfdfdf',
                        borderWidth: 1
                    }}
                        underlineColorAndroid="transparent"

                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        onValueChange={(itemValue, itemPosition) =>
                            handleInputBox(itemValue, "address_type_addr_edit")
                        }
                        // value={address_type}
                        selectedValue={address_type_addr_edit}

                    >
                        <Picker.Item label="------" value="" />
                        {address_types.map(st =>
                            <Picker.Item label={st.name} value={st.value} />
                        )}
                    </Picker>
                </View>
                <View style={{ paddingVertical: 10 }}>
                    <Label>Area / Locality</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1

                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. #30, 2nd Floor, 1 Cross"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        value={address_1_addr_edit}
                        onChangeText={(text) => handleInputBox(text, "address_1_addr_edit")}
                    />
                </View>
                <View style={{ paddingVertical: 10 }}>
                    <Label>Landmark</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1
                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. Opposite Of Post Office"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        value={address_2_addr_edit}
                        onChangeText={(text) => handleInputBox(text, "address_2_addr_edit")}
                    />
                </View>
                <View style={{ paddingVertical: 10 }}>
                    <Label>Postcode</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1
                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. 111111"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        value={pincode_addr_edit}
                        onChangeText={(text) => handleInputBox(text, "pincode_addr_edit")}
                    />
                </View>
                {/* <View style={{ paddingVertical: 10 }}>
                    <Label>City</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1
                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. Bengaluru"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        value={city_addr_edit}
                        onChangeText={(text) => handleInputBox(text, "city_addr_edit")}
                    />
                </View>
                <View style={{ paddingVertical: 5, }}>
                    <Label>State</Label>
                    <Picker style={{
                        height: 40,
                        width: sWidth * 2,
                        borderColor: '#dfdfdf',
                        borderWidth: 1
                    }}
                        underlineColorAndroid="transparent"

                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        onValueChange={(itemValue, itemPosition) =>
                            handleInputBox(itemValue, "state")
                        }
                    // selectedValue={state}

                    >
                        <Picker.Item label="------" value="" />
                        {state_qs.map(st =>
                            <Picker.Item label={st.name} value={st.iso_code} />
                        )}
                    </Picker>
                </View> */}
                {/*<View style={{ paddingVertical: 10 }}>
                                    <Label>State</Label>
                                    <TextInput style={{
                                        height: 40,
                                        borderColor: '#dfdfdf',
                                        borderBottomWidth: 1
                                    }}
                                        underlineColorAndroid="transparent"
                                        placeholder="E.g. Karnataka"
                                        placeholderTextColor="#dfdfdf"
                                        autoCapitalize="none"
                                        onChangeText={(text) => handleInputBox(text, "state")}
                                    />
                                </View>*/}
                <View style={{ paddingVertical: 10 }}>
                    <Label>Country</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1
                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. India"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        value={country_addr_edit}
                        onChangeText={(text) => handleInputBox(text, "country_addr_edit")}
                    />
                </View>
                <View style={{ paddingVertical: 10 }}>
                    <Label>Contact Person Name</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1
                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. John Deo"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        value={name_addr_edit}
                        onChangeText={(text) => handleInputBox(text, "name_addr_edit")}
                    />
                </View>
                <View style={{ paddingVertical: 10 }}>
                    <Label>Mobile Number</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1
                    }}
                        underlineColorAndroid="transparent"
                        placeholder="E.g. 9876543210"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        value={phone_no_addr_edit}
                        onChangeText={(text) => handleInputBox(text, "phone_no_addr_edit")}
                    />
                </View>
            </View>

            <View style={{ marginVertical: 5, borderWidth: 0, height: 40, }}>
                <Text style={{ paddingVertical: 20, marginHorizontal: 20, fontSize: 17, fontWeight: "bold" }}>Save address as</Text>
            </View>
          {/*  <View style={{ flexDirection: "row", justifyContent: "space-between", marginVertical: 20, marginHorizontal: 20, }}>
                          {addTypes.map(ard => {
                              return (
                                  <TouchableOpacity onPress={() => handleInputBox(ard.id, "addressType")}>
                                      <View style={{ borderWidth: addressType === ard.id ? 1.5 : 0, borderColor: pColorOne, height: 40, width: 90, borderRadius: 20, justifyContent: "center", paddingHorizontal: 22.5 }}>
                                          <Text>{ard.name}</Text>
                                      </View>
                                  </TouchableOpacity>
                              )
                          })}
                      </View>*/}

            <TouchableOpacity onPress={() => handleInputBox(!set_default_address_addr_edit, "set_default_address_addr_edit")} style={{ flexDirection: "row", paddingHorizontal: 5, paddingBottom: 20 }}>
                <CheckBox style={{ borderColor: "black", backgroundColor: set_default_address_addr_edit ? "black" : "#fff" }} value={set_default_address_addr_edit}
                    checked={set_default_address_addr_edit} />
                <Text style={{ paddingHorizontal: 20, fontSize: 16 }}>Set as default address</Text>
            </TouchableOpacity>
            <View style={{ paddingHorizontal: 10, paddingBottom: 10 }}>
                <Button style={{ backgroundColor: true ? pColorOne : "#FC9663", justifyContent: "center", }} onPress={() => {
                    updateAddressApi(id_addr_edit);navigation.navigate('ManageAddress');
                }}>
                    <Text style={{ color: "#fff", fontSize: 16 }}>Save</Text>
                </Button>
            </View>
        </ScrollView>
    )
}

export default withNavigation(UpdateAddressForm)
