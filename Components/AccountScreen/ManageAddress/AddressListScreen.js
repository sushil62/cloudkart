import React, { useContext } from 'react'
import { View, Text, TouchableOpacity, ScrollView, Dimensions } from 'react-native'
import { CKart } from '../../../ContextApi'
import { Icon, Button } from 'native-base'
import { Styles } from '../../../Stylesheet'
import { withNavigation } from 'react-navigation'


const AddressListScreen = ({ navigation }) => {
    const { addressListData,  deleteAddressApi,editgetAddressApi ,defaultpostAddressApi} = useContext(CKart)
    // console.log(addressListData, "fetchAllAddress")
    let addressList = []
    if (addressListData.address) {
        addressList = addressListData.address
    }
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 2 - (width / 17)
    const { pColorOne } = Styles
    return (
        <ScrollView style={{ flex: 1, backgroundColor: "#fff" }}>
            <View style={{ backgroundColor: "#dfdfdf", marginVertical: 5, borderWidth: 0, height: 40, }}>
                <TouchableOpacity style={{ paddingVertical: 10 }} onPress={() => { navigation.navigate('NewAddress'); }}>
                    <Text style={{ marginHorizontal: 20, }}> + ADD NEW ADDRESS</Text>
                </TouchableOpacity>
            </View>
            {addressListData !== undefined &&  addressListData.map((adrs,indx) => {
                return (
                    <View key={indx} style={{ marginVertical: 10, marginHorizontal: 20, flex: 1, borderWidth: 0 }}>
                        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                            <Text style={{ fontSize: 18 }}>{adrs.name}</Text>
                            {/*<View style={{ backgroundColor: "#dfdfdf", borderWidth: 0, borderRadius: 5, width: 45, alignItems: "center", paddingVertical: 2 }}>
                                                            <Text>Home</Text>
                                                        </View>*/}
                        </View>
                        <View style={{ paddingVertical: 10 }}>
                            <Text>{adrs.address_1}</Text>
                            <Text>{adrs.address_2}</Text>
                            <Text>{adrs.pincode}</Text>
                            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                <Text>Mobile: {adrs.phone_no}</Text>
                                <TouchableOpacity onPress={() =>  defaultpostAddressApi(adrs.id)} >
                                    <Text style={{ fontWeight: "bold", color: adrs.set_default_address ? pColorOne : null }}>{adrs.set_default_address ? "Default" : "Mark as default"}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", borderTopWidth: 0, marginVertical: 5 }}> 
                            <Button style={{ backgroundColor: "#dfdfdf", width: sWidth, height: 40, justifyContent: "center" }} onPress={() => { editgetAddressApi(adrs.id);navigation.navigate('UpdateAddress'); }}>
                                <Text style={{ fontSize: 13, }}>EDIT</Text>
                            </Button>
                            <Button style={{ backgroundColor: "#dfdfdf", width: sWidth, height: 40, justifyContent: "center" }} onPress={() => deleteAddressApi(adrs.id)}>
                                <Text style={{ fontSize: 13, }}>REMOVE</Text>
                            </Button>
                        </View>
                    </View>
                )
            })}
        </ScrollView>
    )
}

export default withNavigation(AddressListScreen)
