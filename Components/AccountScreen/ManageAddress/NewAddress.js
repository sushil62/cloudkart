import React, { useContext } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { CKart } from '../../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Button } from 'native-base'
import { Styles } from '../../../Stylesheet'
import NewAddressForm from './NewAddressForm'

const NewAddress = ({ navigation }) => {
    const { selectModeChange } = useContext(CKart)

    const { pColorOne } = Styles
    return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
            <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center" }}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Icon type="AntDesign" name="left" style={{ color: "#fff", fontSize: 20 }} />
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>
                <NewAddressForm />
                
            </View>
        </View>
    )
}

export default NewAddress
