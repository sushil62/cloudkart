import React, { useContext } from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import { CKart } from '../../ContextApi'
import Br from '../../Common/Br'
import { Icon } from 'native-base'
import { Ionicons } from '@expo/vector-icons';
import { withNavigation } from 'react-navigation'

const MenuList = ({ navigation }) => {
    const { scrollfunc, fetchAllAddressApi } = useContext(CKart)
    const scrollmenu = [
        { id: 1, title: 'Profile Info', type: 'Feather', name: 'user', keyname: 'Profile Info', pressFunc: () => { scrollfunc(1); navigation.navigate('ProfileInfo');} },
        { id: 2, title: 'Order Details', type: 'Feather', name: 'package', keyname: 'Order Details', pressFunc: () => { scrollfunc(2); } },
        { id: 3, title: 'Tracking', type: 'SimpleLineIcons', name: 'flag', keyname: 'Tracking', pressFunc: () => scrollfunc(3) },
        { id: 4, title: 'Manage Addresses', type: 'Entypo', name: 'address', keyname: 'Manage Addresses', pressFunc: () => { scrollfunc(4); navigation.navigate('ManageAddress'); fetchAllAddressApi(); } },
        { id: 5, title: 'My Coupons', type: 'Entypo', name: 'ticket', keyname: 'My Coupons', pressFunc: () => scrollfunc(5) },
        { id: 6, title: 'My Points', type: 'FontAwesome', name: 'id-card-o', keyname: 'My Points', pressFunc: () => scrollfunc(6) },
        { id: 7, title: 'Privacy Policy', type: 'SimpleLineIcons', name: 'shield', keyname: 'Privacy Policy', pressFunc: () => scrollfunc(7) },
        { id: 8, title: 'Frequently Asked Questions', type: 'Octicons', name: 'comment-discussion', keyname: 'Frequently Asked Questions', pressFunc: () => scrollfunc(8) },
        { id: 9, title: 'Legal Indormation', type: 'FontAwesome', name: 'file-text-o', keyname: 'Legal Indormation', pressFunc: () => scrollfunc(9) }

    ]
    const { select_mode } = useContext(CKart)
    return (
        <ScrollView style={{ flex: 1, backgroundColor: "#fff" }}>
            <View style={{ marginVertical: 10, marginHorizontal: 20, flex: 1 }}>
                {scrollmenu.map(menu => {
                    return (
                        <TouchableOpacity key={menu.id} style={{ padding: 10, flexDirection: 'row', borderBottomWidth: 0.5, borderBottomColor: "#dcdcdc" }} onPress={menu.pressFunc}>
                            <Icon type={menu.type} name={menu.name} size={13} />
                            <Text style={{ color: "#464B4E", alignItems: 'center', marginLeft: 20 }}>  {menu.title} </Text>


                        </TouchableOpacity>

                    )
                })}
            </View>
        </ScrollView>
    )
}

export default withNavigation(MenuList)