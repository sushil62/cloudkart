import React, { useContext } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Br from '../../Common/Br'
import { CKart } from '../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon } from 'native-base'
import { Styles } from '../../Stylesheet'
import MenuList from './MenuList'
import { withNavigation } from 'react-navigation'


const SelectButton = ({ navigation }) => {
    const { selectModeChange } = useContext(CKart)

    const { pColorOne } = Styles
    return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
            <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center" }}>

                <TouchableOpacity onPress={() => navigation.openDrawer()}>
                    <Icon type="MaterialIcons" name="menu" style={{ color: "#fff" }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('HomeScreen')}>
                    <Text style={{ color: "#fff", paddingLeft: 12, fontSize: 16, fontWeight: "700" }}>CloudKart360</Text>
                </TouchableOpacity>
            </View>


            <View style={{ flex: 1 }}>
                <MenuList />
            </View>


        </View>
    )
}

export default withNavigation(SelectButton)
