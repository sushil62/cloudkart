import React, { useContext, useState } from 'react'
import { withNavigation } from 'react-navigation'
import { View, Text, TouchableOpacity, TextInput, Dimensions, StyleSheet, Modal, } from 'react-native'
import { Styles } from '../../Stylesheet'
import { Icon, } from 'native-base'
import { Label, } from 'native-base'
import { CKart } from '../../ContextApi'


const ProfileInfo = ({ navigation }) => {

    const { login_username, handleInputBox, first_name, last_name, mobile_number, updateName, updateUserMobileNumber, updateUserEmail, changePassword, new_password, re_new_password, new_email, re_new_email, captcha, captch_text,userdetail_data } = useContext(CKart)
    const { pColorOne } = Styles
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 2
    const [EmailmodalVisible, setEmailModalVisible] = useState(false);
    const [MobilemodalVisible, setMobileModalVisible] = useState(false);
    const [PasswordmodalVisible, setPasswordModalVisible] = useState(false);
    const [UpdateNameMode, setUpdateNameMode] = useState(false);

    // console.log(userdetail_data,"userdetail_data=============")

    const userinfodata = ()=>{
	    if (userdetail_data !== undefined) {

	    	return(
	    			<View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
	            <View style={{ flexDirection: 'row', justifyContent: "space-between", marginHorizontal: 20, alignItems: "center", marginBottom: 7 }}>
	                <TouchableOpacity onPress={() => navigation.goBack()}>
	                    <Icon type="AntDesign" name="left" style={{ color: "#fff", fontSize: 20 }} />
	                </TouchableOpacity>
	            </View>
	            <View style={{ flex: 1, marginTop: 5, backgroundColor: "#fff", padding: 20, paddingVertical: 20 }}>

	                	{UpdateNameMode === false && 
	                		<View>
	                <View style={{ flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>
	                    <View style={{ flex: 1, paddingBottom: 20 }}>
	                        <Text>First Name</Text>
	                        <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="FontAwesome" name="user" style={{ color: "#000", fontSize: 20,
    padding: 5,
        margin: 2,
         }} />
	                        
	                        <Text style={{
	                            height: 40,
	                            // borderColor: '#dfdfdf',
	                            // borderWidth: 1
	                            borderLeftWidth: .5,
	                            paddingHorizontal:5,
	                            paddingVertical:10
	                        }}>{userdetail_data.first_name}</Text>
	                    </View>
	                    </View>
	                    <View style={{ flex: 1, paddingBottom: 20, paddingLeft: 10 }}>
	                        <Text>Last Name</Text>
	                        <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="FontAwesome" name="user" style={{ color: "#000", fontSize: 20,
    padding: 5,
        margin: 2,
         }} />
	                        <Text style={{
	                            height: 40,
	                            // borderColor: '#dfdfdf',
	                            // borderWidth: 1
	                            borderLeftWidth: .5,
	                            paddingHorizontal:5,
	                            paddingVertical:10
	                        }}>{userdetail_data.last_name}</Text>
	                    </View>
	                    </View>
	                </View>
	                <View style={{ alignItems: "center", marginVertical: 5 }}>
	                		<TouchableOpacity onPress={() => {
	                	                        setUpdateNameMode(true);
	                	                    }}>
	                        <Text style={{  color: pColorOne, fontSize: 15 }}>Update</Text>
	                    </TouchableOpacity>
	                </View>
	                		</View>
	                	                }
	                	 {UpdateNameMode === true &&
	                	 	<View>
	                	 	<View style={{ flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>
	                    <View style={{ flex: 1, paddingBottom: 20 }}>
	                        <Label>First Name</Label>
	                                                <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="FontAwesome" name="user" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
	                        <TextInput style={{
	                            height: 40,
	                            // borderColor: '#dfdfdf',
	                            // borderWidth: 1
	                            borderLeftWidth: .5,
	                            paddingHorizontal:5
	                        }}
	                            underlineColorAndroid="transparent"
	                            placeholder="E.g. John"
	                            placeholderTextColor="#dfdfdf"
	                            autoCapitalize="none"
	                            value={first_name}
	                            onChangeText={(text) => handleInputBox(text, "first_name")}
	                        />
	                    </View>
	                    </View>
	                    <View style={{ flex: 1, paddingBottom: 20,
	                     paddingLeft: 10
	                      }}>
	                        <Label>Last Name</Label>
	                        <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="FontAwesome" name="user" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
   
	                        <TextInput style={{
	                            height: 40,
	                            // borderColor: '#dfdfdf',
	                            // borderWidth: 1
	                            borderLeftWidth: .5,
	                            paddingHorizontal:5
	                        }}
	                            underlineColorAndroid="transparent"
	                            placeholder="E.g. Deo"
	                            placeholderTextColor="#dfdfdf"
	                            autoCapitalize="none"
	                            value={last_name}
	                            onChangeText={(text) => handleInputBox(text, "last_name")}
	                        />
	                        </View>
	                    </View>
	                </View>
	                <View style={{  alignSelf: "center", marginVertical: 5,flexDirection:'row', }}>
		                    <TouchableOpacity style={{marginHorizontal: 10,}} onPress={() => {setUpdateNameMode(!UpdateNameMode); updateName(userdetail_data.user_id)}}>
		                        <Text style={{ color: pColorOne, fontWeight: "bold" }}>SUBMIT</Text>
		                    </TouchableOpacity>
		                    <TouchableOpacity style={{marginHorizontal: 10,}} onPress={() => {setUpdateNameMode(!UpdateNameMode); }}>
		                        <Text style={{ color: pColorOne, fontWeight: "bold" }}>Cancel</Text>
		                    </TouchableOpacity>
	                </View>
	            	 	</View>

	                	 }
	                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
	                    <View style={{ paddingVertical: 20 }}>
	                        <Label>Mobile Number</Label>
	                                                <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="FontAwesome" name="mobile-phone" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
	                        <Text style={{
	                            height: 40,
	                            // borderColor: '#dfdfdf',
	                            borderLeftWidth: .5,
	                            width: sWidth + sWidth / 2.5,
	                            paddingVertical: 10,paddingHorizontal: 10,
	                            fontSize: 15
	                        }}
	                        >{userdetail_data.phone_no}</Text>
	                    </View>
	                    </View>
	                    <TouchableOpacity onPress={() => {
	                        setMobileModalVisible(true);
	                    }}>
	                        <Text style={{ paddingVertical: 50,paddingHorizontal:5, color: pColorOne, fontSize: 15 }}>Update</Text>
	                    </TouchableOpacity>
	                </View>
	                <Modal
	                    animationType="slide"
	                    transparent={true}
	                    visible={MobilemodalVisible}

	                >
	                    <View style={styles.centeredView}>
	                        <View style={styles.modalView}>
	                            <View style={{ paddingVertical: 20 }}>
	                                <Label>Old Mobile Number</Label>
	                                  <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="FontAwesome" name="mobile-phone" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
	                        <Text style={{
	                            height: 40,
	                            // borderColor: '#dfdfdf',
	                            borderLeftWidth: .5,
	                            width: sWidth + sWidth / 2.5,
	                            paddingVertical: 10,paddingHorizontal: 10,
	                            fontSize: 15
	                        }}
	                        >{userdetail_data.phone_no}</Text>
	                            </View>
	                            </View>
	                            <View style={{ paddingVertical: 10 }}>
	                                <Label>New mobile number</Label>
	                                  <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="FontAwesome" name="mobile-phone" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
	                       
	                                <TextInput style={{
	                                    height: 40,
	                            // borderColor: '#dfdfdf',
	                            borderLeftWidth: .5,
	                            width: sWidth + sWidth / 2.5,
	                            paddingVertical: 10,paddingHorizontal: 10,
	                            fontSize: 15
	                                }}
	                                    underlineColorAndroid="transparent"
	                                    placeholder="E.g. 9876543210"
	                                    placeholderTextColor="#dfdfdf"
	                                    autoCapitalize="none"
	                                    onChangeText={(text) => handleInputBox(text, "new_mobile_number")}
	                                />
	                            </View>
	                            </View>
	                            <View style={{ flexDirection: "row" }}>
	                                <TouchableOpacity
	                                    style={{ ...styles.openButton, backgroundColor: pColorOne }}
	                                    onPress={() => {
	                                        updateUserMobileNumber(userdetail_data.user_profile_id);
	                                        setMobileModalVisible(!MobilemodalVisible);
	                                    }}
	                                >
	                                    <Text style={styles.textStyle}>Save Changes</Text>
	                                </TouchableOpacity>
	                                <TouchableOpacity onPress={() => {
	                                    setMobileModalVisible(!MobilemodalVisible);
	                                }}>
	                                    <Text style={{ paddingVertical: 10, paddingHorizontal: 10 }}>Cancel</Text>
	                                </TouchableOpacity>
	                            </View>
	                        </View>
	                    </View>
	                </Modal>
	                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
	                    <View style={{ paddingVertical: 20 }}>
	                        <Label>Email Address</Label>
	                                                                 <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="Fontisto" name="email" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
	                        <Text style={{
	                            height: 40,
	                            // borderColor: '#dfdfdf',
	                            borderLeftWidth: .5,
	                            width: sWidth + sWidth / 2.5,
	                            paddingVertical: 10,
	                            paddingHorizontal: 10,

	                            fontSize: 15
	                        }}

	                        >{userdetail_data.email}</Text>
	                    </View>
	                    </View>
	                    <TouchableOpacity onPress={() => {
	                        setEmailModalVisible(true);
	                        captcha();
	                    }}>
	                        <Text style={{ paddingVertical: 50,paddingHorizontal:5, color: pColorOne, fontSize: 15 }}>Update</Text>
	                    </TouchableOpacity>
	                </View>

	                <Modal
	                    animationType="slide"
	                    transparent={true}
	                    visible={EmailmodalVisible}
	                >
	                    <View style={styles.centeredView}>
	                        <View style={styles.modalView}>
	                            <View style={{ paddingVertical: 20 }}>
	                                <Label>Email Address</Label>
	                                <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="Fontisto" name="email" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
	                                <Text style={{
	                                    height: 40,
	                            // borderColor: '#dfdfdf',
	                            borderLeftWidth: .5,
	                            width: sWidth + sWidth / 2.5,
	                            paddingVertical: 10,
	                            paddingHorizontal: 10,

	                            fontSize: 15
	                                }}

	                                >{login_username}</Text>
	                            </View>
	                            </View>
	                            <View style={{ paddingVertical: 10 }}>
	                                <Label>New e-mail</Label>
	                                                                <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="Fontisto" name="email" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
	                                <TextInput style={{
	                                    height: 40,
	                            // borderColor: '#dfdfdf',
	                            borderLeftWidth: .5,
	                            width: sWidth + sWidth / 2.5,
	                            paddingVertical: 10,
	                            paddingHorizontal: 10,

	                            fontSize: 15
	                                }}
	                                    underlineColorAndroid="transparent"
	                                    placeholder="E.g. xyz@email.com"
	                                    placeholderTextColor="#dfdfdf"
	                                    autoCapitalize="none"
	                                    onChangeText={(text) => handleInputBox(text, "new_email")}
	                                />
	                            </View>
	                            </View>
	                            <View style={{ paddingVertical: 10 }}>
	                                <Label>Re-enter new e-mail</Label>
	                                                                <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="Fontisto" name="email" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
	                                <TextInput style={{
	                                   height: 40,
	                            // borderColor: '#dfdfdf',
	                            borderLeftWidth: .5,
	                            width: sWidth + sWidth / 2.5,
	                            paddingVertical: 10,
	                            paddingHorizontal: 10,

	                            fontSize: 15
	                                }}
	                                    underlineColorAndroid="transparent"
	                                    placeholder="E.g. xyz@email.com"
	                                    placeholderTextColor="#dfdfdf"
	                                    autoCapitalize="none"
	                                    onChangeText={(text) => handleInputBox(text, "re_new_email")}
	                                />
	                                {new_email && re_new_email && new_email !== re_new_email ?
	                                    < Text style={{ color: "red" }}>Email did not match</Text> :
	                                    null
	                                }
	                            </View>
	                            </View>
	                            <View style={{ paddingVertical: 10 }}>
	                                <Label>Password</Label>
	                                                                <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="FontAwesome" name="lock" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
	                                <TextInput style={{
	                                   height: 40,
	                            // borderColor: '#dfdfdf',
	                            borderLeftWidth: .5,
	                            width: sWidth + sWidth / 2.5,
	                            paddingVertical: 10,
	                            paddingHorizontal: 10,

	                            fontSize: 15
	                                }}
	                                    secureTextEntry
	                                    underlineColorAndroid="transparent"
	                                    placeholderTextColor="#dfdfdf"
	                                    autoCapitalize="none"
	                                    onChangeText={(text) => handleInputBox(text, "password")}

	                                />
	                            </View>
	                            </View>
	                            <View style={{ width: sWidth + sWidth / 2, backgroundColor: "#dcdcdc", alignItems: "center" }}>
	                                <Text style={{ paddingVertical: 15 }}>{captch_text}</Text>
	                            </View>
	                            <View style={{ paddingVertical: 10, paddingHorizontal: 10 }}>
	                                <Label>Enter Captcha</Label>
	                                                                <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    

	                                <TextInput style={{
	                                    height: 40,
	                            // borderColor: '#dfdfdf',
	                            // borderLeftWidth: .5,
	                            width: sWidth + sWidth / 2.5,
	                            paddingVertical: 10,
	                            paddingHorizontal: 10,

	                            fontSize: 15

	                                }}
	                                    
	                                    underlineColorAndroid="transparent"
	                                    placeholderTextColor="#dfdfdf"
	                                    autoCapitalize="none"
	                                    onChangeText={(text) => handleInputBox(text, "captcha_input")}

	                                />
	                            </View>
	                            </View>
	                            <View style={{ flexDirection: "row" }}>
	                                <TouchableOpacity
	                                    style={{ ...styles.openButton, backgroundColor: pColorOne }}
	                                    onPress={() => {
	                                        updateUserEmail(userdetail_data.user_id);
	                                        setEmailModalVisible(!EmailmodalVisible);
	                                    }}
	                                >
	                                    <Text style={styles.textStyle}>Save Changes</Text>
	                                </TouchableOpacity>
	                                <TouchableOpacity onPress={() => {
	                                    setEmailModalVisible(!EmailmodalVisible);
	                                }}>
	                                    <Text style={{ paddingVertical: 10, paddingHorizontal: 10 }}>Cancel</Text>
	                                </TouchableOpacity>
	                            </View>
	                        </View>
	                    </View>
	                </Modal>

	                <View style={{ paddingVertical: 10 }}>
	                    <TouchableOpacity onPress={() => {
	                        
	                        setPasswordModalVisible(true);
	                    }}>
	                        <Text style={{ fontSize: 16, paddingVertical: 10,color: pColorOne,  }}>Change Password</Text>
	                    </TouchableOpacity>
	                </View>
	                <Modal
	                    animationType="slide"
	                    transparent={true}
	                    visible={PasswordmodalVisible}

	                >
	                    <View style={styles.centeredView}>
	                        <View style={styles.modalView}>

	                            <View style={{ paddingVertical: 10 }}>
	                                <Label>Current Password</Label>
	                                	                                                                <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="FontAwesome" name="lock" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
	                                <TextInput style={{
	                                    height: 40,
	                            // borderColor: '#dfdfdf',
	                            borderLeftWidth: .5,
	                            width: sWidth + sWidth / 2.5,
	                            paddingVertical: 10,
	                            paddingHorizontal: 10,

	                            fontSize: 15
	                                }}
	                                    underlineColorAndroid="transparent"

	                                    placeholderTextColor="#dfdfdf"
	                                    autoCapitalize="none"
	                                    onChangeText={(text) => handleInputBox(text, "curr_password")}
	                                />
	                            </View>
	                            </View>
	                            <View style={{ paddingVertical: 10 }}>
	                                <Label>New password</Label>
	                                	                                                                <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="FontAwesome" name="lock" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
	                                <TextInput style={{
	                                    height: 40,
	                            // borderColor: '#dfdfdf',
	                            borderLeftWidth: .5,
	                            width: sWidth + sWidth / 2.5,
	                            paddingVertical: 10,
	                            paddingHorizontal: 10,

	                            fontSize: 15

	                                }}
	                                    secureTextEntry
	                                    underlineColorAndroid="transparent"

	                                    placeholderTextColor="#dfdfdf"
	                                    autoCapitalize="none"
	                                    onChangeText={(text) => handleInputBox(text, "new_password")}
	                                />
	                            </View>
	                            </View>
	                            <View style={{ paddingVertical: 10 }}>
	                                <Label>Re-enter new password</Label>
	                                	                                                                <View style={{flexDirection: 'row',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: '#000',
    height: 40,
    borderRadius: 5 ,
}}>
    <Icon type="FontAwesome" name="lock" style={{ color: "#000", fontSize: 25,
    padding: 5,
        margin: 2,
         }} />
	                                <TextInput style={{
	                                   height: 40,
	                            // borderColor: '#dfdfdf',
	                            borderLeftWidth: .5,
	                            width: sWidth + sWidth / 2.5,
	                            paddingVertical: 10,
	                            paddingHorizontal: 10,

	                            fontSize: 15

	                                }}
	                                    secureTextEntry
	                                    underlineColorAndroid="transparent"

	                                    placeholderTextColor="#dfdfdf"
	                                    autoCapitalize="none"
	                                    onChangeText={(text) => handleInputBox(text, "re_new_password")}
	                                />
	                                {new_password && re_new_password && new_password !== re_new_password ?
	                                    < Text style={{ color: "red" }}>Password did not match</Text> :
	                                    null
	                                }
	                            </View>
	                            </View>
	                            <View style={{ flexDirection: "row" }}>
	                                <TouchableOpacity
	                                    style={{ ...styles.openButton, backgroundColor: pColorOne }}
	                                    onPress={() => {
	                                    	changePassword();
	                                        setPasswordModalVisible(!PasswordmodalVisible);
	                                    }}
	                                >
	                                    <Text style={styles.textStyle}>Save Changes</Text>
	                                </TouchableOpacity>
	                                <TouchableOpacity onPress={() => {
	                                    setPasswordModalVisible(!PasswordmodalVisible);
	                                }}>
	                                    <Text style={{ paddingVertical: 10, paddingHorizontal: 10 }}>Cancel</Text>
	                                </TouchableOpacity>
	                            </View>
	                        </View>
	                    </View>
	                </Modal>
	            </View>
	        </View >

	    		)
	    }

    }

    return (

    	<>
    	{userinfodata()}
    	</>
        
    )
}



const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        width: "100%"
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 10,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
});
export default withNavigation(ProfileInfo)
