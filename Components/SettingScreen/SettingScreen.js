import React from 'react'
import { View, Text,Button } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';

const SettingScreen = ({navigation}) => {


    const logout = async () => {
        navigation.navigate('Auth')
        await AsyncStorage.clear();
    }

    return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <Text>SettingScreen</Text>
            <Button title="Log out" onPress={()=>logout()}/>
        </View>
    )
}

export default SettingScreen
