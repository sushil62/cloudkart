import React, { useContext } from 'react'
import { View, Text, ScrollView, Dimensions, TouchableOpacity, Image } from 'react-native'
import { CKart } from '../../ContextApi'
import Carousel from 'react-native-banner-carousel'
import { withNavigation } from 'react-navigation'

const HomeBanner = ({ navigation }) => {
  const { offerData, base_url, fetchProductDtlApi, fetchVendorProductApi, fetchBrandProductApi } = useContext(CKart)
  const screenWidth = Math.round(Dimensions.get('window').width);
  const h = Dimensions.get('window').height;

  return (
    <View>
      <Carousel>
        {offerData.main_banner &&
          offerData.main_banner.page_chunks.map((p, index) => {
            return (
              <View key={index}>
                {p.banner_type === 'link_to_product' && (
                  <TouchableOpacity
                    onPress={() => { fetchProductDtlApi(p.vendor_product_id); navigation.navigate('ProductDetailScreen') }}
                  >
                    <Image style={{ height: screenWidth / 2.55, width: screenWidth, resizeMode: 'stretch' }} source={{ uri: base_url + p.url }} />
                  </TouchableOpacity>
                )}
                {p.banner_type === 'link_to_vendor_catalog' && (
                  <TouchableOpacity
                    onPress={() => { fetchVendorProductApi(p.vendor_id); navigation.navigate('ProductListScreen') }}
                  >
                    <Image style={{ height: screenWidth / 2.5, width: screenWidth, resizeMode: 'stretch' }} source={{ uri: base_url + p.url }} />
                  </TouchableOpacity>
                )}

                {p.banner_type === 'link_to_brand_catalog' && (
                  <TouchableOpacity
                    onPress={() => { fetchBrandProductApi(p.brand_id); navigation.navigate('ProductListScreen') }}
                  >
                    <Image style={{ height: screenWidth / 2.5, width: screenWidth, resizeMode: 'stretch' }} source={{ uri: base_url + p.url }} />
                  </TouchableOpacity>
                )}

                {p.banner_type === 'link_to_category_catalog' && (
                  <TouchableOpacity
                    onPress={() => { fetchCatProductApi(p.category_id); navigation.navigate('ProductListScreen') }}
                  >
                    <Image style={{ height: screenWidth / 2.5, width: screenWidth, resizeMode: 'stretch' }} source={{ uri: base_url + p.url }} />
                  </TouchableOpacity>
                )}
              </View>
            )
          })}
      </Carousel>
      <Carousel>
        {offerData.side_banner1 &&
          offerData.side_banner1.page_chunks.map((p, index) => {
            return (
              <View key={index}>
                {p.banner_type === 'link_to_product' && (
                  <TouchableOpacity
                    onPress={() => { fetchProductDtlApi(p.vendor_product_id); navigation.navigate('ProductDetailScreen') }}
                  >
                    <Image style={{ height: screenWidth / 2.5, width: screenWidth, }} source={{ uri: base_url + p.url }} />
                  </TouchableOpacity>
                )}
                {p.banner_type === 'link_to_vendor_catalog' && (
                  <TouchableOpacity
                    onPress={() => { fetchVendorProductApi(p.vendor_id); navigation.navigate('ProductListScreen') }}
                  >
                  </TouchableOpacity>
                )}

                {p.banner_type === 'link_to_brand_catalog' && (
                  <TouchableOpacity
                    onPress={() => { fetchBrandProductApi(p.brand_id); navigation.navigate('ProductListScreen') }}
                  >
                    <Image style={{ height: screenWidth / 2.5, width: screenWidth, }} source={{ uri: base_url + p.url }} />
                  </TouchableOpacity>
                )}

                {p.banner_type === 'link_to_category_catalog' && (
                  <TouchableOpacity
                    onPress={() => { fetchCatProductApi(p.category_id); navigation.navigate('ProductListScreen') }}
                  >
                    <Image style={{ height: screenWidth / 2.5, width: screenWidth, }} source={{ uri: base_url + p.url }} />
                  </TouchableOpacity>
                )}
              </View>
            )
          }

          )}
      </Carousel>
      <Carousel>
        {offerData.side_banner2 &&
          offerData.side_banner2.page_chunks.map((p, index) => {
            return (
              <View key={index}>
                {p.banner_type === 'link_to_product' && (
                  <TouchableOpacity
                    onPress={() => { fetchProductDtlApi(p.vendor_product_id); navigation.navigate('ProductDetailScreen') }}

                  >

                    <Image style={{ height: screenWidth / 2.5, width: screenWidth, }} source={{ uri: base_url + p.url }} />
                  </TouchableOpacity>
                )}
                {p.banner_type === 'link_to_vendor_catalog' && (
                  <TouchableOpacity
                    onPress={() => { fetchVendorProductApi(p.vendor_id); navigation.navigate('ProductListScreen') }}

                  >

                    <Image style={{ height: screenWidth / 2.5, width: screenWidth, }} source={{ uri: base_url + p.url }} />
                  </TouchableOpacity>
                )}

                {p.banner_type === 'link_to_brand_catalog' && (
                  <TouchableOpacity
                    onPress={() => { fetchBrandProductApi(p.brand_id); navigation.navigate('ProductListScreen') }}

                  >

                    <Image style={{ height: screenWidth / 2.5, width: screenWidth, }} source={{ uri: base_url + p.url }} />
                  </TouchableOpacity>
                )}

                {p.banner_type === 'link_to_category_catalog' && (
                  <TouchableOpacity
                    onPress={() => { fetchCatProductApi(p.category_id); navigation.navigate('ProductListScreen') }}

                  >

                    <Image style={{ height: screenWidth / 2.5, width: screenWidth, }} source={{ uri: base_url + p.url }} />
                  </TouchableOpacity>
                )}
              </View>
            )
          }

          )}
      </Carousel>
    </View>




  )

}
export default withNavigation(HomeBanner)

// autoplay={ false} 
// autoplay
//                     autoplayTimeout={3000}
//                     loop
//                     index={0}
//                     pageSize={BannerWidth}

              // </View>

      // <ScrollView
      //   horizontal
      //   pagingEnabled
      //   showsHorizontalScrollIndicator={false}
      //   style={{height: 180, width: screenWidth}}>
      //         <View>
            // </ScrollView>

// <TouchableOpacity style={{ marginRight: 12, height: 190 }}>

//                             <Image

//                               source=
//                                 {{uri: base_url + p.url}}
//                               style={{
//                             height: screenWidth / 2,
//                             width: screenWidth-30,
//                           }}
//                             />
//                         </TouchableOpacity>