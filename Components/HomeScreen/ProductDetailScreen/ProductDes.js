import React, { useContext } from 'react'
import { View, Text, TouchableOpacity, Dimensions, ToastAndroid, ScrollView } from 'react-native'

import { CKart } from '../../../ContextApi'
import SimilarProduct from './SimilarProduct'
import RelatedProduct from './RelatedProduct'
import StarRating from './StarRating'
import { Icon, Input, Button } from 'native-base'
import { Styles } from '../../../Stylesheet'
import { withNavigation } from 'react-navigation'

const ProductDes = ({ navigation }) => {
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 2

    const { product_dtl_data, base_url, quantity_change, quantity, addToCart, buyNow, setCheckoutData, fetchProductDtlApi, addProductwishlistApi, wishlist_list, handleRemovewishlist, signInToken, bottomModalFunc } = useContext(CKart)
    // console.log(product_dtl_data,"product_dtl_data")    
    const { pColorOne } = Styles
    // {product_dtl_data.vendor_product.product.title}
    const productDetail = () => {
        if (product_dtl_data == undefined) {
            return <TouchableOpacity style={{ padding: 8, backgroundColor: "#f5f5f5" }}></TouchableOpacity>
        }
        else if (product_dtl_data !== undefined) {
            if (product_dtl_data.vendor_product !== undefined) {
                var wishlist_items = wishlist_list.map((w) => {
                    return w.vendor_product_id;
                });
                return (
                    <View style={{ paddingHorizontal: 20 }}>
                        <View style={{ marginTop: 20 }}>
                            <Text style={{ fontSize: 34 }}>{product_dtl_data.vendor_product.vendor && product_dtl_data.vendor_product.vendor.title} </Text>
                            <Text style={{ fontSize: 34, fontWeight: "700" }}>{product_dtl_data.vendor_product.product.title} </Text>
                        </View>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 8 }}>
                            {product_dtl_data.vendor_product.review_summary.length > 0 && product_dtl_data.vendor_product.review_summary.map((r) =>
                                <View style={{ flexDirection: "row" }} >
                                    <Icon name="star" type="FontAwesome" style={{ color: pColorOne, fontSize: 18 }} />
                                    <Text>{' '}{r.rating}{' '} ({r.number_of_users})</Text>
                                </View>
                            )}
                            {product_dtl_data.vendor_product.review_summary.length == 0 &&
                                <View><Text>you are the first user here</Text></View>
                            }
                            {/*<Text style={{ fontSize: 40, color: "#F26D21" }}>*****</Text>*/}
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <TouchableOpacity disabled={product_dtl_data.quantity < 2} onPress={() => quantity_change(-1)} style={{ width: 34, height: 34, backgroundColor: "#f26d21", borderRadius: 8, alignItems: "center", justifyContent: "center", elevation: 1 }}>
                                    <Text style={{ color: "#fff", fontSize: 40 }}>-</Text>
                                </TouchableOpacity>
                                <Text style={{ marginHorizontal: 10 }}>{product_dtl_data.quantity}</Text>
                                <TouchableOpacity onPress={() => quantity_change(1)} style={{ width: 34, height: 34, backgroundColor: "#f26d21", borderRadius: 8, alignItems: "center", justifyContent: "center", elevation: 1 }}>
                                    <Text style={{ color: "#fff", fontSize: 40 }}>+</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flexDirection: "column", marginVertical: 10 }}>

                            {product_dtl_data.vendor_product.product.attributes.map((v) => (

                                <Text style={{ marginHorizontal: 5, marginVertical: 5 }}>{v.attribute_title}:{v.value}</Text>

                            ))}
                        </View>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ paddingBottom: 10 }} >

                            {product_dtl_data.vendor_product.variants.map((v) => (

                                <TouchableOpacity onPress={() => fetchProductDtlApi(v.id)} style={{ margin: 5, borderWidth: 1.5, borderRadius: 5, borderColor: "#f26d21", backgroundColor: v.id == product_dtl_data.vendor_product.id ? pColorOne : null }}>
                                    <View style={{ padding: 6 }}>
                                        {v.product.attributes.length > 0 &&
                                            v.product.attributes.map((a) => (
                                                <Text style={{ color: v.id == product_dtl_data.vendor_product.id ? '#fff' : null }}>
                                                    {a.attribute_title}-{a.value}
                                                </Text>

                                            ))}
                                    </View>
                                </TouchableOpacity>
                            ))}
                        </ScrollView >
                        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>

                            {/*<Button onPress={() => {addProductwishlistApi(product_dtl_data.vendor_product.id);ToastAndroid.show('Item added to WishList.', ToastAndroid.SHORT); }} style={{ backgroundColor: "#dcdcdc", width: sWidth - (sWidth / 8), height: 50, justifyContent: "center" }}>
                                                            <Icon type="FontAwesome" name="heart-o" style={{ fontSize: 15, color: "black", }} />
                                                            <Text style={{ color: "black", fontSize: 15, }}>WISH LIST</Text>
                                                        </Button>*/}
                            {wishlist_items.indexOf(product_dtl_data.vendor_product.id) !== -1 ?
                                (<Button style={{ backgroundColor: "#dcdcdc", width: sWidth - (sWidth / 8), height: 50, justifyContent: "center" }} onPress={() => { signInToken === null ? bottomModalFunc("signIn") : handleRemovewishlist(product_dtl_data.vendor_product.id); }}>
                                    <Icon type="FontAwesome" name="heart" style={{ color: "#000", fontSize: 15 }} />
                                    <Text style={{ color: "#000", fontSize: 15, }}>Wish List</Text>
                                </Button>)
                                : (<Button style={{ backgroundColor: "#dcdcdc", width: sWidth - (sWidth / 8), height: 50, justifyContent: "center" }} onPress={() => { signInToken === null ? bottomModalFunc("signIn") : addProductwishlistApi(product_dtl_data.vendor_product.id); }}>
                                    <Icon type="FontAwesome" name="heart-o" style={{ color: "#000", fontSize: 15 }} />
                                    <Text style={{ color: "#000", fontSize: 15, }}>Wish List</Text>
                                </Button>)}
                            <Button style={{ backgroundColor: pColorOne, width: sWidth - (sWidth / 8), height: 50, justifyContent: "center" }} onPress={() => { setCheckoutData(product_dtl_data.vendor_product.id, product_dtl_data.quantity); navigation.navigate('Checkout'); }}>
                                <Text style={{ color: "#fff", fontSize: 15, }}>BUY NOW</Text>
                            </Button>
                        </View>
                        {product_dtl_data.vendor_product.product.description !== '' && <View style={{ paddingVertical: 12 }}>
                                                    <Text style={{ fontWeight: "700" }}>Product Description</Text>
                                                    <Text style={{ color: "#616161" }}> {product_dtl_data.vendor_product.product.description}
                                                        {/*Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ea, non reprehenderit a rem sint ipsam voluptatum cupiditate iste hic saepe harum illo veritatis, doloribus necessitatibus corporis dolorum blanditiis, ut alias.*/}
                                                    
                                                    </Text>
                                                </View>}
                        <View style={{ paddingVertical: 12 }}>

                            <Text style={{ fontWeight: "700", paddingBottom: 10 }}>Rating & Reviews</Text>
                            {/*<View style={{ paddingVertical: 10 }}>
                                                            <StarRating />
                                                        </View>*/}
                            <View style={{
                                display: 'flex',
                                flexDirection: 'row',
                                // flexWrap: 'wrap',
                                justifyContent: 'flex-start',
                            }}>
                                <TouchableOpacity onPress={() => { signInToken === null ? bottomModalFunc("signIn") : navigation.navigate('ReviewScreen') }} style={{ padding: 5, borderWidth: 1.5, borderRadius: 5, borderColor: "#f26d21", }}>
                                    <Text>Write a review</Text>
                                </TouchableOpacity>
                            </View>
                            {/*   <View style={{ flexDirection: "row", alignItems: "center", borderWidth: .5, marginVertical: 5, borderRadius: 5 }}>
                                                         <Icon type="AntDesign" name="edit" style={{ fontSize: 16, paddingHorizontal: 5 }} />
                                                         <Input style={{ fontSize: 15, }}
                                                             underlineColorAndroid="transparent"
                                                             placeholder='Write a review'
                                                             placeholderTextColor="#616161"
                                                             autoCapitalize="none"
                                                         ></Input>
                                                     </View> */}


                            {product_dtl_data.vendor_product.product_reviews.length > 0 &&
                                product_dtl_data.vendor_product.product_reviews.map((r) => (

                                    <View style={{ paddingVertical: 5 }}>
                                        <View style={{ borderBottomWidth: 1, borderBottomColor: "#dcdcdc", paddingVertical: 5 }}>
                                            <View style={{ flexDirection: "row" }}>
                                                <View style={{ borderWidth: 0, borderRadius: 5, backgroundColor: "#f26d21" }}>
                                                    <Text style={{ paddingHorizontal: 10, color: "#fff" }}>{r.rating} *</Text>
                                                </View>
                                                <Text style={{ paddingHorizontal: 10, fontWeight: "700" }}>{r.title}</Text>
                                            </View>
                                            <View>
                                                <Text style={{ color: "#616161" }}>{r.content}.</Text>
                                                <View style={{ marginVertical: 5, flexDirection: "row" }}>
                                                    {/*<Text>John Deo</Text>*/}
                                                    <Text style={{ marginHorizontal: 20 }}>{r.created}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>

                                ))}
                        </View>
                        <RelatedProduct related={product_dtl_data.vendor_product.related_prods} />
                        <SimilarProduct similar={product_dtl_data.vendor_product.similar_prods} />
                    </View>
                )
            }
        }
    }

    return (
        <>
            {productDetail()}
        </>
    )
}

export default withNavigation(ProductDes)
