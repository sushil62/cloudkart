import React,{ useContext } from 'react'
import { View, Text, Image } from 'react-native'
import { CKart } from '../../../ContextApi'
import Carousel from 'react-native-banner-carousel'



const ProductImage = () => {
	const { product_dtl_data ,base_url} = useContext(CKart)
	const productImg=()=>{
	    if(product_dtl_data == undefined){
        return <TouchableOpacity style={{padding:8,backgroundColor:"#f5f5f5"}}></TouchableOpacity>
    }
    else if(product_dtl_data !== undefined){
        if(product_dtl_data.vendor_product !== undefined){
               var prod_images = [];
                    if (product_dtl_data.vendor_product.product.image) {
                      prod_images.push({image: product_dtl_data.vendor_product.product.image, id: 1});
                    }
                    if (product_dtl_data.vendor_product.product.image2) {
                      prod_images.push({
                        image: product_dtl_data.vendor_product.product.image2,
                        id: 2,
                      });
                    }
                    if (product_dtl_data.vendor_product.product.image3) {
                      prod_images.push({
                        image: product_dtl_data.vendor_product.product.image3,
                        id: 3,
                      });
                    }
                    if (product_dtl_data.vendor_product.product.image4) {
                      prod_images.push({
                        image: product_dtl_data.vendor_product.product.image4,
                        id: 4,
                      });
                    }

    return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center",top:-30 }}>
               <Carousel autoplay={ false}>
        {prod_images.length > 0 && 
        prod_images.map((p,index)=> {
	              return (
	            <View key={index} style={{alignItems: "center"}}>
	            
	                <Image style={{  width: 240, height: 200  }} source={{ uri: p.image }} />
	            </View>
	                
		            )
			      }
                     
            )}  
                </Carousel>
        </View>
    )
}
}
}
return (
        <>
        {productImg()}
        </>
    )
}
export default ProductImage

            // <Image style={{ width: 150, height: 200 }} source={{ uri: 'https://i.pinimg.com/originals/2d/77/c3/2d77c396afa27d5363678e04671ce97d.png' }} />