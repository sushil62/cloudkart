import React, { useContext } from 'react'
import { View, Text, TouchableOpacity,ScrollView,Image } from 'react-native'
import { FontAwesome } from '@expo/vector-icons';

import { CKart } from '../../../ContextApi'


const SimilarProduct = ({similar}) => {
// console.log(similar,'similar------------')
  const { currency,base_url,wishlist_list,handleRemovewishlist,addProductwishlistApi,loading,signInToken,loading_true } = useContext(CKart)


var wishlist_items = wishlist_list.map((w) => {
    return w.vendor_product_id;
  });

if (loading) {
      return (
        <View
          style={{
            backgroundColor: '#fff',
            flex: 1,
        justifyContent: 'center',
        alignItems: "center"
            
          }}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }

    return (
        <View style={{marginVertical:20}}>
        {similar == undefined || similar.length == 0 ? null:
        <Text style={{marginBottom:10,fontWeight: "700"}} >
        Products related to this category

        </Text>
        }
        <ScrollView horizontal >
        <View style={{flexDirection:'row'}}>
        {similar == undefined || similar.length == 0 ? null:<>{similar.map((d)=>{
            return(
                <View
            style={{
              borderWidth: 1,
              width: 160,
              marginRight: 16,
              borderRadius: 8,
              borderColor: '#dcdcdc',
            }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'flex-end', paddingRight: 12, paddingTop: 8, }}>


              {wishlist_items.indexOf(d.vendor_product_id) !== -1 ?
                (<TouchableOpacity onPress={() => { signInToken === null ? bottomModalFunc("signIn") : [loading_true(),handleRemovewishlist(d.vendor_product_id)] }}><View style={{ backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center', padding: 4, borderRadius: 16, right: -5, top: 0, width: 30, height: 30 ,borderWidth: 1, borderColor: '#f5f5f5',}}><FontAwesome style={{ color: 'orange' }} name="heart" size={15} /></View></TouchableOpacity>)
                : (<TouchableOpacity onPress={() => { signInToken === null ? bottomModalFunc("signIn") : [loading_true(),addProductwishlistApi(d.vendor_product_id)] }}><View style={{ backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center', padding: 4, borderRadius: 16, right: -5, top: 0, width: 30, height: 30 ,borderWidth: 1, borderColor: '#f5f5f5',}}><FontAwesome style={{ color: 'orange' }} name="heart-o" size={15} /></View></TouchableOpacity>)
              }



            </View>
            <TouchableOpacity onPress={() => { fetchProductDtlApi(d.vendor_product_id);loading_true();  }}>
            
            <View style={{ alignItems: 'center' }}>
                <Image source={{ uri: base_url + "/media/" + d.image_thumbnail }} style={{ width: 120, height: 120, resizeMode: "stretch" }} />

              
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <View style={{ borderWidth: 1, padding: 2, marginLeft: 12, width: 20, borderColor: 'green', }}>
                <View style={{ width: 12, height: 12, backgroundColor: 'green', borderRadius: 40, marginLeft: 1, }}></View>
              </View>

              {d.discount_percent > 0 &&  <View
                style={{
                  backgroundColor: 'red',
                  width: 40,
                  height: 20,
                  borderTopLeftRadius: 16,
                  borderBottomLeftRadius: 16,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text style={{ color: '#fff', fontSize: 12 }}>{d.discount_percent} %</Text>
              </View>
            }
            </View>
            <View style={{ padding: 12 }}>
              <Text style={{ fontSize: 12, color: '#999' }}>{d.vendor}</Text>
              <Text style={{ marginBottom: 8 }}>{d.product_title}</Text>
<View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text style={{ color: 'orange' }}> {currency}{' '}{d.discount_percent > 0 ? d.discounted_price : d.price}</Text>
                {/*<Text>10kg</Text>*/}
              </View>
            </View>
          </TouchableOpacity>
            </View>

                )

        })}</>}
        
        
        </View>
        </ScrollView>
            
        </View>
    )
}

export default SimilarProduct
