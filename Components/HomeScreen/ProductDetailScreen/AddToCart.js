import React, { useContext } from 'react'
import { View, Text, TouchableOpacity, ScrollView, Image, ToastAndroid } from 'react-native'
import { CKart } from '../../../ContextApi'
import { Icon } from 'native-base'

const AddToCart = () => {
	const { product_dtl_data, addToCartValidation, currency,valid_postcode } = useContext(CKart)
	return (
		<View style={{ backgroundColor: '#212121', padding: 20 }}>
			<View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItem: 'center' }}>
				<Text style={{ color: '#fff', fontSize: 24 }} >
					{currency}{' '}{product_dtl_data !== undefined ? product_dtl_data.vendor_product !== undefined ? product_dtl_data.vendor_product.discounted_price_incl_tax : null : null}
				</Text>
				<TouchableOpacity style={{ flexDirection: 'row', alignItem: 'center' }} onPress={() => { valid_postcode === undefined ? bottomModalFunc("zipcode") : addToCartValidation(product_dtl_data.vendor_product.vendor.id, product_dtl_data.vendor_product.id, product_dtl_data.vendor_product.product.slug, product_dtl_data.quantity);}}>
					<TouchableOpacity style={{ width: 34, height: 34, backgroundColor: "#F26D21", borderRadius: 8, alignItems: "center", justifyContent: "center", elevation: 1 }}>
						<Icon style={{ fontSize: 20, color: "#fff" }} type="Ionicons" name="cart" />
					</TouchableOpacity>
					<Text style={{ color: '#fff', fontSize: 14, paddingLeft: 8, paddingTop: 5 }}>AddToCart</Text>
				</TouchableOpacity>
			</View>
		</View>
	)
}
export default AddToCart
