import React, { useContext, useState } from 'react'
import { View, Text, TouchableOpacity, Dimensions,TextInput, ScrollView } from 'react-native'
// import Br from '../../Common/Br'
import { CKart } from '../../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Button, CheckBox, Item, Label, Input } from 'native-base'
import { Styles } from '../../../Stylesheet'
import ScrollAbleMenu from '.././ScrollAbleMenu'
import StarRating from './StarRating'

    // {product_dtl_data.vendor_product.product.title}

// import SearchBox from './SearchBox'
// import ProductAndOffer from './ProductAndOffer'
import { withNavigation } from "react-navigation";

const ReviewScreen = ({ navigation }) => {

    const { facetednav, product_count, filterDataFunc, filterId, filterValue, filterType, setFilterData, filterData,product_dtl_data, resetFilter, fatchFilteredDataApi,handleInputBox,writeAReviewApi } = useContext(CKart)
    
    const { pColorOne } = Styles
    // console.log(facetednav, "facetednav")
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 2 - (width / 20)
    return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
            <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
               <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center" }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon type="MaterialIcons" name="arrow-back" style={{ color: "#fff", fontSize: 20 }} />
                    </TouchableOpacity>
                    <Text style={{ color: "#fff", paddingLeft: 12, fontSize: 16, fontWeight: "700" }}>Write A Review</Text>
                </View>
                
            </View>

            <View style={{ marginVertical: 10, backgroundColor: "white", paddingHorizontal: 20, flex: 1, borderWidth: 0 }}>

                <View style={{ paddingVertical: 10 }}>

                <Label>Title of Your Review:</Label>
                    <TextInput style={{
                        height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1

                    }}
                        underlineColorAndroid="transparent"
                        placeholder="Enter Title"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        onChangeText={(text) => handleInputBox(text, "review_title")}
                    />
                </View>
                <View style={{ paddingVertical: 10 }}>
                <Label style={{paddingHorizontal: 10,paddingBottom:10}}>Rating</Label>
                <StarRating />
                </View>
                <View style={{ paddingVertical: 10 }}>

                <Label>Text of Your Reviews:</Label>
                    <TextInput style={{
                        // height: 40,
                        borderColor: '#dfdfdf',
                        borderBottomWidth: 1

                    }}
                        // underlineColorAndroid="transparent"review_rating
                        placeholder="Enter Reviews"
                        placeholderTextColor="#dfdfdf"
                        autoCapitalize="none"
                        multiline={true}
                        numberOfLines={4}
                        onChangeText={(text) => handleInputBox(text, "review_text")}
                    />
                </View>
                </View>
                

                   
                    <Button onPress={() => {writeAReviewApi(product_dtl_data.vendor_product.id);navigation.navigate('ProductDetailScreen')}} style={{ backgroundColor: pColorOne,  height: 40, justifyContent: "center" }}>
                        <Text >Submit</Text>
                    </Button>
        </View>
    )
}



export default withNavigation(ReviewScreen);
