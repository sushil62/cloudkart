import React, { useContext, useState } from 'react'
import { View } from 'react-native'
import { CKart } from '../../../ContextApi'
import { Icon } from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Styles } from '../../../Stylesheet'

const StarRating = () => {
    const { pColorOne } = Styles
    const { product_dtl_data, addToCart,review_rating,handleInputBox } = useContext(CKart)
    const starList = [1, 2, 3, 4, 5]
    // var [rating, setRating] = useState(null)

    return (
        <View style={{ flexDirection: "row" }}>
            {
                starList.map(m => {
                    return (
                        <TouchableOpacity onPress={() => handleInputBox(m,"review_rating")} style={{ paddingHorizontal: 2 }}>
                            {m <= review_rating ?
                                <Icon name="star" type="FontAwesome" style={{ color: pColorOne }} /> :
                                <Icon name="star-o" type="FontAwesome" />
                            }
                        </TouchableOpacity>
                    )
                })
            }
        </View>
    )
}
export default StarRating
