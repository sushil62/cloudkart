import React,{useContext} from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Icon } from 'native-base'
import { withNavigation } from 'react-navigation'
import { CKart } from '../../../ContextApi'



const Back_CartButton = ({navigation}) => {
  const {  cart_data } = useContext(CKart)
  const cart_length = cart_data !== undefined ? cart_data.cart_items !== undefined ? cart_data.cart_items : null : null

    return (
        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center",backgroundColor: "#f7f7f6",paddingHorizontal:20,paddingTop:50 }}>
            <TouchableOpacity onPress={() => navigation.goBack()} style={{ width: 34, height: 34, backgroundColor: "#fff", borderRadius: 8, alignItems: "center", justifyContent: "center",elevation:1 }}>
                <Icon style={{ fontSize: 20,color: "#F26D21" }} type="AntDesign" name="left" />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Cart')} style={{ width: 34, height: 34, backgroundColor: "#fff", borderRadius: 8, alignItems: "center", justifyContent: "center",elevation:1 }}>
                <Icon style={{ fontSize: 20, color: "#F26D21" }} type="Ionicons" name="cart" />
            {cart_length ? <View style={{ backgroundColor: "#F26D21", justifyContent: 'center', alignItems: 'center', padding: 4, borderRadius: 10, position: "absolute", right: -4, top: -2, width: 20, height: 20 }}><Text style={{ fontSize: 10,color: "#fff" }}>{cart_data.cart_items.length}</Text></View> : null}

            </TouchableOpacity>
        </View>
    )
}

export default withNavigation(Back_CartButton)
