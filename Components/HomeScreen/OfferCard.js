import React from 'react'
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native'

const OfferCard = () => {
    return (
        <View style={{ height: 200,width:"100%" }}>
            <TouchableOpacity style={{ marginRight: 12, height: 190 }}>
                <Image style={{ width: "100%", height: 190, borderRadius: 8 }} source={require('../../Images/offer.png')} />
            </TouchableOpacity>
        </View>
    )
}

export default OfferCard

