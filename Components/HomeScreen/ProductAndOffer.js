import React, { useContext } from 'react'
import { View, Text, ScrollView } from 'react-native'
import OfferCard from './OfferCard'
import PreferredVendors from './PreferredVendors'
import TopProductRows from './TopProductRows'
import TopBrands from './TopBrands'
import HomeBanner from './HomeBanner'
import ShpByVendor from './ShpByVendor'
import ShpByBrand from './ShpByBrand'
import ShpByCat from './ShpByCat'
import ProductList from './ProductList'
import ProductOffer from './ProductOffer'

import { CKart } from '../../ContextApi'


const ProductAndOffer = () => {
    const { select_mode } = useContext(CKart)
    // <OfferCard />
    return (
        <ScrollView style={{ flex: 1, backgroundColor: "#fff" }}>
            <View style={{ marginTop: 20, flex: 1, marginVertical: 5 }}>
                <HomeBanner />
            </View>
            <View style={{ marginVertical: 10,  flex: 1 }}>
                <View>
                    {/*<PreferredVendors />*/}
                    {/*<TopBrands />*/}
                    <TopProductRows />
                </View>
            </View>
        </ScrollView>
    )
}

export default ProductAndOffer
