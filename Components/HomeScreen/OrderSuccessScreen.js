import React, { useContext, useState } from 'react'
import { View, Text, TouchableOpacity, ScrollView, Dimensions, TextInput, } from 'react-native'
import { CKart } from '../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Button, Item, Label, Input, CheckBox, Radio } from 'native-base'
import { Styles } from '../../Stylesheet'
import { withNavigation } from 'react-navigation'
import StarRating from './ProductDetailScreen/StarRating'


const OrderSuccessScreen = ({ navigation }) => {
    const { selectModeChange } = useContext(CKart)
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 2 - (width / 17)
    const { pColorOne } = Styles

    return (
        <ScrollView style={{ flex: 1, backgroundColor: "#fff" }}>
            <View style={{ backgroundColor: "#F3F3F3", height: "auto", minHeight: 100, paddingVertical: 10, flexDirection: "row", }}>
                <Icon type='Ionicons' name='checkmark-circle' style={{ marginLeft: 20, color: 'green', fontSize: 30 }} />
                <View style={{ marginHorizontal: 10 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 20 }}>Order Successfully Placed</Text>
                    <Text style={{ marginTop: 10, fontWeight: 'bold', fontSize: 15 }}>ORDER ID #212321</Text>
                    <Text>An invoice has been sent to your email ID</Text>
                </View>
            </View>
            <View style={{ alignItems: "center", paddingVertical: 10 }}>
                <Text style={{ fontSize: 15, fontWeight: "bold", paddingVertical: 20 }}>Enjoying the App?</Text>
                <StarRating />
            </View>
            <View style={{ paddingHorizontal: 20, paddingVertical: 20 }}>
                <Text style={{ fontSize: 15, fontWeight: "bold" }}>Payment Details</Text>
                <View style={{ marginVertical: 10 }}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", paddingVertical: 2 }}>
                        <Text>Payment Status</Text>
                        <Text style={{ color: "green" }}>Successful</Text>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", paddingVertical: 2 }}>
                        <Text>Delivery Charge</Text>
                        <Text>50.00</Text>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", paddingVertical: 2 }}>
                        <Text style={{ color: "green" }}>Total Savings</Text>
                        <Text style={{ color: "green" }}>9.00</Text>
                    </View>
                </View>
            </View>
            <View style={{ paddingHorizontal: 20, paddingVertical: 20 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                    <Text style={{ fontSize: 15, fontWeight: "bold" }}>Shipment Details</Text>
                    <TouchableOpacity style={{ borderWidth: .5, borderRadius: 2, borderColor: pColorOne }}>
                        <Text style={{ paddingHorizontal: 12, paddingVertical: 5, color: pColorOne }}>VIEW ITEMS</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ marginVertical: 10 }}>
                    <Text>Standard Delivery</Text>
                    <Text style={{ fontSize: 14, fontWeight: "bold" }}>Delivery by 01 Jan, Tuesday</Text>
                </View>
            </View>
            <View style={{ paddingHorizontal: 20, paddingVertical: 20 }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                    <Text style={{ fontSize: 15, fontWeight: "bold" }}>Delivery to</Text>
                    <TouchableOpacity style={{ borderWidth: .5, borderRadius: 2, borderColor: pColorOne }}>
                        <Text style={{ paddingHorizontal: 5, paddingVertical: 5, color: pColorOne }}>TRACK ORDER</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ marginVertical: 10 }}>
                    <Text style={{ color: pColorOne }}>Home</Text>
                    <Text>Jaynagar, 4th phase,</Text>
                    <Text>Bengaluru 560066, +919876543210</Text>
                </View>
            </View>
            <Button disabled={false} style={{ backgroundColor: true ? pColorOne : "#FC9663", justifyContent: "center", height: 55, marginHorizontal: 20, marginVertical: 20 }} onPress={() => { navigation.navigate('HomeScreen') }}>
                <Text style={{ color: "#fff", fontSize: 16, }}>CONTINUE SHOPPING</Text>
            </Button>
        </ScrollView>
    )
}

export default withNavigation(OrderSuccessScreen)
