import React, { useContext } from 'react'
import { View, Text, ScrollView, Dimensions, TouchableOpacity, Image } from 'react-native'

import { CKart } from '../../ContextApi'
import { withNavigation } from 'react-navigation'



const TopBrands = ({ navigation }) => {
  const { offerData, base_url, fetchBrandProductApi } = useContext(CKart)
  const screenWidth = Math.round(Dimensions.get('window').width);

  const h = Dimensions.get('window').height;


  const handleBrands = () => {
    if (offerData !== undefined) {
      if (offerData.top_brands !== undefined) {

        return (
          <View style={{marginHorizontal: 20,}}>
            {offerData.top_brands.length > 0 && (
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 16,
                  paddingVertical: 10,
                }}>
                Top Brands
              </Text>
            )}
            <View style={{ flexDirection: 'row', flexWrap: "wrap" }}>
              {offerData.top_brands &&
                offerData.top_brands.map((v) => (

                  <TouchableOpacity
                    style={{
                      borderWidth: 1,
                      borderColor: '#dcdcdc',
                      width: 110,
                      height: 110,
                      marginRight: 6,
                      marginBottom: 6,
                      borderRadius: 8
                    }}
                    onPress={() => { fetchBrandProductApi(v.id); navigation.navigate('ProductListScreen') }}>
                    <View style={{ alignItems: 'center', }}>

                      <Image

                        source=
                        {{ uri: base_url + v.image }}

                        style={{ width: 105, height: 105 }}
                      />
                    </View>
                  </TouchableOpacity>
                ))}
            </View>
          </View>


        )


      }

    }


  }

  return (

    <>
      {handleBrands()}
    </>

  )

}
export default withNavigation(TopBrands)

/*<View style={{ padding: 12 }}>
                            <Text style={{ marginBottom: 8 }}>{v.name}</Text>
                          </View>*/
