import React, { useContext } from 'react'
import { View, Text, ScrollView, TouchableOpacity, Image, ToastAndroid, Dimensions, ActivityIndicator } from 'react-native'
import { FontAwesome } from '@expo/vector-icons';

import ProductListBanners from './ProductListBanners'
import { withNavigation } from 'react-navigation'

import _ from "underscore";
import { Icon } from 'native-base'




import { CKart } from '../../ContextApi'

const ProductList = ({ navigation }) => {
  var { screenwidth, screenheight } = Dimensions.get('window')
  const h = Dimensions.get('window').height;


  const { product_count,valid_postcode,cart_data, addToCartValidation, loading_true, loading, product_list, page_details, base_url, facetednav, select_mode, t_filter, fetchProductDtlApi, product_offers_rec, wishlist_list, previous_url, next_url, redirectpageApi, addProductwishlistApi, handleRemovewishlist, bottomModalFunc, signInToken, currency, getItmQty, incItmQty, decItmQty } = useContext(CKart)
  // console.log(page_details, "page_details -----------")
  // bottomModalFunc("signIn")
  var wishlist_items = wishlist_list.map((w) => {
    return w.vendor_product_id;
  });

  const page_header = () => {

    if (page_details !== undefined && !_.isEmpty(page_details)) {

      return (<View style={{ flexDirection: 'row', borderRadius: 8, margin: 8, backgroundColor: "#ADD8E6" }}>
        <View style={{ padding: 5 }}>
          <Image source={{ uri: page_details.logo ? page_details.logo : page_details.image }} style={{ width: 160, height: 100, resizeMode: 'contain' }} />
        </View>
        <View style={{ flexDirection: 'column', }}>
          <Text style={{ marginTop: 10, fontWeight: "700", paddingLeft: 5 }}>{page_details.title ? page_details.title : page_details.name}
          </Text>
          <Text style={{ marginTop: 5, width: 200 }}>{page_details.description}
          </Text>
        </View>
      </View>)
    }
  }

  if (loading) {
    return (
      <View
        style={{
          backgroundColor: '#fff',
          flex: 1,
          justifyContent: 'center',
          alignItems: "center"

        }}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }
  return (
    <ScrollView style={{ backgroundColor: "#fff" }}>
      <ProductListBanners />
      {page_header()}
      {product_list.length > 0 &&

        <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: '#bbb', borderBottomWidth: 1, borderTopColor: '#bbb', borderTopWidth: 1, marginBottom: 10 }}>
              <View style={{ alignItems: 'center', flex: 1, marginVertical: 8 }}>
      
              <TouchableOpacity  onPress={() => { bottomModalFunc("sorting"); }}>
                <Text>
                  <FontAwesome style={{ marginRight: 5 }} name="sort-amount-desc" size={15} />
                  <Text>{'   '}Sort By</Text>
                </Text>
                </TouchableOpacity>
              </View>
              <View style={{ alignItems: 'center', flex: 1, marginVertical: 8 }}>
              <TouchableOpacity  onPress={() => { navigation.navigate('Filter') }}>
      
                <Text>
                 <FontAwesome style={{ marginRight: 5 }} name="filter" size={15} />
                     <Text>{'   '}Filter</Text>
                </Text>
                </TouchableOpacity>
              </View>
            </View>
      
      }
       {/* <View style={{ alignItems: 'flex-end', margin: 10, }}>
                 <TouchableOpacity style={{
                   alignItems: 'center', height: h / 26,
                   width: h / 10,
                   borderWidth: 1,
                   borderColor: '#bbb',
                   borderRadius: 12, padding: 5
                 }} onPress={() => { navigation.navigate('Filter') }}>
                   <Text>
                     <FontAwesome style={{ marginRight: 5 }} name="filter" size={15} />
                     <Text>{'   '}Filter</Text>
                   </Text>
                 </TouchableOpacity>
               </View>*/}

      <View style={{ flexDirection: 'row', flexWrap: "wrap", marginHorizontal: 18, justifyContent: 'space-between' }}>
        {product_list.length > 0 && product_list.map((p) => (

          <View key={p.vendor_product_id} style={{ borderWidth: 1, width: 150, marginVertical: 6, marginHorizontal: 6, borderRadius: 8, borderColor: "#f5f5f5", }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'flex-end', paddingRight: 12, paddingTop: 8, }}>


              {wishlist_items.indexOf(p.vendor_product_id) !== -1 ?
                (<TouchableOpacity onPress={() => { signInToken === null ? bottomModalFunc("signIn") : [loading_true(), handleRemovewishlist(p.vendor_product_id)] }}><View style={{ backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center', padding: 4, borderRadius: 16, right: -5, top: 0, width: 30, height: 30, borderWidth: 1, borderColor: '#f5f5f5', }}><FontAwesome style={{ color: 'orange' }} name="heart" size={15} /></View></TouchableOpacity>)
                : (<TouchableOpacity onPress={() => { signInToken === null ? bottomModalFunc("signIn") : [loading_true(), addProductwishlistApi(p.vendor_product_id)] }}><View style={{ backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center', padding: 4, borderRadius: 16, right: -5, top: 0, width: 30, height: 30, borderWidth: 1, borderColor: '#f5f5f5', }}><FontAwesome style={{ color: 'orange' }} name="heart-o" size={15} /></View></TouchableOpacity>)
              }

            </View>
            <TouchableOpacity onPress={() => { fetchProductDtlApi(p.vendor_product_id); loading_true(); navigation.navigate('ProductDetailScreen') }}>
              <View style={{ alignItems: 'center', paddingTop: 10, }}>
                <Image source={{ uri: base_url + "/media/" + p.image_thumbnail }} style={{ width: 120, height: 120, resizeMode: "stretch" }} />
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                <View style={{ borderWidth: 1, padding: 2, marginLeft: 12, width: 20, borderColor: 'green', }}>
                  <View style={{ width: 12, height: 12, backgroundColor: 'green', borderRadius: 40, marginLeft: 1, }}></View>
                </View>
                {p.discount_percent > 0 && (
                  <View
                    style={{ backgroundColor: 'red', width: 40, height: 20, borderTopLeftRadius: 16, borderBottomLeftRadius: 16, alignItems: 'center', justifyContent: 'center', }}>
                    <Text style={{ color: '#fff', fontSize: 12 }}>- {p.discount_percent} %</Text>
                  </View>
                )}
              </View>
              <View style={{ padding: 12 }}>
                <Text style={{ fontSize: 12, color: '#999' }}>{p.vendor}</Text>
                <Text numberOfLines={1} >{p.product_title}</Text>
                <View
                  style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                  <Text style={{ color: 'orange' }}> {currency}{' '}{p.discount_percent > 0 ? p.discounted_price : p.price}</Text>
                  {/*<Text>10kg</Text>*/}
                </View>
              </View>

              <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 25, marginVertical: 10, alignItems: "center" }}>
                <TouchableOpacity onPress={() => decItmQty(p.product_slug)} style={{ borderRadius: 5, width: 26, height: 26, backgroundColor: "#F26D21", alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ color: "#fff", }}>-</Text>
                </TouchableOpacity>
                <Text>{getItmQty(p.product_slug) ? getItmQty(p.product_slug) : 1}</Text>
                <TouchableOpacity onPress={() => incItmQty(p.product_slug)} style={{ borderRadius: 5, width: 26, height: 26, backgroundColor: "#F26D21", alignItems: "center", justifyContent: "center" }}>
                  <Text style={{ color: "#fff", }}>+</Text>
                </TouchableOpacity>
              </View>

              <TouchableOpacity style={{ backgroundColor: "#F26D21", borderRadius: 8, alignItems: "center", justifyContent: "center", margin: 5, }} onPress={() => { valid_postcode === undefined ? bottomModalFunc("zipcode") : addToCartValidation(p.vendor_id, p.vendor_product_id, p.product_slug, getItmQty(p.product_slug) ? getItmQty(p.product_slug) : 1); }}>
                <Text style={{ color: '#fff', fontSize: 14, padding: 7, }}>Add To Cart</Text>
              </TouchableOpacity>
            </TouchableOpacity>
            {cart_data !== undefined && cart_data.cart_items !== undefined  && cart_data.cart_items.map(c => c.vendor_product.id === p.vendor_product_id? 
            <View style={{ alignItems: "center", justifyContent: "center", margin: 5,}}><Text style={{color:"green",fontSize:16}}>{c.quantity} in basket</Text></View> : null
              )  }
          </View>
        ))}
      </View>
      {product_list.length == 0 &&
        <Text style={{ alignSelf: 'center' }}> Sorry No products are available
        </Text>
      }
      {product_list.length > 0 &&
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'center', marginVertical: 10 }}>
          <TouchableOpacity onPress={previous_url !== null ? () => { loading_true(); redirectpageApi(previous_url) } : null} style={{ justifyContent: "center", borderWidth: previous_url !== null ? 1 : null, marginRight: 10, borderRadius: previous_url !== null ? 5 : null }} >
            <Text style={{ paddingHorizontal: 60, paddingVertical: 15 }}> Previous </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={next_url !== null ? () => { loading_true(); redirectpageApi(next_url) } : null} style={{ justifyContent: 'center', borderWidth: next_url !== null ? 1 : null, borderRadius: next_url !== null ? 5 : null }}>
            <Text style={{ paddingHorizontal: 60, paddingVertical: 15 }}>
              Next
      </Text>
          </TouchableOpacity>
        </View>
      }


    </ScrollView>



  )
}
export default withNavigation(ProductList)