// WishListScreen.js

import React, { useContext } from 'react'

import { withNavigation } from 'react-navigation'
import { CKart } from '../../../ContextApi'
import { FontAwesome } from '@expo/vector-icons';



import { Text, View, TouchableOpacity, ScrollView, Dimensions, Image } from 'react-native';
import { Styles } from '../../../Stylesheet'
import { Icon, Button } from 'native-base'

const WishListScreen = ({ navigation }) => {
  // cart_data.cart_items
  // onPress={() => removeWishlistItemApi(p.id)}
  const { wishlistApi, wishlist_list, base_url, fetchProductDtlApi, removeWishlistItemApi, signInToken,currency } = useContext(CKart)
  // console.log(wishlist_list,'wishlist_list')
  const { pColorOne } = Styles
  const { width, height } = Dimensions.get('window')
  const sWidth = width / 2

  // if (signInToken !== null) {
  //   wishlistApi();
  // }



  return (
    <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
      <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
        <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center" }}>
          
          <Text style={{ color: "#fff", fontSize: 20, fontWeight: 'bold', paddingLeft: 10 }}>WishList</Text>

        </View>
      </View>
      <View style={{ flex: 1, marginTop: 5, backgroundColor: "white" }}>
        <ScrollView>
          <View style={{ flexDirection: 'row', flexWrap: "wrap", marginHorizontal: 18 }}>
            {wishlist_list.length > 0 && wishlist_list.map((p) => (

              <TouchableOpacity key={p.id} style={{ borderWidth: 1, width: 150, marginVertical: 6, marginHorizontal: 6, borderRadius: 8, borderColor: "#f5f5f5", }}>
                <TouchableOpacity onPress={() => removeWishlistItemApi(p.id)}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'flex-end', paddingRight: 6, paddingTop: 8, }}>
                    <Icon type="EvilIcons" style={{ color: 'orange' }} name="close-o" size={15} />

                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { fetchProductDtlApi(p.vendor_product_id); navigation.navigate('ProductDetailScreen') }}>
                  <View style={{ alignItems: 'center', paddingTop: 10, }}>
                    <Image source={{ uri: base_url + "/media/" + p.vp_image }} style={{ width: 120, height: 120 }} />
                  </View>
                </TouchableOpacity>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', }}>
                  <View style={{ borderWidth: 1, padding: 2, marginLeft: 12, width: 20, borderColor: 'green', }}>
                    <View style={{ width: 12, height: 12, backgroundColor: 'green', borderRadius: 40, marginLeft: 1, }}></View>
                  </View>
                  {p.vp_discount_percent > 0 && (
                    <View
                      style={{ backgroundColor: 'red', width: 40, height: 20, borderTopLeftRadius: 16, borderBottomLeftRadius: 16, alignItems: 'center', justifyContent: 'center', }}>
                      <Text style={{ color: '#fff', fontSize: 12 }}>{p.vp_discount_percent} %</Text>
                    </View>
                  )}
                </View>
                <View style={{ padding: 12 }}>
                  <Text style={{ fontSize: 12, color: '#999' }}>{p.vendor}</Text>
                  <Text >{p.product}</Text>
                  {/*<Text style={{ marginBottom: 8 }}>{p.product}</Text>*/}
                  <View
                    style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                    <Text style={{ color: 'orange' }}> {currency}{' '}{p.vp_price}</Text>
                    {/*<Text>10kg</Text>*/}
                  </View>
                </View>
              </TouchableOpacity>
            ))}

          </View>
          {wishlist_list !== undefined && wishlist_list.length == 0 &&
            <View style={{ alignItems: "center", paddingTop: 20 }}>
              <Text style={{ fontSize: 17 }}>No Products present</Text>
              <View style={{ paddingTop: 20 }}>
                <Button style={{ backgroundColor: pColorOne, width: sWidth + sWidth / 4, height: 40, justifyContent: "center" }} onPress={() => { navigation.navigate('HomeScreen'); }}>
                  <Text style={{ color: "#fff", fontSize: 18, }}>CONTINUE SHOPPING</Text>
                </Button>
              </View>
            </View>
          }
        </ScrollView>
      </View>

    </View>
  )
}

export default withNavigation(WishListScreen);


