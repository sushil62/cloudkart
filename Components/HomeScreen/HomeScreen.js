import React, { useContext } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Br from '../../Common/Br'
import HeaderMenu from '../../Common/HeaderMenu'
import { CKart } from '../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon } from 'native-base'
import {Styles} from '../../Stylesheet'
import ScrollAbleMenu from './ScrollAbleMenu'
import StoreList from './StoreList'
import SearchBox from './SearchBox'
import ProductAndOffer from './ProductAndOffer'

const HomeScreen = ({ navigation }) => {
    const {selectModeChange,vendorSlugChange,vendor_slug} = useContext(CKart)

    const {pColorOne} = Styles
    // if (vendor_slug !== undefined && vendor_slug !== null ) {
	   //  vendorSlugChange();
    // }
    return (
        <View style={{backgroundColor:pColorOne , flex: 1, paddingTop: 50 }}>

            <HeaderMenu iconSize={25} children2={<StoreList />} headerTitle="Cloudkart360" iconType="MaterialIcons" iconName="menu" pressFunc={() => navigation.openDrawer()}>

                <ProductAndOffer/>
            </HeaderMenu>
            


        </View>
    )
}

export default HomeScreen
