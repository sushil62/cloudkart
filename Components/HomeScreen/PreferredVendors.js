import React, { useContext } from 'react'
import { View, Text, ScrollView, Dimensions, TouchableOpacity, Image } from 'react-native'
import { withNavigation } from 'react-navigation'

import { CKart } from '../../ContextApi'


const PreferredVendors = ({ navigation }) => {
  const { offerData, base_url, fetchVendorProductApi } = useContext(CKart)
  const screenWidth = Math.round(Dimensions.get('window').width);

  const h = Dimensions.get('window').height;

  return (
    <View style={{ marginHorizontal: 20, }}>
      {offerData.preferred_vendors && (

        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 16,
            paddingVertical: 10,
          }}>
          Top Vendors
        </Text>
      )}
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={{ height: 180, }}>
        {offerData.preferred_vendors &&
          offerData.preferred_vendors.map((v) => (

            <TouchableOpacity
              style={{
                borderWidth: 1,
                width: 160,
                marginRight: 10,
                // marginHorizontal: 5,
                borderRadius: 8,
                borderColor: '#f5f5f5',
                backgroundColor: "#f5f5f5"

              }}
              key={v.id}
              onPress={() => { fetchVendorProductApi(v.id); navigation.navigate('ProductListScreen') }}
            >
              <View style={{ alignItems: 'center', paddingBottom: 20, }}>
                <View style={{ padding: 12 }}>
                  <Text style={{ marginBottom: 8, color: 'orange' }}>{v.name}</Text>
                </View>
                <Image

                  source=
                  {{ uri: base_url + v.logo }}

                  style={{ width: 160, height: 100, resizeMode: "stretch", marginTop: 10 }}
                />
              </View>
            </TouchableOpacity>
          ))}
      </ScrollView>
    </View>
  )

}
export default withNavigation(PreferredVendors)

/*onPress={() => this.onVendorSelect(v.id)}*/