import React, { useContext } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Br from '../../Common/Br'
import { CKart } from '../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon } from 'native-base'
import { Styles } from '../../Stylesheet'
import ScrollAbleMenu from './ScrollAbleMenu'
import StoreList from './StoreList'
import SearchBox from './SearchBox'
import ShpByVendor from './ShpByVendor'

import HeaderMenu from '../../Common/HeaderMenu'



const ShpByVendorScreen = ({ navigation }) => {
    const { selectModeChange } = useContext(CKart)

    const { pColorOne } = Styles
    return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
            <HeaderMenu iconSize={20} children2={<StoreList />} iconType="AntDesign" iconName="left" pressFunc={() => navigation.goBack()}>
                <ShpByVendor />
            </HeaderMenu>
        </View>

    )
}

export default ShpByVendorScreen
