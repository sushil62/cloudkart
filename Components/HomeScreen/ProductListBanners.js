import React, { useContext } from 'react'
import { View, Text, ScrollView, TouchableOpacity, Image,Dimensions } from 'react-native'
import { FontAwesome } from '@expo/vector-icons';

import { withNavigation } from 'react-navigation'

import Carousel from 'react-native-banner-carousel'




import { CKart } from '../../ContextApi'

const ProductListBanners = ({navigation}) => {
  const { product_offers_rec ,base_url} = useContext(CKart)
  // console.log(product_offers_rec,"product_offers_rec")
 const screenWidth = Math.round(Dimensions.get('window').width);

    const h = Dimensions.get('window').height;
    if (product_offers_rec !== undefined) {

	return (
	
            <View style={{paddingBottom:20}}>
           <> 
	{product_offers_rec.length >0 &&
          product_offers_rec.map((r) => (
            <View>
	              {r.row_items.length >0 &&
	              	r.row_items.map((d) => (
            <View>
	            <Carousel>
        {d.page_chunk.page_chunks.length > 0 && 
        d.page_chunk.page_chunks.map((p,index)=> {
	              return (
	            <View key={index}>
	            
	                <Image style={{ height: screenWidth / 2.5,width: screenWidth-10,marginHorizontal:5,marginTop:5 }} source={{ uri: base_url + p.url }} />
	            </View>
	                
		            )
			      }
                     
            )}  
                </Carousel>
	            </View>
	              	
            ))}
        </View>
            ))}
        </>
    
        </View>

)
    }

}
export default ProductListBanners