import React, { useContext } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { CKart } from '../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Button } from 'native-base'
import { Styles } from '../../Stylesheet'
import { withNavigation } from 'react-navigation'
import OrderSuccessScreen from './OrderSuccessScreen';

const OrderSuccess = ({ navigation }) => {
    const { selectModeChange } = useContext(CKart)

    const { pColorOne } = Styles
    return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
            <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center" }}>

            </View>
            <View style={{ flex: 1 }}>
                <OrderSuccessScreen />
            </View>
        </View>
    )
}

export default withNavigation(OrderSuccess)