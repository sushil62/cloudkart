import React, { useContext } from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import { CKart } from '../../ContextApi'
import { withNavigation } from 'react-navigation'
import { FontAwesome } from '@expo/vector-icons';


const ShpByCat = ({ navigation }) => {
    const { cat_list, fetchCatProductApi, selectDropDown,loading_true, select_cat, toggleDropDown, toggle_dropdown, selectChildrenDropDown, select_pdct, toggle_children_dropdown } = useContext(CKart)

    return (
        <View style={{ backgroundColor: '#fff', flex: 1, paddingVertical: 30 }} >
            <ScrollView>
                <View style={{ flexDirection: 'column' }}>
                    {cat_list == undefined ? <Text>No data found</Text> : <>
                        {cat_list.map((v) => {
                            const childVar = v.children.length > 0
                            return (
                                <View key={v.id}>
                                    <View>
                                        <TouchableOpacity 
                                            style={{ borderBottomWidth: 0.5, borderBottomColor: "#dcdcdc", paddingVertical: 12 }}
                                            onPress={() => { childVar ? selectDropDown(v.id) : [navigation.navigate('ProductListScreen'), fetchCatProductApi(v.id),loading_true()] }}>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20 }}>
                                                <Text style={{ color: "#424242", fontWeight: "bold" }}>{v.name}</Text>
                                                {childVar &&
                                                    <FontAwesome style={{ marginRight: 5 }} name={select_cat === v.id && !toggle_dropdown ? "caret-up" : "caret-down"} size={15} />
                                                }
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    {select_cat == v.id ? (<View>
                                        {childVar && v.children.map((p) => {
                                            const secChildVar = p.children.length > 0
                                            return (
                                                <View key={p.id} style={{ borderBottomWidth: 0.5, borderBottomColor: "#dcdcdc", paddingVertical: 12, marginHorizontal: 30 }}>
                                                    <TouchableOpacity 
                                                        onPress={() => { secChildVar ? selectChildrenDropDown(p.id) : [navigation.navigate('ProductListScreen'), fetchCatProductApi(p.id),loading_true()] }}>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                                            <Text style={{ color: "#424242" }}>{p.name} </Text>
                                                            {secChildVar &&
                                                                <FontAwesome style={{ marginRight: 5 }} name={select_pdct === p.id && !toggle_children_dropdown ? "caret-up" : "caret-down"} size={15} />
                                                            }
                                                        </View>
                                                    </TouchableOpacity>
                                                    {select_pdct === p.id ? (
                                                        <View>
                                                            {p.children.map((m) => {
                                                                return (
                                                                    <TouchableOpacity key={m.id}
                                                                        style={{ paddingVertical: 12 }}
                                                                        onPress={() => { fetchCatProductApi(m.id);loading_true(); navigation.navigate('ProductListScreen') }}>
                                                                        <View style={{ marginHorizontal: 10 }}>
                                                                            <Text style={{ color: "#424242" }}>{m.name} </Text>
                                                                        </View>
                                                                    </TouchableOpacity>
                                                                )
                                                            })}
                                                        </View>
                                                    ) : null}
                                                </View>
                                            )
                                        })}
                                    </View>) : null}
                                </View>
                            )
                        })}
                    </>}
                </View>
            </ScrollView>
        </View>
    )
}
export default withNavigation(ShpByCat)