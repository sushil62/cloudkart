import React, { useContext } from 'react'
import { View, Text, ScrollView, Dimensions, TouchableOpacity, Image } from 'react-native'

import { CKart } from '../../ContextApi'

import { withNavigation } from 'react-navigation'

const { width, height } = Dimensions.get('window')
const sWidth = width - 81

const ShpByBrand = ({ navigation }) => {
  const { loading_true,brand_list, fetchBrandProductApi, base_url } = useContext(CKart)
  return (
    <View style={{ backgroundColor: '#fff', flex: 1 }} >
      <ScrollView>
        <View style={{ alignItems: 'center', paddingBottom: 20 }}>

        </View>
        <View >
          {brand_list == undefined ? <Text>No data found</Text> : <>
            {brand_list.map((b, indx) => (
              <View key={indx} style={{ flexDirection: 'row', flexWrap: "wrap", marginHorizontal: 20, justifyContent: 'space-between' }}>
                {b.brands.map((v) => (
                  <TouchableOpacity
                    key={v.id}
                    style={{
                      width: sWidth / 1.8,
                      // height: 50,
                      // marginHorizontal: 20,
                      marginBottom: 10,
                      borderRadius: 8,
                      backgroundColor: "#f5f5f5"


                    }} onPress={() => { fetchBrandProductApi(v.id);loading_true(); navigation.navigate('ProductListScreen') }} >
                    <View style={{ alignItems: 'center', paddingVertical: 10 }}>
                      <Image

                        source=
                        {{ uri: base_url + "/media/" + v.img_url }}

                        style={{ width: sWidth / 2 - 10, height: 120, resizeMode: "contain" }}
                      />
                    </View>
                    <View style={{ alignItems: 'center', paddingBottom: 20, }}>
                      <View style={{ padding: 12 }}>
                        <Text style={{ marginBottom: 8 ,fontSize:16,fontWeight:"700",color:"orange"}}>{v.name}</Text>
                      </View>

                    </View>
                  </TouchableOpacity>
                ))}
              </View>
            ))}</>}
        </View>
      </ScrollView>
    </View>

  )
}
export default withNavigation(ShpByBrand)