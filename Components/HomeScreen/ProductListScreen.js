import React, { useContext } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Br from '../../Common/Br'
import { CKart } from '../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon } from 'native-base'
import { Styles } from '../../Stylesheet'
import ScrollAbleMenu from './ScrollAbleMenu'
import SearchBox from './SearchBox'
import ProductList from './ProductList'
import ListScreen from '../../Common/ListScreen'
import { withNavigation } from 'react-navigation'

const ProductListScreen = ({ navigation }) => {
    const { selectModeChange } = useContext(CKart)

    const { pColorOne } = Styles
    return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>

            <ListScreen iconSize={20} iconType="AntDesign" iconName="left" pressFunc={() => {navigation.goBack();}}>
            <View style={{ flex: 1 }}>
		        <ScrollAbleMenu />
		        <SearchBox />
		        
		        <ProductList />
		      </View>
            </ListScreen>


        </View>


    )
}

export default withNavigation(ProductListScreen)