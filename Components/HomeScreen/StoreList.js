import React,{useContext} from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import {CKart} from '../../ContextApi'

import { withNavigation } from 'react-navigation'

const StoreList = ({navigation}) => {
    const {scrollfunc,scrollMenuSelection} = useContext(CKart) 
    // const scrollmenu = [
    //     { id:1,title:'Shop All',keyname:'category',pressFunc:()=> {scrollfunc(1); navigation.navigate('ShpByCatScreen')}},
    //     { id:2,title:'Shop By Brand',keyname:'Brand',pressFunc:()=> {scrollfunc(2); navigation.navigate('ShpByBrandScreen')}},
    //     { id:3,title:'Shop By Store',keyname:'Store',pressFunc:()=> {scrollfunc(3); navigation.navigate('ShpByVendorScreen')}},
    // ] style={{ color:scrollMenuSelection == menu.id ? "#fff" : "#000" }}

    return (
        <View style={{marginTop:10,marginHorizontal: 20}}>
            {/* <ScrollView horizontal> */}
                <View style={{ flexDirection: "row",justifyContent:"space-between"}}>
                            <TouchableOpacity  onPress= {()=> {scrollfunc(3); navigation.navigate('ShpByVendorScreen')}} >
                            <Text >Shop By Store</Text>
                            </TouchableOpacity>
                    
                </View>
            {/* </ScrollView> */}
        </View>
    )
}

export default withNavigation(StoreList)

                // {scrollmenu.map(menu =>{
                //     return (
                //         )
                // })}

