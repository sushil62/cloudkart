import React, { useContext } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Br from '../../Common/Br'
import { CKart } from '../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon } from 'native-base'
import {Styles} from '../../Stylesheet'
import ScrollAbleMenu from './ScrollAbleMenu'
import SearchBox from './SearchBox'
import ShpByBrand from './ShpByBrand'
import HeaderMenu from '../../Common/HeaderMenu'



const ShpByBrandScreen = ({ navigation }) => {
    const {selectModeChange} = useContext(CKart)

    const {pColorOne} = Styles
    return (
        <View style={{backgroundColor:pColorOne , flex: 1, paddingTop: 50 }}>
       
            <HeaderMenu iconSize={20} children2={<ScrollAbleMenu />} iconType="AntDesign" iconName="left"  pressFunc={() => navigation.goBack()}>
                <ShpByBrand />
            </HeaderMenu>


        </View>
    )
}

export default ShpByBrandScreen
