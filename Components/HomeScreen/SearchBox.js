import React, { useContext, useState } from 'react'
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native'
import { Icon } from 'native-base'
import { withNavigation } from 'react-navigation'
import { CKart } from '../../ContextApi'
import Autocomplete from 'react-native-autocomplete-input';

const SearchBox = ({ navigation }) => {

    const { handleInputBox, fetchSearchApi, search_query, suggestions } = useContext(CKart)
    const data_list = suggestions
    const [filteredData, setFilteredData] = useState([]); // Filtered data
    const [selectedValue, setSelectedValue] = useState({}); // selected data
    const findData = (query) => {
        handleInputBox(query, "search_query")
        if (query) {
            const regex = new RegExp(`${query.trim()}`, 'i');
            setFilteredData(
                data_list.filter((data) => data.product_title.search(regex) >= 0)
            );
        } else {
            setFilteredData([]);
        }
    };

    return (
        <View style={{ marginHorizontal: 20, marginTop: 10, elevation: 1, top: 16, borderWidth: 0.5, borderRadius: 8, borderColor: "#f5f5f5" }}>
            <TouchableOpacity onPress={() => navigation.navigate('SearchField')}>
                <View style={{ backgroundColor: "#fff", flexDirection: "row", borderRadius: 8, alignContent: "center", alignItems: "center", height: 35 }}>
                    <Icon type="AntDesign" name="search1" style={{ fontSize: 18, color: "#999", paddingLeft: 8 }} />
                    <Text style={{ color: "#dcdcdc", paddingHorizontal: 10 }}>Search for the product here...</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}



export default withNavigation(SearchBox)
