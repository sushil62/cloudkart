import React, { useContext } from 'react'
import { View, Text, TouchableOpacity, Dimensions,ActivityIndicator } from 'react-native'
import { CKart } from '../../ContextApi'
import { Icon } from 'native-base'
import { Styles } from '../../Stylesheet'
import { withNavigation } from 'react-navigation'
import Back_CartButton from './ProductDetailScreen/Back_CartButton'
import ProductImage from './ProductDetailScreen/ProductImage'
import ProductDes from './ProductDetailScreen/ProductDes'
import { ScrollView } from 'react-native-gesture-handler'
import AddToCart from './ProductDetailScreen/AddToCart'


const { width, height } = Dimensions.get('window')

const ProductDetailScreen = ({ navigation }) => {
    const { loading,selectModeChange ,product_dtl_data} = useContext(CKart)

    // console.log("product_dtl_data",product_dtl_data.vendor_product)

    const { pColorOne } = Styles

    if (loading) {
      return (
        <View
          style={{
            backgroundColor: '#fff',
            flex: 1,
        justifyContent: 'center',
        alignItems: "center"
            
          }}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }
    return (
        <View style={{ backgroundColor: "#fff", flex: 1, borderWidth: 3 }}>
            <Back_CartButton />
            <ScrollView>
                <View style={{ backgroundColor: "#f7f7f6", paddingHorizontal: 20, width, height: 300, paddingTop: 50, borderBottomLeftRadius: 140, borderBottomRightRadius: 140 }}>
                    <ProductImage />
                </View>
                <View style={{  }}>
                    <ProductDes />
                </View>
            </ScrollView>
            <AddToCart />
        </View>


    )
}

export default withNavigation(ProductDetailScreen)



