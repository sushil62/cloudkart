import React, { useContext, useState } from 'react'
import { View, Text, TouchableOpacity, ScrollView, Dimensions, TextInput, Modal, StyleSheet, } from 'react-native'
import { CKart } from '../../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Button, Item, Label, Input, CheckBox, Radio } from 'native-base'
import { Styles } from '../../../Stylesheet'
import { withNavigation } from 'react-navigation'


const CheckoutScreen = ({ navigation }) => {
    const { selectModeChange, addressListData,defaultpostAddressApi,delivery_option_change,delivery_option,delivery_options,delivery_choice_change,delivery_choice,delivery_slot_change,delivery_slot } = useContext(CKart)
    // let addressList = []
    // if (addressListData.address) {
    //     addressList = addressListData.address
    // }
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 2 - (width / 17)
    const { pColorOne } = Styles

    const deliveryDate = [
        { id: 1, date: "03 Aug", day: "SUN" },
        { id: 2, date: "04 Aug", day: "MON" },
        { id: 3, date: "05 Aug", day: "TUE" },
        { id: 4, date: "06 Aug", day: "WED" },]
    const [selectedDate, setSelectedDate] = useState(deliveryDate[0].id);

    const deliveryTime = [
        { id: 1, startTime: "6 AM", endTime: "9 AM", status: "" },
        { id: 2, startTime: "9 AM", endTime: "12 PM", status: "" },
        { id: 3, startTime: "12 PM", endTime: "3 PM", status: "Unavailable" },
        { id: 4, startTime: "3 PM", endTime: "6 PM", status: "" },
    ]

//     const del_options = [
//  {"id": 10, "name": "store delivery",
//         "del_choices": [],
//         "del_slots": [
//         {"id": "1-25", "name": "2 to 4 Slot - 2020-10-21"},
//         {"id": "2-22", "name": "8 to 10 Slot - 2020-10-22"},
//         {"id": "2-23", "name": "10 t0 12 Slot - 2020-10-22"},
//         {"id": "2-24", "name": "12 to 2 Slot - 2020-10-22"},
//         {"id": "2-25", "name": "2 to 4 Slot - 2020-10-22"},
//         {"id": "2-26", "name": "8 to 10 Saturday Slot - 2020-10-22"},
//         {"id": "2-27", "name": "10 to 12 Saturday - 2020-10-22"}]},  
        
//         {"id": 1, "name": "Next Day",
//         "del_choices": [
//         {"id": 1, "name": "Deliver to neighbour"},
//         {"id": 2, "name": "Deliver to doorstep only"},
//         {"id": 3, "name": "Leave safe at Doorstep"}],
// "del_slots": []},
        
//         {"id": 2, "name": "Next Day 12:00",
//         "del_choices": [
//         {"id": 1, "name": "Deliver to neighbour"},
//         {"id": 2, "name": "Deliver to doorstep only"},
//         {"id": 3, "name": "Leave safe at Doorstep"}],
// "del_slots": []},
        
//         {"id": 3, "name": "Next Day 10:30",
//         "del_choices": [
//         {"id": 1, "name": "Deliver to neighbour"},
//         {"id": 2, "name": "Deliver to doorstep only"},
//         {"id": 3, "name": "Leave safe at Doorstep"}],
// "del_slots": []},
        
//         {"id": 4, "name": "Next Day 09:00",
//         "del_choices": [
//         {"id": 2, "name": "Deliver to doorstep only"}],
// "del_slots": []},
        
//         {"id": 5, "name": "Saturday",
//         "del_choices": [
//         {"id": 1, "name": "Deliver to neighbour"},
//         {"id": 2, "name": "Deliver to doorstep only"},
//         {"id": 3, "name": "Leave safe at Doorstep"}],
// "del_slots": []},
        
//         {"id": 6, "name": "Saturday by 10.30am",
//         "del_choices": [
//         {"id": 1, "name": "Deliver to neighbour"},
//         {"id": 2, "name": "Deliver to doorstep only"},
//         {"id": 3, "name": "Leave safe at Doorstep"}],
// "del_slots": []},
        
//         {"id": 7, "name": "Saturday 09:00",
//         "del_choices": [
//         {"id": 2, "name": "Deliver to doorstep only"}],
// "del_slots": []}
// ]

    const [selectedTime, setSelectedTime] = useState(0);

    const [modalVisible, setModalVisible] = useState(false);
    const [modalHeight, setModalHeight] = useState(false);
    const [proceedButton, setProceedButton] = useState(false);
    const [messageTimer, setMessgeTimer] = useState(false);
    const [editButton, setEditButton] = useState(false);


    const handleClose = () => {
        setModalVisible(!modalVisible);
        setModalHeight(false);
        setProceedButton(false);
        setEditButton(false);
    }
    const modalBackgroundStyle = {
        backgroundColor: 'rgba(0, 0, 0, 0.6)'
    };

    const handleProceed = () => {
        setModalHeight(true);
        setProceedButton(true);
        setMessgeTimer(true)
        setTimeout(() => { setMessgeTimer(false) }, 5000);
    }

    const handleEdit = () => {
        setEditButton(true);
    }

    const modalHeightStyle = {
        height: 500
    }

    return (
        <>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
            >
                <View style={[styles.modalTransperency, modalBackgroundStyle]}>
                    {/* Transperency Section */}
                    <View style={styles.modalLogoClose}>

                        <TouchableOpacity onPress={handleClose}>
                            <View style={styles.closeIcon}>
                                <Icon type="AntDesign" name="close" style={{ color: "#fff", fontSize: 19 }} />
                            </View>

                        </TouchableOpacity>
                    </View>

                    <View style={[styles.modalSection, modalHeightStyle]}>
                        <ScrollView>
                            <TouchableOpacity style={{ paddingVertical: 10, backgroundColor: "#f3f3f3", alignItems: "center" }} onPress={() => { handleClose(); navigation.navigate('NewAddress'); }}>
                                <Text>+ Add new address</Text>
                            </TouchableOpacity>
                            {addressListData.map((adrs,indx) => {
                                return (
                                    <TouchableOpacity key={indx} style={{ height: "auto", minHeight: 100, paddingVertical: 10 }}>
                                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                                            <Radio selected={adrs.set_default_address} selectedColor={pColorOne} 
                                            onPress={ () => defaultpostAddressApi(adrs.id) }  /> 
                                            {/*<View style={{ paddingHorizontal: 10 }}>
                                                <Text style={{ fontSize: 16, fontWeight: "bold", color: pColorOne }}>Home</Text>
                                            </View>*/}
                                            <View style={{ paddingHorizontal: 10 }}>
                                                <Text style={{ fontSize: 16, fontWeight: "bold", color: pColorOne }}>{adrs.address_type == 'billing_information' ? "Billing Address" : "Shipping Address"}</Text>
                                            </View>
                                        </View>
                                        <View style={{ paddingHorizontal: 27 }}>
                                            <Text>{adrs.address_1}</Text>
                                            <Text>{adrs.address_2}</Text>
                                            <Text>{adrs.city} - {adrs.pincode}</Text>
                                            <Text>{adrs.state_title}</Text>
                                            <Text>Mobile: {adrs.phone_no}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })}
                        </ScrollView>
                    </View>
                </View>
            </Modal>
            <ScrollView style={{ flex: 1, backgroundColor: "#fff" }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", paddingVertical: 15, marginHorizontal: 20, alignItems: "center" }}>
                    <Text style={{ fontSize: 17, fontWeight: "bold" }}>Delivery To</Text>
                    <TouchableOpacity style={{ borderWidth: 0.5, height: 32, width: 90, alignItems: "center", borderColor: pColorOne, borderRadius: 5 }} onPress={() => { setModalVisible(true); }} >
                        <Text style={{ fontSize: 14, paddingTop: 5, color: pColorOne, fontWeight: "100" }}>CHANGE</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ backgroundColor: "#F3F3F3", height: "auto",  paddingVertical: 10 }}>
                    {/* <View style={{ marginHorizontal: 20, flexDirection: "row", alignItems: "center" }}>
                        <Radio selected={true} selectedColor={pColorOne} />
                        <View style={{ paddingHorizontal: 10 }}>
                            <Text style={{ fontSize: 16, fontWeight: "bold", color: pColorOne }}>Home</Text>
                        </View>
                    </View>
                    <View style={{ paddingHorizontal: 50 }}>
                        <Text>Silkboard Busstand, 15th cross, Bengaluru-560065</Text>
                        <Text>+91-9876543210</Text>
                    </View> */}
                {addressListData.map((adrs,indx) => {

                                return (
                                    <TouchableOpacity key={indx}
                                      style={{ height: "auto",
                                       // minHeight: 100,
                                        // paddingVertical: 10,
                                        paddingHorizontal: 30  }}

                                     >
                        {adrs.set_default_address &&
                        <>
                                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                                            <Radio selected={adrs.set_default_address} selectedColor={pColorOne} 
                                            onPress={ () => defaultpostAddressApi(adrs.id) }  /> 
                                            {/*<View style={{ paddingHorizontal: 10 }}>
                                                <Text style={{ fontSize: 16, fontWeight: "bold", color: pColorOne }}>Home</Text>
                                            </View>*/}
                                            <View style={{ paddingHorizontal: 10 }}>
                                                <Text style={{ fontSize: 16, fontWeight: "bold", color: pColorOne }}>{adrs.address_type == 'billing_information' ? "Billing Address" : "Shipping Address"}</Text>
                                            </View>
                                        </View>
                                        <View style={{ paddingHorizontal: 27 }}>
                                            <Text>{adrs.address_1}</Text>
                                            <Text>{adrs.address_2}</Text>
                                            <Text>{adrs.city} - {adrs.pincode}</Text>
                                            <Text>{adrs.state_title}</Text>
                                            <Text>Mobile: {adrs.phone_no}</Text>
                                        </View>
                        </>
                                }
                                    </TouchableOpacity>
                                )
                            })}
                </View>
                <View style={{ backgroundColor: "#F3F3F3", height: "auto", minHeight: 60, marginTop: 10, justifyContent: "center" }}>
                    <View style={{ marginHorizontal: 20, flexDirection: "row", }}>
                        <Icon type="AntDesign" name="edit" style={{ fontSize: 20, paddingTop: 9 }} />
                        <TextInput style={{
                            paddingHorizontal: 10,
                            width: "100%",
                            height: 40,
                            borderColor: '#dfdfdf',
                        }}
                            underlineColorAndroid="transparent"
                            placeholder="Any instruction?"
                            placeholderTextColor="black"
                            autoCapitalize="none"
                        />
                    </View>

                </View>
                <View style={{ paddingVertical: 20, marginHorizontal: 20, }}>
                    {/*<Text style={{ fontSize: 17, fontWeight: "bold" }}>Set Delivery Date & Time</Text>
                                        <View style={{ flexDirection: "row", justifyContent: "space-between", paddingVertical: 10 }}>
                                            {deliveryDate.map((dt,index) => {
                                                return (
                                                    <View key={index} style={{ borderWidth: 0.3, borderColor: "#dfdfdf", height: 80, width: 80, justifyContent: "center", backgroundColor: selectedDate === dt.id ? pColorOne : null }}>
                                                        <TouchableOpacity style={{ paddingHorizontal: 17 }} onPress={() => setSelectedDate(dt.id)}>
                                                            <Text style={{ fontWeight: "bold", fontSize: 16, paddingLeft: 5, color: selectedDate === dt.id ? "#fff" : null }}>{dt.day}</Text>
                                                            <Text style={{ color: selectedDate === dt.id ? "#fff" : null }}>{dt.date}</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                )
                                            })}
                                        </View>*/}
                    <View>
                        <Text style={{borderBottomColor: 'grey',
                      borderBottomWidth: 1,paddingBottom:10,fontSize: 17, fontWeight: "bold"}}>Delivery Option</Text>

                        { delivery_options !== undefined  && delivery_options.map((tm,idx) => {
                            return (

                                <TouchableOpacity key={idx} onPress={() => delivery_option_change(tm.id) } style={{ flexDirection: "row", height: 50,  alignItems: "center" }}>
                                    <Radio onPress={ () => setSelectedTime(tm.id) } selected={tm.id === delivery_option} selectedColor={pColorOne} style={{ paddingHorizontal: 10 }} />
                                    <View style={{ paddingHorizontal: 10, flexDirection: "row" }}>
                                        <Text style={{ color: tm.status === "Unavailable" ? "#A7A7A7" : null }}>{tm.name} </Text>
                                       {/* <Text style={{ color: "red", paddingLeft: 100 }}>{tm.status}</Text>*/}
                                    </View>
                                </TouchableOpacity>
                                )

                                
                        })}
                         { delivery_options !== undefined  && delivery_options.map((tm,idx) => {
                        return (
                            <View >
                            {tm.id === delivery_option && 
                                    <View>
                                    {tm.del_choices.length > 0 &&
                                        <View>
                                        <Text style={{borderBottomColor: 'grey',
                      borderBottomWidth: 1,paddingBottom:10,fontSize: 17, fontWeight: "bold"}}>Delivery Choice</Text>

                                    {tm.del_choices.map((choices,indx)=>{
                                                                            return(
                                                                                <TouchableOpacity key={indx} 
                                                                                onPress={() => delivery_choice_change(choices.id) }
                                                                                 style={{ flexDirection: "row", height: 50, alignItems: "center" }}>
                                                                        <Radio 
                                                                        // onPress={ () => setSelectedTime(tm.id) } 
                                                                        selected={choices.id === delivery_choice} 
                                                                        selectedColor={pColorOne} style={{ paddingHorizontal: 10 }} />
                                                                        <View style={{ paddingHorizontal: 10, flexDirection: "row" }}>
                                                                            <Text>{choices.name} </Text>
                                                                        </View>
                                                                    </TouchableOpacity>
                                    
                                    
                                                                                )
                                    
                                    
                                                                        }) }

                                        </View>
                                    }
                                    {tm.del_slots.length > 0 && 
                                        <View>
                                        <Text style={{borderBottomColor: 'grey',
                      borderBottomWidth: 1,paddingBottom:10,fontSize: 17, fontWeight: "bold"}}>Delivery Slot</Text>

                                    {tm.del_slots.map((slots,indx)=>{
                                                                            return(
                                                                                <TouchableOpacity key={indx} 
                                                                                onPress={() => delivery_slot_change(slots.id) }
                                                                                 style={{ flexDirection: "row", height: 50, backgroundColor: tm.id === selectedTime ? "#F7DCCB" : null, alignItems: "center" }}>
                                                                        <Radio 
                                                                        // onPress={ () => setSelectedTime(tm.id) } 
                                                                        selected={slots.id === delivery_slot} 
                                                                        selectedColor={pColorOne} style={{ paddingHorizontal: 10 }} />
                                                                        <View style={{ paddingHorizontal: 10, flexDirection: "row" }}>
                                                                            <Text>{slots.name} </Text>
                                                                        </View>
                                                                    </TouchableOpacity>
                                    
                                    
                                                                                )
                                    
                                    
                                                                        }) 
                                    }
                                        </View>
                                    }
                                    </View>




                                }
                            </View>
                            
                        
                        )
                    })}

                    </View>
                </View>
            </ScrollView>
        </>
    )
}


const styles = StyleSheet.create({
    rightIcon: {
        marginRight: 15
    },
    rightIconSection: {
        flexDirection: 'row',
        marginTop: 10
    },
    closeIcon: {
        marginVertical: 15,
        marginHorizontal: 30
    },
    logo: {
        marginVertical: 10,
        marginHorizontal: 30
    },
    cartCount: {
        width: 22,
        height: 22,
        borderRadius: 50,
        textAlign: 'center',
        color: '#ffffff',
        backgroundColor: '#499142',
        position: 'absolute',
        top: -12,
        right: 8
    },
    cartCountRed: {
        backgroundColor: '#ff555c'
    },
    cartCountText: {
        textAlign: 'center',
        color: '#ffffff'
    },
    modalTransperency: {
        flex: 1,
        justifyContent: 'center',
        alignItems: "center"
    },
    modalSection: {
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 35,
        fontSize: 16,
        width: '100%',
        height: 300
    },
    modalLogoClose: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    message: {
        backgroundColor: '#f3f3f3',
        borderColor: '#F26D21',
        borderWidth: 1,
        borderStyle: 'solid',
        width: 354,
        height: 53,
        borderRadius: 26,
        marginVertical: 20,
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    messageText: {
        fontSize: 12,
        fontWeight: '500',
        color: '#F26D21'
    }

});

export default withNavigation(CheckoutScreen)
