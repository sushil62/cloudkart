import React, { useContext } from 'react'
import { withNavigation } from 'react-navigation'
import { CKart } from '../../../ContextApi'
import { Text, View, TouchableOpacity, ScrollView, Dimensions, Image, ToastAndroid,ActivityIndicator } from 'react-native';
import { Styles } from '../../../Stylesheet'
import { Icon, Button } from 'native-base'

const Cart = ({ navigation }) => {
  // cart_data.cart_items
  const { loading,loading_true,cart_data, inc_qty, dec_qty, remove_cart_item, fetchProductDtlApi,checkoutGetApi, fetchAllAddressApi, wishlist_list, addProductwishlistApi, handleRemovewishlist, signInToken, bottomModalFunc, currency } = useContext(CKart)
  // console.log(cart_data,'cart_data')

  const { pColorOne } = Styles
  const { width, height } = Dimensions.get('window')
  const sWidth = width / 2

  // var save = []

  //   for (var i = 0; i < cart_data.cart_items.length; i++) {
  //       save.push(cart_data.cart_items[i].save_amount)
  //   }
  //   // const scores = [98,45,33,47,100,80];
  //   const totalsavemoney = save.reduce(
  //   (previousScore, currentScore, index)=>previousScore+currentScore, 
  //   0);

  const handle_cart_list = () => {
    var wishlist_items = wishlist_list.map((w) => {
      return w.vendor_product_id;
    });

    if (cart_data !== undefined) {
      if (cart_data.cart_items !== undefined) {

        return (
          <View>
            {cart_data != undefined && cart_data.cart_items.length > 0 ? cart_data.cart_items.map(lst => {
              return (
                <View style={{ paddingVertical: 10 }} key={lst.id}>
                  <TouchableOpacity onPress={() => { fetchProductDtlApi(lst.vendor_product.id); navigation.navigate('ProductDetailScreen') }} >
                    <View style={{ borderBottomWidth: 0.5, borderBottomColor: "#dcdcdc", minHeight:120 }}>
                      {lst.vendor_product.discount_percent > 0 && <View style={{ backgroundColor: "#EC2C46", width: 65, height: 25, borderTopStartRadius: 5, justifyContent: "center", alignItems: "center" }}>
                        <Text style={{ color: "#fff", fontSize: 15, }}>{lst.vendor_product.discount_percent}% OFF</Text>
                      </View>}
                      <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginVertical: 5 }}>
                        <View style={{ flexDirection: "row", }}>
                          <Image source={{ uri: lst.vendor_product.product.image }} style={{ width: 80, height: 80 ,resizeMode: "center"}} />
                          <View style={{ paddingHorizontal: 10 }}>
                            <View style={{width:140}}>
                              <Text style={{ fontSize: 16 }}>{lst.vendor_product.product.title}</Text>
                            </View>
                            <Text style={{ fontSize: 13, color: "#777" }}>{lst.weight}</Text>
                            <Text style={{ fontSize: 16 , }}>{currency}{' '}{lst.vendor_product.discounted_price_incl_tax}</Text>
                          </View>
                        </View>
                        <View style={{ marginEnd: 10, paddingTop: 40, flexDirection: "row", alignItems: "center", justifyContent: "space-between", }}>
                          <TouchableOpacity onPress={() => {dec_qty(lst.id);}}>
                            <View style={{ backgroundColor: pColorOne, width: 30, height: 30, borderRadius: 2, justifyContent: "center", alignItems: "center" }}>
                              <Text style={{ color: "#fff", fontSize: 15, }}>-</Text>
                            </View>
                          </TouchableOpacity>
                          <Text style={{ paddingHorizontal: 10 }}>{lst.quantity}</Text>
                          <TouchableOpacity onPress={() => {inc_qty(lst.id);}}>
                            <View style={{ backgroundColor: pColorOne, width: 30, height: 30, borderRadius: 2, justifyContent: "center", alignItems: "center" }}>
                              <Text style={{ color: "#fff", fontSize: 15, }}>+</Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                  <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
                    {wishlist_items.indexOf(lst.vendor_product.id) !== -1 ?
                      (<Button style={{ backgroundColor: pColorOne, width: sWidth - 10, height: 40, justifyContent: "center" }} onPress={() => { signInToken === null ? bottomModalFunc("signIn") : [handleRemovewishlist(lst.vendor_product.id)] }}>
                        <Icon type="FontAwesome" name="heart" style={{ fontSize: 15 }} />
                        <Text style={{ color: "#fff", fontSize: 15, }}>Wish List</Text>
                      </Button>)
                      : (<Button style={{ backgroundColor: pColorOne, width: sWidth - 10, height: 40, justifyContent: "center" }} onPress={() => { signInToken === null ? bottomModalFunc("signIn") : [addProductwishlistApi(lst.vendor_product.id)] }}>
                        <Icon type="FontAwesome" name="heart-o" style={{ fontSize: 15 }} />
                        <Text style={{ color: "#fff", fontSize: 15, }}>Wish List</Text>
                      </Button>)}


                    <Button style={{ backgroundColor: "red", width: sWidth - 10, height: 40, justifyContent: "center" }} onPress={() =>{ remove_cart_item(lst.id);}}>
                      <Icon type="MaterialIcons" name="delete" style={{ fontSize: 15 }} />
                      <Text style={{ color: "#fff", fontSize: 15, }}>Remove</Text>
                    </Button>
                  </View>
                </View>
              )
            }) :
              <View style={{ alignItems: "center", paddingTop: 20 }}>
                <Text style={{ fontSize: 17 }}>Your Cart is empty</Text>
                <View style={{ paddingTop: 20 }}>
                  <Button style={{ backgroundColor: pColorOne, width: sWidth, height: 40, justifyContent: "center" }} onPress={() => { navigation.navigate('HomeScreen'); }}>
                    <Text style={{ color: "#fff", fontSize: 18, }}>ADD ITEMS</Text>
                  </Button>
                </View>
              </View>
            }
          </View>

        )
      }

    }
  }


  return (
    <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
      <View style={{ flexDirection: 'row',  marginHorizontal: 20, alignItems: "center", marginBottom: 7 }}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Icon type="AntDesign" name="left" style={{ color: "#fff", fontSize: 20 }} />
        </TouchableOpacity>
        <Text style={{ color: "#fff", fontSize: 18, fontWeight: '700',marginHorizontal:20 }}>Your Cart</Text>
        
      </View>
    
      <View style={{ flex: 1, marginTop: 5, backgroundColor: "white" }}>
             {/* <View style={{ flexDirection: "row", alignItems: "center", paddingVertical: 10, backgroundColor: "#F9F8F8" }}>
                <Icon type="MaterialIcons" name="location-on" style={{ fontSize: 17, color: '#999', paddingLeft: 10, }} />
                <Text style={{ fontSize: 17, color: '#777' }}>Hennur, Bengaluru</Text>
                <TouchableOpacity>
                  <Icon type="Entypo" name="chevron-down" style={{ fontSize: 23, marginTop: 5 }} />
                </TouchableOpacity>
              </View>*/}
        <ScrollView style={{ paddingHorizontal: 10, paddingTop: 5 }}>

          {handle_cart_list()}
        </ScrollView>
        {cart_data != undefined && cart_data.cart_items.length > 0 &&
          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
            <View style={{ marginHorizontal: 10 }}>
              <Text style={{ fontSize: 18 }}>Total  &nbsp;&nbsp;{currency} {cart_data.subtotal}</Text>
              {cart_data.total_save_amount !== undefined && cart_data.total_save_amount > 0 && <Text style={{ fontSize: 15, color: "green" }}>Saved &nbsp;&nbsp;&nbsp;{currency} {cart_data.total_save_amount}</Text>}
            </View>
            <Button style={{ backgroundColor: pColorOne, width: sWidth, height: 55, justifyContent: "center" }} onPress={() => { signInToken === null ? bottomModalFunc("signIn") : [navigation.navigate('Checkout'), fetchAllAddressApi(),checkoutGetApi()] }}>
              <Text style={{ color: "#fff", fontSize: 18, }}>CHECKOUT</Text>
            </Button>
          </View>
        }
      </View>
    </View>
  )
}

export default withNavigation(Cart);
