import React, { useContext, useState } from 'react'
import { View, Text, TouchableOpacity, ScrollView, Dimensions, TextInput } from 'react-native'
import { CKart } from '../../../ContextApi'
import { Icon, Button, Label, CheckBox } from 'native-base'
import { Styles } from '../../../Stylesheet'
import { withNavigation } from 'react-navigation'

const AddNewCard = ({ navigation }) => {
    const { selectModeChange } = useContext(CKart)
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 2.5
    const [addressType, setAddressType] = useState(null);
    const [isDefaultAddress, SetIsDefaultAddress] = useState(false);
    const { buyNow } = useContext(CKart)

    const { pColorOne } = Styles
    return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
            <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center" }}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Icon type="AntDesign" name="left" style={{ color: "#fff", fontSize: 20 }} />
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>
                <ScrollView style={{ flex: 1, backgroundColor: "#fff" }}>
                    <View style={{ marginVertical: 5, borderWidth: 0, height: 40, marginHorizontal: 20 }}>
                        <View style={{ paddingVertical: 10, flexDirection: "row", justifyContent: "space-between" }}>
                            <View style={{ flexDirection: "row" }}>
                                <Text style={{ fontSize: 17, fontWeight: "bold" }}>Amount Payable</Text>
                                <Text style={{ fontSize: 15, paddingTop: 1.5 }}>(incl. of all taxes)</Text>
                            </View>
                            <Text style={{ fontSize: 17, fontWeight: "bold" }}><Icon type="FontAwesome" name="rupee" style={{ fontSize: 15 }} />107.00</Text>

                        </View>
                    </View>

                    <View style={{ marginVertical: 10, marginHorizontal: 20, flex: 1, borderWidth: 0 }}>

                        <View style={{ paddingVertical: 10 }}>
                            <Label>Card Number</Label>
                            <TextInput style={{
                                height: 40,
                                borderColor: '#dfdfdf',
                                borderBottomWidth: 1
                            }}
                                underlineColorAndroid="transparent"
                                placeholder="**** **** **** 1234"
                                placeholderTextColor="#dfdfdf"
                                autoCapitalize="none"
                            />
                        </View>
                        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                            <View style={{ paddingVertical: 10 }}>
                                <Label>Expire Date</Label>
                                <TextInput style={{
                                    height: 40,
                                    width: sWidth,
                                    borderColor: '#dfdfdf',
                                    borderBottomWidth: 1
                                }}
                                    underlineColorAndroid="transparent"
                                    placeholder="MM/YYYY"
                                    placeholderTextColor="#dfdfdf"
                                    autoCapitalize="none"
                                />
                            </View>
                            <View style={{ paddingVertical: 10 }}>
                                <Label>CVV <Icon name="questioncircle" type="AntDesign" style={{ fontSize: 17 }} /></Label>
                                <TextInput style={{
                                    height: 40,
                                    width: sWidth,
                                    borderColor: '#dfdfdf',
                                    borderBottomWidth: 1
                                }}
                                    underlineColorAndroid="transparent"
                                    placeholder="E.g. 123"
                                    placeholderTextColor="#dfdfdf"
                                    autoCapitalize="none"
                                />
                            </View>
                        </View>
                        <View style={{ paddingVertical: 10 }}>
                            <Label>Name on Card</Label>
                            <TextInput style={{
                                height: 40,
                                borderColor: '#dfdfdf',
                                borderBottomWidth: 1
                            }}
                                underlineColorAndroid="transparent"
                                placeholder="E.g. John Deo"
                                placeholderTextColor="#dfdfdf"
                                autoCapitalize="none"
                            />
                        </View>

                    </View>
                    <TouchableOpacity onPress={() => SetIsDefaultAddress(!isDefaultAddress)} style={{ flexDirection: "row", paddingHorizontal: 5, paddingBottom: 20 }}>
                        <CheckBox style={{ borderColor: pColorOne, backgroundColor: isDefaultAddress ? pColorOne : "#fff" }} value={isDefaultAddress}
                            checked={isDefaultAddress} />
                        <View style={{ paddingHorizontal: 20, }}>
                            <Text style={{ fontSize: 16 }}>Save card for future transaction</Text>
                            <Text style={{ fontSize: 14, color: "#626262" }}>It's perfectly secure</Text>
                        </View>
                    </TouchableOpacity>
                    <Button disabled={false} style={{ backgroundColor: true ? pColorOne : "#FC9663", justifyContent: "center", height: 55, marginHorizontal: 20 }} onPress={() => { navigation.navigate('OrderSuccess'); buyNow(); }}>
                        <Text style={{ color: "#fff", fontSize: 16 }}>PLACE ORDER & PAY</Text>
                    </Button>

                </ScrollView>
            </View>
        </View>
    )
}

export default withNavigation(AddNewCard)