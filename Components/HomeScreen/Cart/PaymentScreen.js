import React, { useContext, useState, useRef } from 'react'
import { View, Text, TouchableOpacity, ScrollView, Dimensions, TextInput, Modal, StyleSheet, Image, ActivityIndicator } from 'react-native'
import { CKart } from '../../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Button, Item, Label, Input, CheckBox, Radio } from 'native-base'
import { Styles } from '../../../Stylesheet'
import { withNavigation } from 'react-navigation'
import { WebView } from 'react-native-webview';


const PaymentScreen = ({ navigation }) => {
    const { selectModeChange,currency,cart_data,paypalbtn_data } = useContext(CKart)
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 3
    // console.log(cart_data.cart_items)

    const { pColorOne } = Styles

    const deliveryOptionList = [
        { id: 1, name: "Standard Delivery", },
        { id: 2, name: "Click and Collect", },
        { id: 3, name: "Next Day", },
        { id: 4, name: "Next Day 12.00", },
        { id: 5, name: "Next Day 10.30", },
        { id: 6, name: "Next Day 09.00", },
        { id: 7, name: "Saturday", },
        { id: 8, name: "Saturday by 10.30am", },
        { id: 9, name: "Saturday by 09.00", },
    ]

    const deliveryTypeList = [
        { id: 1, name: "Type One", },
        { id: 2, name: "Type Two", },
    ]

    const deliverySlotList = [
        { id: 1, name: "Slot One", },
        { id: 2, name: "Slot Two", },
    ]

    const [deliveryOption, setDeliveryOption] = useState(null)
    const [deliveryType, setDeliveryType] = useState(null)
    const [deliverySlot, setDeliverySlot] = useState(null)
    const [selectedMethod, setMethod] = useState(null);
    const [buttonName, setbuttonName] = useState('PAY');

    const [modalVisible, setModalVisible] = useState(false);
    const [modalHeight, setModalHeight] = useState(false);
    const [proceedButton, setProceedButton] = useState(false);
    const [messageTimer, setMessgeTimer] = useState(false);
    const [editButton, setEditButton] = useState(false);
    const [showModal, setShowModal] = useState(false);


    const handleClose = () => {
        setModalVisible(!modalVisible);
        setModalHeight(false);
        setProceedButton(false);
        setEditButton(false);
    }
    const modalBackgroundStyle = {
        backgroundColor: 'rgba(0, 0, 0, 0.6)'
    };

    const handleProceed = () => {
        setModalHeight(true);
        setProceedButton(true);
        setMessgeTimer(true)
        setTimeout(() => { setMessgeTimer(false) }, 5000);
    }

    const handleEdit = () => {
        setEditButton(true);
    }

    const modalHeightStyle = {
        height: 500
    }
    const webviewRef = useRef(null)
    const backButtonHandler = () => {
        if (webviewRef.current) webviewRef.current.goBack()
    }
    const frontButtonHandler = () => {
        if (webviewRef.current) webviewRef.current.goForward()
    }

    const [canGoBack, setCanGoBack] = useState(false)
    const [canGoForward, setCanGoForward] = useState(false)
    const [currentUrl, setCurrentUrl] = useState('')

    return (
        <>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
            >
                <View style={[styles.modalTransperency, modalBackgroundStyle]}>
                    {/* Transperency Section */}
                    <View style={styles.modalLogoClose}>

                        <TouchableOpacity onPress={handleClose}>
                            <View style={styles.closeIcon}>
                                <Icon type="AntDesign" name="close" style={{ color: "#fff", fontSize: 19 }} />
                            </View>

                        </TouchableOpacity>
                    </View>
                    <View style={[styles.modalSection, modalHeightStyle]}>
                        <ScrollView>
                        {cart_data!== undefined && cart_data.cart_items.map((item,indx) => {
                                    return (
                                    <View key={indx} style={{flex: 1,backgroundColor: '#ffffff',borderColor: '#cfcfcf',borderWidth: 1,borderStyle: 'solid',marginBottom:10}}>
                                        {item.vendor_product.discount_percent > 0 && <View style={styles.offerBox}>
                                             <Text style={styles.offerText}>{item.vendor_product.discount_percent}%{' '}OFF</Text>
                                        </View>}
                                        <View style={styles.productBox}>
                                            <View style={styles.productImage}>
                                              <Image source={{ uri: item.vendor_product.product.image }} style={{ width: 80, height: 80 ,resizeMode: "center"}} />
                                            </View>
                                            <View style={styles.productText}>
                                                <Text style={styles.productName}>{item.vendor_product.product.title}</Text>
                                                <View style={styles.weightSection}>
                                                    {<Text style={styles.quantityMultiple}>x{item.quantity}</Text>}
                                                </View>
                                                <View style={styles.priceSection}>
                                                {item.vendor_product.discount_percent > 0 ?<Text style={{ fontSize: 16, fontWeight: "bold" }}>{currency} {item.vendor_product.discounted_price_incl_tax}</Text>
                                                :<Text style={{ fontSize: 16, fontWeight: "bold" }}>{currency} {item.vendor_product.price_excl_tax}</Text>}
                                                   
                                                </View>
                                            </View>
                                            </View>
                                    </View>
                                    )
                                })}

                        </ScrollView>
                    </View>
                </View>
            </Modal>
            <ScrollView style={{ flex: 1, backgroundColor: "#fff" }}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", paddingVertical: 15, marginHorizontal: 20, alignItems: "center" }}>
                    <Text style={{ fontSize: 17, fontWeight: "bold" }}>Order Summary</Text>
                    <TouchableOpacity style={{ borderWidth: 0.5, height: 32, width: 90, alignItems: "center", borderColor: pColorOne, borderRadius: 5, justifyContent: "center" }} onPress={() => { setModalVisible(true); }} >
                        <Text style={{ fontSize: 12, color: pColorOne, fontWeight: "100" }}>VIEW ITEMS</Text>
                    </TouchableOpacity>
                </View>
                {/*<View style={{ backgroundColor: "#F3F3F3", height: "auto", minHeight: 100, paddingVertical: 10, }}>
                    <Text style={{ fontSize: 17, fontWeight: "bold", paddingHorizontal: 20, }}>Delivery Option</Text>
                    <View style={{ paddingVertical: 10 }}>
                        {deliveryOptionList.map((d,idx) => {
                            return (
                                <TouchableOpacity key={idx} onPress={() => setDeliveryOption(d.id)} style={{ flexDirection: "row", height: 30, alignItems: "center", paddingHorizontal: 20 }}>
                                    <Radio color={"black"} onPress={() => setDeliveryOption(d.id)} selected={d.id === deliveryOption} selectedColor={pColorOne} style={{ paddingHorizontal: 10 }} />
                                    <View style={{ paddingHorizontal: 10, }}>
                                        <Text >{d.name}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                </View>
                <View style={{ backgroundColor: "#F3F3F3", height: "auto", minHeight: 100, paddingVertical: 10, marginTop: 10, }}>
                    <Text style={{ fontSize: 17, fontWeight: "bold", paddingHorizontal: 20, }}>Delivery Type</Text>
                    <View style={{ paddingVertical: 10 }}>
                        {deliveryTypeList.map((d,indx) => {
                            return (
                                <TouchableOpacity key={indx} onPress={() => setDeliveryType(d.id)} style={{ flexDirection: "row", height: 30, alignItems: "center", paddingHorizontal: 20 }}>
                                    <Radio color={"black"} onPress={() => setDeliveryType(d.id)} selected={d.id === deliveryType} selectedColor={pColorOne} style={{ paddingHorizontal: 10 }} />
                                    <View style={{ paddingHorizontal: 10, }}>
                                        <Text >{d.name}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                </View>
                <View style={{ backgroundColor: "#F3F3F3", height: "auto", minHeight: 100, paddingVertical: 10, marginTop: 10, }}>
                    <Text style={{ fontSize: 17, fontWeight: "bold", paddingHorizontal: 20, }}>Delivery Type</Text>
                    <View style={{ paddingVertical: 10 }}>
                        {deliverySlotList.map((d,index) => {
                            return (
                                <TouchableOpacity key={index} onPress={() => setDeliverySlot(d.id)} style={{ flexDirection: "row", height: 30, alignItems: "center", paddingHorizontal: 20 }}>
                                    <Radio color={"black"} onPress={() => setDeliverySlot(d.id)} selected={d.id === deliverySlot} selectedColor={pColorOne} style={{ paddingHorizontal: 10 }} />
                                    <View style={{ paddingHorizontal: 10, }}>
                                        <Text >{d.name}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                </View>*/}
                <View style={{ backgroundColor: "#F3F3F3", height: "auto", minHeight: 60, marginTop: 10, justifyContent: "center", }}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <View style={{ marginHorizontal: 20, flexDirection: "row", marginVertical: 5 }}>
                            <Icon type="MaterialCommunityIcons" name="brightness-percent" style={{ fontSize: 20, paddingTop: 9, color: pColorOne }} />
                            <TextInput style={{
                                paddingHorizontal: 10,

                                height: 40,
                                borderColor: '#dfdfdf',
                                fontStyle: "italic"
                            }}
                                underlineColorAndroid="transparent"
                                placeholder="Have a promo code?"
                                placeholderTextColor={pColorOne}
                                autoCapitalize="none"
                            />
                        </View>
                        <TouchableOpacity style={{ borderWidth: 0.5, marginRight: 20, marginVertical: 10, borderRadius: 5, borderColor: pColorOne }}>

                            <Text style={{ marginHorizontal: 10, marginVertical: 5, color: pColorOne, fontSize: 15 }}>APPLY</Text>
                        </TouchableOpacity>
                    </View>

                </View>
                <View style={{ backgroundColor: "#F3F3F3", height: "auto", minHeight: 100, paddingVertical: 10, marginTop: 10, }}>
                    <View style={{ marginHorizontal: 20, flexDirection: "row", paddingVertical: 2, alignItems: "center", justifyContent: "space-between" }}>
                        <Text style={{ fontSize: 16, fontWeight: "bold" }}>Total Amount</Text>
                        <Text style={{ fontSize: 16, fontWeight: "bold" }}>{currency} {cart_data.subtotal}</Text>
                    </View>
                    {cart_data.shipping_charges > 0 && <View style={{ marginHorizontal: 20, flexDirection: "row", paddingVertical: 2, alignItems: "center", justifyContent: "space-between" }}>
                        <Text style={{ fontSize: 16, }}>Delivery Charge</Text>
                        <Text style={{ fontSize: 16, }}>+ {cart_data.shipping_charges}</Text>
                    </View>}
                    <View style={{ marginHorizontal: 20, flexDirection: "row", paddingVertical: 2, alignItems: "center", justifyContent: "space-between" }}>
                        <View style={{ flexDirection: "row" }}>
                            <Text style={{ fontSize: 16, fontWeight: "bold" }}>Amount Payable{' '}</Text>
                            <Text style={{ fontSize: 16, }}>(incl. of all taxes)</Text>
                        </View>
                        <Text style={{ fontSize: 16, fontWeight: "bold" }}>{currency} {cart_data.cart_total}</Text>
                    </View>
                </View>

                <Modal
                    visible={showModal}
                >
                    <View style={{ flexDirection: "row", justifyContent: "space-between", paddingVertical: 20, paddingHorizontal: 20 }}>
                        <TouchableOpacity onPress={() => setShowModal(false)}>
                            <Icon type="AntDesign" name="close" style={{ fontSize: 20 }} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={backButtonHandler}>
                            <Icon type="AntDesign" name="left" style={{ fontSize: 20 }} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={frontButtonHandler}>
                            <Icon type="AntDesign" name="right" style={{ fontSize: 20 }} />
                        </TouchableOpacity>
                    </View>
                    <WebView
                        source={{ uri: 'https://www.paypal.com/uk/signin' }}
                        // html={ paypalbtn_data }
                        startInLoadingState={true}
                        renderLoading={() => (
                            <ActivityIndicator
                                color='black'
                                size='large'
                                style={styles.flexContainer}
                            />
                        )}
                        ref={webviewRef}
                        onNavigationStateChange={navState => {
                            setCanGoBack(navState.canGoBack)
                            setCanGoForward(navState.canGoForward)
                            setCurrentUrl(navState.url)
                        }}
                    />
                </Modal>
                <View style={{ paddingVertical: 20, }}>
                    <Text style={{ fontSize: 17, fontWeight: "bold", paddingHorizontal: 20 }}>Select payment method</Text>

                    <Button style={{ marginVertical: 10, backgroundColor: "#ffc439", justifyContent: "center", height: 50, marginHorizontal: 20 }} onPress={() => setShowModal(true)}>
                        {/* <Text style={{ color: "#fff", fontSize: 16 }}>PAYPAL</Text> */}
                        <Image style={{ width: 60, height: 15, }} source={require('../../../assets/paypal-logo-png.png')} />
                    </Button>
                    <Button style={{ marginTop: 5, backgroundColor: "#2C2E2F", justifyContent: "center", height: 50, marginHorizontal: 20 }} onPress={() => navigation.navigate('AddNewCard')}>
                        <Text style={{ color: "#fff", fontSize: 16 }}>
                            <Icon type="FontAwesome" name="credit-card" style={{ fontSize: 20, color: "#fff" }} />
                            {`  `}Debit or Credit Card</Text>
                    </Button>
                    <View style={{ alignItems: "center", }}>
                        <Text style={{ fontStyle: "italic", color: "#b4b4b4" }}>Powered by
                            <Image style={{ width: 50, height: 13, marginTop: 5 }} source={require('../../../assets/paypal-logo-png.png')} />
                        </Text>
                        <Text style={{ fontWeight: "bold", paddingVertical: 15, fontSize: 16 }}>OR</Text>
                    </View>
                    <Button style={{ backgroundColor: true ? pColorOne : "#FC9663", justifyContent: "center", height: 50, marginHorizontal: 20 }} onPress={() => navigation.navigate('HomeScreen')}>
                        <Text style={{ color: "#fff", fontSize: 16 }}>CONTINUE SHOPPING</Text>
                    </Button>
                </View>
            </ScrollView>
        </>
    )
}


const styles = StyleSheet.create({
    rightIcon: {
        marginRight: 15
    },
    rightIconSection: {
        flexDirection: 'row',
        marginTop: 10
    },
    offerBox: {
        width: 58,
        height: 22,
        backgroundColor: '#ff555c',
        marginHorizontal: 10,
        marginTop: 10
    },
    offerText: {
        color: '#ffffff',
        fontSize: 10,
        textAlign: 'center',
        paddingVertical: 3
    },
    productBox: {
        flexDirection: 'row'
    },
    productImage: {
        width: 110,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    productText: {
        flex: 1,
        color: '#313131'
    },
    productName: {
        fontWeight: '500',
        color: '#313131',
        paddingVertical: 8
    },
    productWeight: {
        fontSize: 12,
        fontWeight: '300',
        color: '#313131'
    },
    priceSection: {
        flexDirection: 'row',
        paddingVertical: 8
    },
    weightSection: {
        flexDirection: 'row',
    },
    
    closeIcon: {
        marginVertical: 15,
        marginHorizontal: 30
    },
    logo: {
        marginVertical: 10,
        marginHorizontal: 30
    },
    cartCount: {
        width: 22,
        height: 22,
        borderRadius: 50,
        textAlign: 'center',
        color: '#ffffff',
        backgroundColor: '#499142',
        position: 'absolute',
        top: -12,
        right: 8
    },
    cartCountRed: {
        backgroundColor: '#ff555c'
    },
    cartCountText: {
        textAlign: 'center',
        color: '#ffffff'
    },
    modalTransperency: {
        flex: 1,
        justifyContent: 'center',
        alignItems: "center"
    },
    modalSection: {
        backgroundColor: "white",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        padding: 35,
        fontSize: 16,
        width: '100%',
        height: 300
    },
    modalLogoClose: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    message: {
        backgroundColor: '#f3f3f3',
        borderColor: '#499142',
        borderWidth: 1,
        borderStyle: 'solid',
        width: 354,
        height: 53,
        borderRadius: 26,
        marginVertical: 20,
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    messageText: {
        fontSize: 12,
        fontWeight: '500',
        color: '#499142'
    },
    flexContainer: {
        alignItems: "center",
        marginBottom:300
    },


});
export default withNavigation(PaymentScreen)
