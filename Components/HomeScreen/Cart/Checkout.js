import React, { useContext } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { CKart } from '../../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Button } from 'native-base'
import { Styles } from '../../../Stylesheet'
import { withNavigation } from 'react-navigation'
import CheckoutScreen from './CheckoutScreen';

const Checkout = ({ navigation }) => {
    const { selectModeChange,addPaypalButtonApi } = useContext(CKart)

    const { pColorOne } = Styles
    return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
            <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center" }}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Icon type="AntDesign" name="left" style={{ color: "#fff", fontSize: 20 }} />
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>
                <CheckoutScreen />
                <Button style={{ backgroundColor: true ? pColorOne : "#FC9663", justifyContent: "center",height:55 }} onPress={() => {navigation.navigate('Payment');addPaypalButtonApi()}}>
                    <Text style={{ color: "#fff", fontSize: 16 }}>Proceed To Pay</Text>
                </Button>
            </View>
        </View>
    )
}

export default withNavigation(Checkout)