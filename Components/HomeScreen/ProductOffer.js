import React,{ useContext } from 'react'
import { View, Text,ScrollView,TouchableOpacity,Image,Dimensions } from 'react-native'
import { FontAwesome } from '@expo/vector-icons';


import { CKart } from '../../ContextApi'

const ProductOffer = () => {
	const { product_offers_rec,t_filter,base_url} = useContext(CKart)
	const screenWidth = Math.round(Dimensions.get('window').width);

    const h = Dimensions.get('window').height;
return (
           <ScrollView
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        style={{height: 180, width: screenWidth}}>
        { product_offers_rec.map((p) => (
              <View>
	                <TouchableOpacity style={{ marginRight: 12, height: 190 }}>
                        <Image source= {{uri: base_url + p.url}} style={{height: screenWidth / 2,width: screenWidth,}}/>
                    </TouchableOpacity>
               
              </View>
            ))}          
            </ScrollView>
        
       
)}
export default ProductOffer