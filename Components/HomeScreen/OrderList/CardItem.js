// CardItem.js

import React, { useContext } from 'react'

// import { withNavigation } from 'react-navigation'
import { CKart } from '../../../ContextApi'
import { FontAwesome } from '@expo/vector-icons';

import { Text, View, TouchableOpacity, ScrollView, Dimensions, Image,StyleSheet,TouchableHighlight } from 'react-native';
import { Styles } from '../../../Stylesheet'
import { Icon, Button } from 'native-base'

const CardItem = ({productDetails,fromViewItems,quantityCount}) => {

	const { handleFilterChange,orderDetails,viewItems,view_order} = useContext(CKart)
	return (
		 <View style={styles.container}>
                <View style={styles.offerBox}>
                    <Text style={styles.offerText}>{productDetails.offer}</Text>
                </View>
                <View style={styles.productBox}>
                    <View style={styles.productImage}>
                        <Image source={productDetails.productImg} />
                    </View>
                    <View style={styles.productText}>
                        <Text style={styles.productName}>{productDetails.productName}</Text>
                        <View style={styles.weightSection}>
                            <Text style={styles.productWeight}>{productDetails.weight}</Text>
                            {fromViewItems &&<Text style={styles.quantityMultiple}>x{productDetails.quantity}</Text>}
                        </View>
                        <View style={styles.priceSection}>
                            <Text style={styles.offerPrice}>{productDetails.offerPrice}</Text>
                            <Text style={styles.actualPrice}>{productDetails.actualPrice}</Text>
                        </View>
                    </View>
                    {/*{!fromViewItems &&
                                        <View style={styles.numberOfQuanityButton}>
                                            <TouchableHighlight underlayColor="white" onPress={quantityCount > 1 ? () => this.handleQuantity('minus') : () => alert('Not Allowed')}>
                                                <View style={styles.plusButton}>
                                                    <Text style={styles.plusButtonText}>-</Text>
                                                </View>
                                            </TouchableHighlight>
                                            <Text style={styles.numberOfQuanityButtonText}>{quantityCount}</Text>
                                            <TouchableHighlight underlayColor="white" onPress={() => this.handleQuantity('plus')}>
                                                <View style={styles.minusButton}>
                                                    <Text style={styles.minusButtonText}>+</Text>
                                                </View>
                                            </TouchableHighlight>
                                        </View>}
                                    </View>
                                    {!fromViewItems &&
                                    <View style={styles.bottomSection}>
                                        <TouchableHighlight underlayColor="white" onPress={() => alert('Whishlist')}>
                                            <View style={styles.wishlistButton}>
                                                <Image
                                                    style={styles.heartIcon}
                                                    source={require('../../assets/icons/whiteHeart.png')}
                                                />
                                                <Text style={styles.wishlistButtonText}>WISHLIST</Text>
                                            </View>
                                        </TouchableHighlight>
                                        <TouchableHighlight underlayColor="white"onPress={() => alert('delete')}>
                                            <View style={styles.addToCartButton}>
                                            <Image
                                                    style={styles.deleteIcon}
                                                    source={require('../../assets/icons/deleteIcon.png')}
                                                />
                                                <Text style={styles.addToCartButtonText}>REMOVE ITEM</Text>
                                            </View>
                                        </TouchableHighlight>
                                    </View>}*/}
            </View>
            </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        borderColor: '#cfcfcf',
        borderWidth: 1,
        borderStyle: 'solid'
    },
    offerBox: {
        width: 58,
        height: 22,
        backgroundColor: '#ff555c',
        marginHorizontal: 10,
        marginTop: 10
    },
    offerText: {
        color: '#ffffff',
        fontSize: 10,
        textAlign: 'center',
        paddingVertical: 3
    },
    productBox: {
        flexDirection: 'row'
    },
    productImage: {
        width: 110,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    productText: {
        flex: 1,
        color: '#313131'
    },
    productName: {
        fontWeight: '500',
        color: '#313131',
        paddingVertical: 8
    },
    productWeight: {
        fontSize: 12,
        fontWeight: '300',
        color: '#313131'
    },
    priceSection: {
        flexDirection: 'row',
        paddingVertical: 8
    },
    weightSection: {
        flexDirection: 'row'
    },
    offerPrice: {
        fontWeight: '700',
        color: '#313131'
    },
    actualPrice: {
        fontWeight: '700',
        color: '#b4b4b4',
        textDecorationLine: 'line-through',
        marginLeft: 15
    },
    numberOfQuanityButton: {
        width: 150,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    numberOfQuanityButtonText: {
        textAlign: 'center',
        fontSize: 18,
        paddingVertical: 20,
        paddingHorizontal: 15,
        color: '#313131'
    },
    plusButton: {
        width: 31,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#ffffff',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#499142'
    },
    plusButtonText: {
        textAlign: 'center',
        fontSize: 18,
        color: '#ffffff'
    },
    minusButton: {
        width: 31,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#ffffff',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 5,
        backgroundColor: '#499142'
    },
    minusButtonText: {
        textAlign: 'center',
        fontSize: 18,
        color: '#ffffff'
    },
    bottomSection: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    wishlistButton: {
        width: 205,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#494d5a'
    },
    wishlistButtonText: {
        textAlign: 'center',
        fontSize: 17,
        paddingVertical: 20,
        color: '#fff'
    },
    addToCartButton: {
        width: 205,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#ff555c'
    },
    addToCartButtonText: {
        textAlign: 'center',
        fontSize: 17,
        paddingVertical: 20,
        color: '#ffffff'
    },
    heartIcon: {
        marginHorizontal: 10
    },
    deleteIcon: {
        marginHorizontal: 10
    },
    quantityMultiple: {
        fontSize: 12,
        paddingLeft: 40
    }
});

export default CardItem;

