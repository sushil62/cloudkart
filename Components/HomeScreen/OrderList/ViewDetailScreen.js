// ViewDetailScreen
import React, { useContext } from 'react'

import { withNavigation } from 'react-navigation'
import { CKart } from '../../../ContextApi'
import { FontAwesome } from '@expo/vector-icons';

import { Text, View, TouchableOpacity, ScrollView, Dimensions, Image,StyleSheet,TouchableHighlight } from 'react-native';
import { Styles } from '../../../Stylesheet'
import { Icon, Button } from 'native-base'
// import { CardItem } from './CardItem'

const ViewDetailScreen = ({ navigation }) => {
    // cart_data.cart_items
    // onPress={() => this.handleFilterChange('Order Details')}
// onPress={() => this.handleFilterChange('View Items')}
    // onPress={() => removeWishlistItemApi(p.id)}
    const { handleFilterChange,orderDetails,viewItems,view_order} = useContext(CKart)
    // console.log(view_order,'view_order')
    const { pColorOne } = Styles
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 2

     return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
        <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center" }}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Icon type="AntDesign" name="close" style={{ color: "#fff", fontSize: 20 }} />
                </TouchableOpacity>
                <Text style={{ color: "#fff", fontSize: 20, fontWeight: 'bold',paddingLeft:10 }}>ViewDetails</Text>
                
            </View>
            </View>
            <View style={{ flex: 1, marginTop: 5, backgroundColor: "white"}}>
             <View style={styles.container}>
                <View style={styles.buttonsSection}>
                    <TouchableHighlight  onPress={() => handleFilterChange('Order Details')} underlayColor="white" >
                        <View style={[styles.button,  orderDetails &&styles.buttonActive]}>
                            <Text style={[styles.buttonText,  orderDetails &&styles.buttonTextActive]}>ORDER SUMMARY</Text>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight onPress={() => handleFilterChange('View Items')} underlayColor="white" >
                        <View style={[styles.button,( viewItems !==undefined ? viewItems:null) && styles.buttonActive]}>
                            <Text style={[styles.buttonText, (viewItems !==undefined? viewItems:null) && styles.buttonTextActive]}>VIEW ITEMS</Text>
                        </View>
                    </TouchableHighlight>
                </View>
                {orderDetails && view_order!== undefined ? (
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.statusSection}>
                            <View style={styles.status}>
                                <Text>Status</Text>
                                <Text style={styles.orangeText}>{view_order.status_title}</Text>
                            </View>
                            <View style={styles.placedOn}>
                                <Text>Placed On</Text>
                                <Text style={styles.orangeText}>{view_order.order_date}</Text>
                            </View>
                        </View>
                        <View style={styles.deliveryDateAndTimeSection}>
                            <Text style={styles.boldText}>Delivery Date & Time</Text>
                            <Text>Standard delivery</Text>
                            <Text style={styles.orangeText}>Sunday 09, August |  5 PM - 8 PM</Text>
                        </View>
                        <View style={styles.deliveryAddressSection}>
                            <Text style={styles.boldText}>Delivery Address</Text>
                            <Text>Name</Text>
                            <Text>{view_order.shipping_name}</Text>
                            <View style={styles.addressSection}>
                                <Text style={styles.addressHeading}>Home</Text>
                                <Text style={styles.address}>{view_order.shipping_address_1},{view_order.shipping_address_2}{view_order.shipping_address_2?",":null} {view_order.shipping_city_name},</Text>
                                <Text style={styles.address}>{view_order.shipping_state_name}{view_order.shipping_state_name?",":null}{view_order.shipping_country},{view_order.shipping_zip}</Text>
                            </View>
                        </View>
                        <View style={styles.invoicesDetailsSection}>
                            <Text style={styles.boldText}>Invoice Details</Text>
                            <View style={styles.invoicesDetails}>
                                <Text>Invoice Number</Text>
                                <Text>WO123-3049</Text>
                            </View>
                            <View style={styles.invoicesDetails}>
                                <Text>Order ID</Text>
                                <Text>#{view_order.id}</Text>
                            </View>
                            <View style={styles.invoicesDetails}>
                                <Text>Payment By</Text>
                                <Text>{view_order.payment_method}</Text>
                            </View>
                            <View style={styles.invoicesDetails}>
                                <Text>Total Item</Text>
                                <Text>{view_order.order_items.length}</Text>
                            </View>
                            <View style={styles.invoicesDetails}>
                                <Text>Shipping Charge</Text>
                                <Text>₹ {view_order.shipping_charges}</Text>
                            </View>
                            {view_order.cash_on_delivery_charges > 0 &&

	                            <View style={styles.invoicesDetails}>
	                                <Text>Cash On Delivery Charge</Text>
	                                <Text>₹ {view_order.cash_on_delivery_charges}</Text>
	                            </View>
                            }
                            {/*<View style={styles.invoicesDetails}>
                                                            <Text>Amount</Text>
                                                            <Text>₹ {view_order.price_excl_tax}</Text>
                                                        </View>
                                                        <View style={styles.invoicesDetails}>
                                                            <Text>Tax Amount</Text>
                                                            <Text>₹ {view_order.tax_amount}</Text>
                                                        </View>*/}
                            <View style={styles.invoicesDetails}>
                                <Text style={styles.boldText}>Total Amount</Text>
                                <Text style={styles.boldText}>₹ {view_order !== undefined ? view_order.order_total: null}</Text>
                            </View>
                        </View>
                        <View>
                            <TouchableHighlight underlayColor="white" onPress={() => navigation.navigate('MyCart')}>
                                <View style={styles.viewItemsButton}>
                                    <Text style={styles.viewItemsButtonText}>CANCEL ORDER</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </ScrollView>
                ) : (
                        <ScrollView>
                            <Text style={styles.boldItemsText}>{view_order!== undefined && view_order.order_items.length} Items  | ₹ {view_order !== undefined ? view_order.order_total: null}</Text>
                            <View>
                                {view_order!== undefined && view_order.order_items.map(item => {
                                    return (
                                    <View style={{flex: 1,backgroundColor: '#ffffff',borderColor: '#cfcfcf',borderWidth: 1,borderStyle: 'solid'}}>
                                        <View style={styles.offerBox}>
						                    <Text style={styles.offerText}>{item.discount_percent}%{' '}OFF</Text>
						                </View>
						                <View style={styles.productBox}>
						                    <View style={styles.productImage}>
						                        <Image source={item.productImg} />
						                    </View>
						                    <View style={styles.productText}>
						                        <Text style={styles.productName}>{item.title}</Text>
						                        <View style={styles.weightSection}>
						                            <Text style={styles.productWeight}>{item.weight_in_grams}{' '}gms</Text>
						                            {<Text style={styles.quantityMultiple}>x{item.quantity}</Text>}
						                        </View>
						                        <View style={styles.priceSection}>
						                            <Text style={styles.offerPrice}>{item.discounted_price}</Text>
						                            <Text style={styles.actualPrice}>{item.price_excl_tax}</Text>
						                        </View>
						                    </View>
						                    </View>
                                    </View>
                                    )
                                })}
                            </View>
                        </ScrollView>
                    )}
                </View>

            </View>

        </View>
    )
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        paddingHorizontal: 20
    },
    offerBox: {
        width: 58,
        height: 22,
        backgroundColor: '#ff555c',
        marginHorizontal: 10,
        marginTop: 10
    },
    offerText: {
        color: '#ffffff',
        fontSize: 10,
        textAlign: 'center',
        paddingVertical: 3
    },
    productBox: {
        flexDirection: 'row'
    },
    productImage: {
        width: 110,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    productText: {
        flex: 1,
        color: '#313131'
    },
    productName: {
        fontWeight: '500',
        color: '#313131',
        paddingVertical: 8
    },
    productWeight: {
        fontSize: 12,
        fontWeight: '300',
        color: '#313131'
    },
    priceSection: {
        flexDirection: 'row',
        paddingVertical: 8
    },
    weightSection: {
        flexDirection: 'row',
    },
    offerPrice: {
        fontWeight: '700',
        color: '#313131'
    },
    actualPrice: {
        fontWeight: '700',
        color: '#b4b4b4',
        textDecorationLine: 'line-through',
        marginLeft: 15
    },
    buttonsSection: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        borderBottomWidth: 1,
        borderBottomColor: '#000',
    },
    button: {
        width: 175,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        textAlign: 'center',
        fontSize: 16,
        color: '#000000'
    },
    buttonActive: {
        borderBottomWidth: 5,
        borderBottomColor: '#F26D21',
    },
    buttonTextActive: {
        fontWeight: '700'
    },
    statusSection: {
        width: '100%',
        flexDirection: 'row',
        paddingVertical: 30,
        borderBottomColor: '#bfbfbf',
        borderBottomWidth: 1,
        borderStyle: 'dashed'
    },
    status: {
        width: '50%'
    },
    placedOn: {
        width: '50%'
    },
    orangeText: {
        color: '#F26D21'
    },
    deliveryDateAndTimeSection: {
        paddingVertical: 30,
        borderBottomColor: '#bfbfbf',
        borderBottomWidth: 1,
        borderStyle: 'dashed'
    },
    boldText: {
        fontSize: 16,
        fontWeight: '700',
        color: '#494d5a',
        paddingBottom: 10
    },
    deliveryAddressSection: {
        paddingVertical: 30,
        borderBottomColor: '#bfbfbf',
        borderBottomWidth: 1,
        borderStyle: 'dashed'
    },
    addressSection: {
        paddingTop: 15
    },
    addressHeading: {
        fontWeight: '500',
        color: '#F26D21'
    },
    address: {
        fontSize: 12,
        fontWeight: '300',
        color: '#7a7d88',
        paddingRight: 100
    },
    invoicesDetailsSection: {
        paddingTop: 30,
        paddingBottom: 20
    },
    invoicesDetails: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 4
    },
    viewItemsButton: {
        width: 124,
        height: 39,
        borderColor: '#ff555c',
        borderWidth: 1,
        borderStyle: 'solid',
        borderRadius: 3,
        marginBottom: 20
    },
    viewItemsButtonText: {
        fontSize: 12,
        fontWeight: '300',
        paddingVertical: 10,
        color: '#ff555c',
        textAlign: 'center'
    },
    boldItemsText: {
        fontSize: 16,
        fontWeight: '700',
        color: '#494d5a',
        paddingVertical: 20
    },
    quantityMultiple: {
        fontSize: 12,
        paddingLeft: 40
    }
});


export default withNavigation(ViewDetailScreen);
