
import React, { useContext, useEffect } from 'react'

import { CKart } from '../../../ContextApi'
import { FontAwesome } from '@expo/vector-icons';
import { Text, View, TouchableOpacity, ScrollView, Dimensions, Image, TouchableHighlight, StyleSheet } from 'react-native';
import { Styles } from '../../../Stylesheet'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Button } from 'native-base'
import { withNavigation } from "react-navigation";

import SignInM from '../../../Common/SignInM'

const OrderListScreen = ({ navigation }) => {
    // cart_data.cart_items
    // onPress={() => removeWishlistItemApi(p.id)}


    const { order_list, base_url, ViewOrderApi, modalVisible, bottomModalFunc, signin_changes, setModalVisible, signInToken, orderListApi, signInError } = useContext(CKart)
    // console.log(order_list,'order_list-----------------')
    const { pColorOne } = Styles
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 2



    // if (signInToken !== null) {
    //     orderListApi();
    // }

    // const Login_modal = async () => {


    //     const signToken = await AsyncStorage.getItem('signToken')
    //     console.log(signToken, "signToken")
    //     console.log(signInError, "signInError")
    //     signInError === true ? bottomModalFunc("signIn") : null


    // }

    //     useEffect( () => {
    //     	Login_modal()
    // }, [signInError])



    //     <Text onPress={() => setModalVisible(true)}> hello world
    // </Text>

    return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
            <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center" }}>

                    <Text style={{ color: "#fff", fontSize: 20, fontWeight: 'bold', paddingLeft: 10 }}>OrderList</Text>

                </View>
            </View>
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View >

                        {order_list !== undefined && order_list.map(item => {
                            return (
                                <>

                                    <View key={item.id} style={styles.orderCompletedSection}>
                                        <View style={styles.dateAndStatusSection}>
                                            <Text style={styles.orderDate}>{item.order_date}</Text>
                                            <View style={styles.statusSection}>
                                                {/*<Image style={styles.whiteTick} source={item.statusImage} />*/}
                                                <Text style={styles.statusText}>{item.status_title}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.orderIdSection}>
                                            <Text>Order ID</Text>
                                            <Text>#{item.id}</Text>
                                        </View>
                                        <View style={styles.totalAmountSection}>
                                            <Text>Total Amount</Text>
                                            <Text>{currency}{' '}{item.order_total}</Text>
                                        </View>
                                        <View style={styles.totalItemsSection}>
                                            <Text>Total Items</Text>
                                            <Text>{item.order_items.length}</Text>
                                        </View>
                                        <View style={styles.buttonSecion}>
                                            <TouchableHighlight style={styles.buttonViewDetails} underlayColor="white" onPress={() => { ViewOrderApi(item.id); navigation.navigate('ViewDetailScreen') }}>
                                                <View>
                                                    <Text style={styles.buttonText}>VIEW DETAILS</Text>
                                                </View>
                                            </TouchableHighlight>
                                        </View>
                                    </View>
                                </>

                            )
                        })}
                        {order_list !== undefined && order_list.length == 0 &&

                            <View style={{ alignItems: "center", paddingTop: 20 }}>
                                <Text style={{ fontSize: 17 }}>No orders present</Text>
                                <View style={{ paddingTop: 20 }}>
                                    <Button style={{ backgroundColor: pColorOne, width: sWidth + sWidth / 4, height: 40, justifyContent: "center" }} onPress={() => { navigation.navigate('HomeScreen'); }}>
                                        <Text style={{ color: "#fff", fontSize: 18, }}>CONTINUE SHOPPING</Text>
                                    </Button>
                                </View>
                            </View>
                        }
                    </View>
                </ScrollView>
            </View>
            <SignInM />

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f6f6',
        paddingHorizontal: 15
    },
    orderInProgressText: {
        fontWeight: '500',
        color: '#494d5a',
        paddingVertical: 24
    },
    orderInProgressSection: {
        backgroundColor: '#499142',
        borderRadius: 5
    },
    orderInfoSection1: {
        paddingHorizontal: 20,
        paddingVertical: 15,
        borderBottomColor: '#ffffff',
        borderBottomWidth: 1,
        borderStyle: 'solid'
    },
    orderInfoSection1Text: {
        fontSize: 12,
        color: '#fff'
    },
    orderInfoSection2: {
        width: '100%',
        flexDirection: 'row',
        paddingHorizontal: 20,
        paddingVertical: 15,
    },
    totalAmount: {
        width: '50%'
    },
    totalItems: {
        width: '50%'
    },
    orderInfoSection2Text: {
        color: '#fff'
    },
    orderInfoSection3: {
        paddingHorizontal: 20,
        paddingBottom: 15,
    },
    textBold: {
        fontWeight: '700'
    },
    orderInfoSection4: {
        paddingHorizontal: 20,
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    orderProgressNumber: {
        width: 23,
        height: 23,
        borderRadius: 50,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#fff',
        marginRight: 5
    },
    orderProgressNumberText: {
        color: '#fff',
        fontSize: 12,
        textAlign: 'center',
        paddingTop: 2
    },
    orderInfoSection5: {
        paddingHorizontal: 20,
        paddingBottom: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    progressText1: {
        fontSize: 12,
        fontWeight: '500',
        color: '#fff',
        paddingLeft: 7,
        paddingRight: 28
    },
    progressText2: {
        fontSize: 12,
        fontWeight: '500',
        color: '#fff',
        paddingLeft: 5,
        paddingRight: 15
    },
    progressText3: {
        fontSize: 12,
        fontWeight: '500',
        color: '#fff',
        paddingLeft: 25,
        paddingRight: 20
    },
    progressText4: {
        fontSize: 12,
        fontWeight: '500',
        color: '#fff',
        paddingLeft: 10,
        paddingRight: 20
    },
    buttonSecion: {
        width: '100%',
        flexDirection: 'row'
    },
    buttons: {
        width: '50%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#499142',
        paddingVertical: 15
    },
    buttonText: {
        color: '#F26D21'
    },
    whiteTick: {
        marginHorizontal: 3
    },
    orderCompletedSection: {
        backgroundColor: '#fff',
        paddingTop: 15,
        marginBottom: 10
    },
    buttonViewDetails: {
        width: '100%',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#F26D21',
        paddingVertical: 15,
        borderRadius: 5
    },
    dateAndStatusSection: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        paddingHorizontal: 20
    },
    statusSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 18
    },
    orderDate: {
        fontWeight: '700',
        color: '#313131'
    },
    statusText: {
        color: '#F26D21',
        fontWeight: '500'
    },
    orderIdSection: {
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    totalAmountSection: {
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    totalItemsSection: {
        paddingHorizontal: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20
    }
});

export default withNavigation(OrderListScreen);



  // <Text style={styles.orderInProgressText}>Order Inprogress</Text>
  //                   <View style={styles.orderInProgressSection}>
  //                       <View style={styles.orderInfoSection1}>
  //                           <Text style={styles.orderInfoSection1Text}>Standard Delivery</Text>
  //                           <Text style={styles.orderInfoSection1Text}>Sunday 09, August | 5 PM - 8 PM</Text>
  //                       </View>
  //                       <View style={styles.orderInfoSection2}>
  //                           <View style={styles.totalAmount}>
  //                               <Text style={styles.orderInfoSection2Text}>Total Amount</Text>
  //                               <Text style={styles.orderInfoSection2Text}>₹ 107.00</Text>
  //                           </View>
  //                           <View style={styles.totalItems}>
  //                               <Text style={styles.orderInfoSection2Text}>Total Items</Text>
  //                               <Text style={styles.orderInfoSection2Text}>02</Text>
  //                           </View>
  //                       </View>
  //                       <View style={styles.orderInfoSection3}>
  //                           <Text style={styles.orderInfoSection2Text}>Delivery Address</Text>
  //                           <Text style={styles.orderInfoSection2Text}><Text style={styles.textBold}>HOME:</Text> Hennur, Kalyan Nagar, Bangalore 5600043</Text>
  //                       </View>
  //                       <View style={styles.orderInfoSection4}>
  //                           {1<=item.status  ?
		// 	                    <Icon type="FontAwesome" name="check-circle" style={{ fontSize: 10,paddingRight:8 }} />
  //                   			:
  //                               <View style={styles.orderProgressNumber}>
  //                                   <Text style={styles.orderProgressNumberText}>1</Text>
  //                               </View>
  //                           }
  //                           {2<=item.status  ?
  //                           	<Text>{'------------'}
  //                           	</Text>
  //                   			:
  //                               <Text>{'=========='}
  //                           	</Text>
  //                           }
  //                           {2<=item.status  ?
		// 	                    <Icon type="FontAwesome" name="check-circle" style={{ fontSize: 10,paddingRight:8 }} />
  //                               :
  //                               <View style={styles.orderProgressNumber}>
  //                                   <Text style={styles.orderProgressNumberText}>2</Text>
  //                               </View>
  //                           }
  //                           {3<=item.status  ?
  //                           	<Text>{'------------'}
  //                           	</Text>
  //                   			:
  //                               <Text>{'=========='}
  //                           	</Text>
  //                           }
  //                           {3<=item.status  ?
		// 	                    <Icon type="FontAwesome" name="check-circle" style={{ fontSize: 10,paddingRight:8 }} />
  //                               :
  //                               <View style={styles.orderProgressNumber}>
  //                                   <Text style={styles.orderProgressNumberText}>3</Text>
  //                               </View>
  //                           }
  //                           {4<=item.status  ?
  //                           	<Text>{'------------'}
  //                           	</Text>
  //                   			:
  //                               <Text>{'=========='}
  //                           	</Text>
  //                           }
  //                           {4<=item.status  ?
		// 	                    <Icon type="FontAwesome" name="check-circle" style={{ fontSize: 10,paddingRight:8 }} />
  //                               :
  //                               <View style={styles.orderProgressNumber}>
  //                                   <Text style={styles.orderProgressNumberText}>4</Text>
  //                               </View>
  //                           }
  //                       </View>
  //                       <View style={styles.orderInfoSection5}>
  //                           <Text style={styles.progressText1}>Order Placed</Text>
  //                           <Text style={styles.progressText2}>Packed</Text>
  //                           <Text style={styles.progressText3}>On The Way</Text>
  //                           <Text style={styles.progressText4}>Delivered</Text>
  //                       </View>
  //                       <View style={styles.buttonSecion}>
  //                           <View style={styles.buttons}>
  //                               <TouchableHighlight underlayColor="white" onPress={() => navigation.navigate('ViewDetails')}>
  //                                   <Text style={styles.buttonText}>VIEW DETAILS</Text>
  //                               </TouchableHighlight>
  //                           </View>
  //                           <View style={styles.buttons}>
  //                               <TouchableHighlight underlayColor="white" onPress={() => alert('Help')}>
  //                                   <Text style={styles.buttonText}>HELP</Text>
  //                               </TouchableHighlight>
  //                           </View>
  //                       </View>
  //                   </View>