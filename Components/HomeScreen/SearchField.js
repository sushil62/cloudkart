import React, { useContext, useState } from 'react'
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native'
import { Icon } from 'native-base'
import { withNavigation } from 'react-navigation'
import { CKart } from '../../ContextApi'
import Autocomplete from 'react-native-autocomplete-input';
import { Styles } from '../../Stylesheet'
import ListScreen from '../../Common/ListScreen'

const SearchField = ({ navigation }) => {
    const { pColorOne } = Styles
    const { handleInputBox, fetchSearchApi, search_query, suggestions } = useContext(CKart)
    const data_list = suggestions
    const [filteredData, setFilteredData] = useState([]); // Filtered data
    const [selectedValue, setSelectedValue] = useState({}); // selected data

    const findData = (query) => {
        handleInputBox(query, "search_query")
        if (query) {
            const regex = new RegExp(`${query.trim()}`, 'i');
            setFilteredData(
                data_list.filter((data) => data.product_title.search(regex) >= 0)
            );
        } else {
            setFilteredData([]);
        }
    };

    return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
            <ListScreen iconSize={20} iconType="AntDesign" iconName="left" pressFunc={() => navigation.goBack()}>
                <View style={{ backgroundColor: "#fff", flex: 1 }}>
                    <View style={{ marginHorizontal: 20, top: -15, borderRadius: 8, borderColor: "#f5f5f5" }}>
                        <View style={{ backgroundColor: "#fff", flexDirection: "row", borderRadius: 8, borderWidth: 0.3, borderColor: "#dcdcdc" }}>
                            <Autocomplete
                                autoCapitalize="none"
                                autoCorrect={false}
                                containerStyle={styles.autocompleteContainer}
                                data={filteredData}
                                defaultValue={
                                    JSON.stringify(selectedValue) === '{}' ?
                                        '' :
                                        selectedValue.product_title
                                }
                                style={{ borderRadius: 8, height: 40 }}
                                inputContainerStyle={{ borderWidth: 0, paddingHorizontal: 10 }}
                                listStyle={{ borderWidth: 0, paddingHorizontal: 5 }}
                                onChangeText={(text) => findData(text)}
                                placeholder="  Search for the product here..."
                                renderItem={({ item }) => (
                                    <TouchableOpacity
                                        onPress={() => {
                                            handleInputBox(item.product_title, "search_query");
                                            setSelectedValue(item);
                                            setFilteredData([]);
                                            fetchSearchApi(search_query);
                                            navigation.navigate('ProductListScreen');
                                        }}>
                                        <Text style={styles.itemText}>
                                            {item.product_title}
                                        </Text>
                                    </TouchableOpacity>
                                )}
                            />
                            {/* <TextInput onChangeText={(text) => handleInputBox(text, "search_query")} placeholder="Search for the product here..." style={{ backgroundColor: "#fff", padding: 5, borderRadius: 8, width: "90%" }} /> */}
                            <TouchableOpacity style={{ paddingHorizontal: 10 }} onPress={() => { fetchSearchApi(search_query); navigation.navigate('ProductListScreen') }} >
                                <Icon type="AntDesign" name="search1" style={{ fontSize: 18, color: "#999", paddingTop: 12 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ListScreen>

        </View>

    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16,
        marginTop: 40,
    },
    autocompleteContainer: {
        borderRadius: 8
    },
    descriptionContainer: {
        flex: 1,
        justifyContent: 'center',
    },
    itemText: {
        fontSize: 15,
        paddingTop: 5,
        paddingBottom: 5,
        margin: 2,
    },
    infoText: {
        textAlign: 'center',
        fontSize: 16,
    },
});

export default withNavigation(SearchField)
