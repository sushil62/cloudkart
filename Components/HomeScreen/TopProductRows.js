import React, { useContext } from 'react'
import { View, Text, ScrollView, Dimensions, TouchableOpacity, Image, ToastAndroid } from 'react-native'

import { CKart } from '../../ContextApi'
import { withNavigation } from 'react-navigation'
import { FontAwesome } from '@expo/vector-icons';

import Carousel from 'react-native-banner-carousel'



const TopProductRows = ({ navigation }) => {
  const { offerData,valid_postcode, addToCartValidation, getItmQty,cart_data, decItmQty, incItmQty, loading_true, base_url, fetchCatProductApi, fetchProductDtlApi, fetchVendorProductApi, fetchBrandProductApi, wishlist_list, addProductwishlistApi, handleRemovewishlist, signInToken, bottomModalFunc, currency } = useContext(CKart)
  const screenWidth = Math.round(Dimensions.get('window').width);
  // console.log(offerData,"offerData---------------")
  const h = Dimensions.get('window').height;
  var wishlist_items = wishlist_list.map((w) => {

    return w.vendor_product_id;
  });

  return (
    <View>
      {offerData.rows &&
        offerData.rows.map((r, index) => (
          <View key={index}>
            {r.row_data.map((d, index) => (
              <View key={index}>
                {d.page_chunk.page_chunks.length > 0 &&
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 16,
                      paddingVertical: 10,
                      marginHorizontal: 20,
                    }}>
                    {d.page_chunk.title}
                  </Text>
                }
                {d.page_chunk.chunk_type === "banner" &&

                  <Carousel>
                    {d.page_chunk.page_chunks &&
                      d.page_chunk.page_chunks.map((p, index) => {
                        return (
                          <View key={index}>
                            {p.banner_type === 'link_to_product' && (
                              <TouchableOpacity
                                onPress={() => { fetchProductDtlApi(p.vendor_product_id); navigation.navigate('ProductDetailScreen') }}

                              >

                                <Image style={{ height: screenWidth / 2.5, width: screenWidth, }} source={{ uri: base_url + p.url }} />
                              </TouchableOpacity>
                            )}
                            {p.banner_type === 'link_to_vendor_catalog' && (
                              <TouchableOpacity
                                onPress={() => { fetchVendorProductApi(p.vendor_id); navigation.navigate('ProductListScreen') }}

                              >

                                <Image style={{ height: screenWidth / 2.5, width: screenWidth, }} source={{ uri: base_url + p.url }} />
                              </TouchableOpacity>
                            )}

                            {p.banner_type === 'link_to_brand_catalog' && (
                              <TouchableOpacity
                                onPress={() => { fetchBrandProductApi(p.brand_id); navigation.navigate('ProductListScreen') }}

                              >

                                <Image style={{ height: screenWidth / 2.5, width: screenWidth, }} source={{ uri: base_url + p.url }} />
                              </TouchableOpacity>
                            )}

                            {p.banner_type === 'link_to_category_catalog' && (
                              <TouchableOpacity
                                onPress={() => { fetchCatProductApi(p.category_id); navigation.navigate('ProductListScreen') }}

                              >

                                <Image style={{ height: screenWidth / 2.5, width: screenWidth - 30, }} source={{ uri: base_url + p.url }} />
                              </TouchableOpacity>
                            )}
                          </View>
                        )
                      }

                      )}
                  </Carousel>

                }
                {d.page_chunk.chunk_type === "custom-banner" &&

                  <Carousel>
                    {d.page_chunk.page_chunks &&
                      d.page_chunk.page_chunks.map((p, index) => {
                        return (
                          <View key={index}>
                            {p.banner_type === 'link_to_product' && (
                              <TouchableOpacity
                                onPress={() => { fetchProductDtlApi(p.vendor_product_id); navigation.navigate('ProductDetailScreen') }}

                              >

                                <Image style={{ height: screenWidth / 2.5, width: screenWidth, }} source={{ uri: base_url + p.url }} />
                              </TouchableOpacity>
                            )}
                            {p.banner_type === 'link_to_custom' && (
                              <TouchableOpacity
                              // onPress={() => { fetchProductDtlApi(p.vendor_product_id);navigation.navigate('ProductDetailScreen') }} 

                              >

                                <Image style={{ height: screenWidth / 2, width: screenWidth, resizeMode: 'contain', }} source={{ uri: base_url + p.url }} />
                              </TouchableOpacity>
                            )}
                            {p.banner_type === 'link_to_vendor_catalog' && (
                              <TouchableOpacity
                                onPress={() => { fetchVendorProductApi(p.vendor_id); navigation.navigate('ProductListScreen') }}

                              >

                                <Image style={{ height: screenWidth / 2.5, width: screenWidth - 30, }} source={{ uri: base_url + p.url }} />
                              </TouchableOpacity>
                            )}

                            {p.banner_type === 'link_to_brand_catalog' && (
                              <TouchableOpacity
                                onPress={() => { fetchBrandProductApi(p.brand_id); navigation.navigate('ProductListScreen') }}

                              >

                                <Image style={{ height: screenWidth / 2.5, width: screenWidth, }} source={{ uri: base_url + p.url }} />
                              </TouchableOpacity>
                            )}

                            {p.banner_type === 'link_to_category_catalog' && (
                              <TouchableOpacity
                                onPress={() => { fetchCatProductApi(p.category_id); navigation.navigate('ProductListScreen') }}

                              >

                                <Image style={{ height: screenWidth / 2.5, width: screenWidth - 30, }} source={{ uri: base_url + p.url }} />
                              </TouchableOpacity>
                            )}
                          </View>
                        )
                      }

                      )}
                  </Carousel>

                }
                {d.page_chunk.chunk_type !== "banner" && d.page_chunk.chunk_type !== "custom-banner" &&
                  <ScrollView
                    // horizontal

                    //   showsHorizontalScrollIndicator={false}
                    style={{ marginHorizontal: 20, }}
                  >
                    <View style={{ flexDirection: 'row', flexWrap: "wrap", justifyContent: 'space-between' }}>

                      {d.page_chunk.page_chunks &&
                        d.page_chunk.page_chunks.map((p, index) => (
                          <View key={index} style={{ flexDirection: 'row', marginTop: 12 }}>
                            {p.banner_type === 'link_to_vendor_catalog' && (
                              <TouchableOpacity
                                style={{
                                  borderWidth: 1,
                                  width: 160,
                                  marginRight: 10,
                                  // marginHorizontal: 5,
                                  borderRadius: 8,
                                  borderColor: '#f5f5f5',
                                  backgroundColor: "#f5f5f5"
                                }}
                                onPress={() => { fetchVendorProductApi(p.vendor_id); loading_true(); navigation.navigate('ProductListScreen') }}
                              >
                                <View style={{ alignItems: 'center', paddingTop: 10, }}>
                                  <View style={{ padding: 12 }}>
                                    <Text style={{ marginBottom: 8, color: 'orange' }}>{p.vendor}</Text>
                                  </View>
                                  <Image

                                    source=
                                    {{ uri: base_url + p.pcb_image }}

                                    style={{ width: 160, height: 100, resizeMode: 'contain', marginTop: 10 }}
                                  />
                                </View>
                              </TouchableOpacity>
                            )}
                            {p.banner_type === 'link_to_brand_catalog' && (
                              <TouchableOpacity
                                style={{
                                  borderWidth: 1,
                                  width: 160,
                                  marginRight: 10,
                                  // marginHorizontal: 5,
                                  borderRadius: 8,
                                  borderColor: '#f5f5f5',
                                  backgroundColor: "#f5f5f5"
                                }}
                                onPress={() => { fetchBrandProductApi(p.brand_id); loading_true(); navigation.navigate('ProductListScreen') }}

                              >
                                <View style={{ alignItems: 'center', paddingTop: 10, }}>
                                  <View style={{ padding: 12 }}>
                                    <Text style={{ marginBottom: 8, color: 'orange' }}>{p.brand}</Text>
                                  </View>
                                  <Image

                                    source=
                                    {{ uri: base_url + p.pcb_image }}

                                    style={{ width: 160, height: 100, resizeMode: 'contain', marginTop: 10 }}
                                  />
                                </View>
                              </TouchableOpacity>
                            )}
                            {p.banner_type === 'link_to_category_catalog' && (
                              <TouchableOpacity
                                style={{
                                  backgroundColor: '#ffc59c',
                                  // width: 140,
                                  width: 160,

                                  // height: 160,
                                  // marginHorizontal: 12,

                                  marginRight: 6,
                                  marginBottom: 6,
                                  borderRadius: 8
                                }}
                                onPress={() => { fetchCatProductApi(p.category_id); loading_true(); navigation.navigate('ProductListScreen') }}
                              >
                                <View style={{ alignItems: 'center' }}>
                                  <View style={{ padding: 6 }}>
                                    <Text>{p.category}</Text>
                                  </View>
                                  <Image

                                    source=
                                    {{ uri: base_url + p.pcb_image }}

                                    style={{ width: 120, height: 120, resizeMode: 'contain' }}
                                  />
                                </View>


                              </TouchableOpacity>
                            )}
                            {p.banner_type === 'link_to_product' && (
                              <View
                                style={{
                                  borderWidth: 1,
                                  width: 160,
                                  marginRight: 16,
                                  borderRadius: 8,
                                  borderColor: '#f5f5f5',
                                }}


                              >
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignSelf: 'flex-end', paddingRight: 12, paddingTop: 8, }}>


                                  {wishlist_items.indexOf(p.vendor_product_id) !== -1 ?
                                    (<TouchableOpacity onPress={() => { signInToken === null ? bottomModalFunc("signIn") : handleRemovewishlist(p.vendor_product_id); }}><View style={{ backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center', padding: 4, borderRadius: 16, right: -5, top: 0, width: 30, height: 30, borderWidth: 1, borderColor: '#f5f5f5', }}><FontAwesome style={{ color: 'orange' }} name="heart" size={15} /></View></TouchableOpacity>)
                                    : (<TouchableOpacity onPress={() => { signInToken === null ? bottomModalFunc("signIn") : addProductwishlistApi(p.vendor_product_id); }}><View style={{ backgroundColor: "#fff", justifyContent: 'center', alignItems: 'center', padding: 4, borderRadius: 16, right: -5, top: 0, width: 30, height: 30, borderWidth: 1, borderColor: '#f5f5f5', }}><FontAwesome style={{ color: 'orange' }} name="heart-o" size={15} /></View></TouchableOpacity>)
                                  }



                                </View>
                                <TouchableOpacity onPress={() => { fetchProductDtlApi(p.vendor_product_id); loading_true(); navigation.navigate('ProductDetailScreen') }} >
                                  <View style={{ alignItems: 'center', paddingTop: 10, }}>
                                    <Image

                                      source=
                                      {{ uri: base_url + p.pcb_image }}

                                      style={{ width: 120, height: 120, resizeMode: 'contain', marginTop: 10 }}
                                    />
                                  </View>
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                    }}>
                                    <View
                                      style={{
                                        borderWidth: 1,
                                        padding: 2,
                                        marginLeft: 12,
                                        width: 20,
                                        borderColor: 'green',
                                      }}>
                                      <View
                                        style={{

                                          width: 12,
                                          height: 12,
                                          backgroundColor: 'green',
                                          borderRadius: 40,
                                          marginLeft: 1,

                                        }}></View>
                                    </View>

                                    {p.vp_discount_percent > 0 && <View
                                      style={{
                                        backgroundColor: 'red',
                                        width: 40,
                                        height: 20,
                                        borderTopLeftRadius: 16,
                                        borderBottomLeftRadius: 16,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                      }}>
                                      <Text style={{ color: '#fff', fontSize: 12 }}>- {p.vp_discount_percent} %</Text>
                                    </View>}
                                  </View>
                                  <View style={{ padding: 12 }}>
                                    <Text style={{ fontSize: 12, color: '#999' }}>{p.product_vendor}</Text>
                                    <Text style={{ marginBottom: 8, color: 'orange' }} numberOfLines={1} >{p.vendor_product}</Text>
                                    <View
                                      style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                      }}>
                                      <Text style={{ color: 'orange' }}> {currency}{' '}{p.vp_discount_percent > 0 ? p.vp_discounted_price : p.vp_price}</Text>
                                      {/*<Text>10kg</Text>*/}
                                    </View>
                                  </View>
                                  <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 25, marginVertical: 10, alignItems: "center" }}>
                                    <TouchableOpacity onPress={() => decItmQty(p.product_slug)} style={{ borderRadius: 5, width: 26, height: 26, backgroundColor: "#F26D21", alignItems: "center", justifyContent: "center" }}>
                                      <Text style={{ color: "#fff", }}>-</Text>
                                    </TouchableOpacity>
                                    <Text>{getItmQty(p.product_slug) ? getItmQty(p.product_slug) : 1}</Text>
                                    <TouchableOpacity onPress={() => incItmQty(p.product_slug)} style={{ borderRadius: 5, width: 26, height: 26, backgroundColor: "#F26D21", alignItems: "center", justifyContent: "center" }}>
                                      <Text style={{ color: "#fff", }}>+</Text>
                                    </TouchableOpacity>
                                  </View>
                                  <TouchableOpacity style={{ backgroundColor: "#F26D21", borderRadius: 8, alignItems: "center", justifyContent: "center", margin: 5, }} onPress={() => { valid_postcode === undefined ? bottomModalFunc("zipcode") : addToCartValidation(p.vendor_id, p.vendor_product_id, p.product_slug, getItmQty(p.product_slug) ? getItmQty(p.product_slug) : 1); }}>
                                    <Text style={{ color: '#fff', fontSize: 14, padding: 7, }}>Add To Cart</Text>
                                  </TouchableOpacity>
                                </TouchableOpacity>
                                {cart_data !== undefined && cart_data.cart_items !== undefined  && cart_data.cart_items.map(c => c.vendor_product.id === p.vendor_product_id? 
            <View style={{ alignItems: "center", justifyContent: "center", margin: 5,}}><Text style={{color:"green",fontSize:16}}>{c.quantity} in basket</Text></View> : null
              )  }
                              </View>
                            )}
                          </View>

                        ))}
                    </View>
                  </ScrollView>
                }
              </View>
            ))}
          </View>
        ))}
    </View>

  )
}

export default withNavigation(TopProductRows)



//                                         flexDirection: 'row',
//                                         justifyContent: 'space-between',
//                                         alignItems: 'center',
//                                       }}>
//                                        <View
//                                       style={{
//                                           borderWidth: 1,
//                                           padding:2,
//                                           marginLeft: 12,
//                                           width: 20,
//                                           borderColor: 'green',   
//                                       }}>
//                                       <View
//                                         style={{
//                                           width: 12,
//                                           height: 12,
//                                           backgroundColor: 'green',
//                                           borderRadius: 40,
//                                           marginLeft: 1,

//                                         }}></View>
//                                         </View>

//                                       <View
//                                         style={{
//                                           backgroundColor: 'red',
//                                           width: 40,
//                                           height: 20,
//                                           borderTopLeftRadius: 16,
//                                           borderBottomLeftRadius: 16,
//                                           alignItems: 'center',
//                                           justifyContent: 'center',
//                                         }}>
//                                         <Text style={{ color: '#fff', fontSize: 12 }}>- {p.vp_discount_percent} %</Text>
//                                       </View>
//                                     </View>
//                                     <View style={{ padding: 12 }}>
//                                       <Text style={{ fontSize: 12, color: '#999' }}>{p.product_vendor}</Text>
//                                       <Text style={{ marginBottom: 8 }}>{p.vendor_product}</Text>
//                                       <View
//                                         style={{
//                                           flexDirection: 'row',
//                                           justifyContent: 'space-between',
//                                         }}>
//                                         <Text style={{ color: 'orange' }}> {currency}{' '}{p.vp_price}</Text>
//                                         {/*<Text>10kg</Text>*/}
//                                       </View>
//                                     </View>
