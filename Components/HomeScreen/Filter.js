import React, { useContext, useState } from 'react'
import { View, Text, TouchableOpacity, Dimensions, ScrollView } from 'react-native'
import Br from '../../Common/Br'
import { CKart } from '../../ContextApi'
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Button, CheckBox } from 'native-base'
import { Styles } from '../../Stylesheet'
import ScrollAbleMenu from './ScrollAbleMenu'
import SearchBox from './SearchBox'
import ProductAndOffer from './ProductAndOffer'
import { withNavigation } from "react-navigation";

const Filter = ({ navigation }) => {

    const { facetednav, product_count, filterDataFunc, filterId, filterValue, filterType, setFilterData, filterData, resetFilter, fatchFilteredDataApi } = useContext(CKart)
    const menuItems = [
        { id: '1', name: 'Price', type: "price_range_facet", value: facetednav.price_range_facet },
        { id: '2', name: 'Category', type: "category", value: facetednav.category },
        { id: '3', name: 'Brand', type: "brand", value: facetednav.brand },
        // { id: '4', name: 'Vendor', type: "vendor", value: facetednav.vendor },
        { id: '5', name: 'Availability', type: "availability_facet", value: facetednav.availability_facet },
        { id: '6', name: 'Discount', type: "discount_percent_facet", value: facetednav.discount_percent_facet },
        { id: '7', name: 'Customer Rating', type: "review_rating_facet", value: facetednav.review_rating_facet },
        // { id: '8', name: 'Sort', type: "sort_facet", value: facetednav.sort_facet },
    ]
    const { pColorOne } = Styles
    // console.log(facetednav, "facetednav")
    const { width, height } = Dimensions.get('window')
    const sWidth = width / 2 - (width / 20)
    return (
        <View style={{ backgroundColor: pColorOne, flex: 1, paddingTop: 50 }}>
            <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                <View style={{ marginBottom: 12, marginHorizontal: 20, flexDirection: "row", alignItems: "center" }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon type="MaterialIcons" name="arrow-back" style={{ color: "#fff", fontSize: 20 }} />
                    </TouchableOpacity>
                    <Text style={{ color: "#fff", paddingLeft: 12, fontSize: 16, fontWeight: "700" }}>Filter</Text>
                </View>
                <View style={{ paddingRight: 10 }}>
                    <Text style={{ color: "#fff", fontSize: 14, textAlign: "right" }}>{product_count}</Text>
                    <Text style={{ color: "#fff", fontSize: 10 }}>Products Found</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', flex: 1, marginTop: 10, backgroundColor: "white", justifyContent: "space-between" }}>
                <ScrollView style={{ width: width / 3 }}>
                    {menuItems.map(
                        (item, index) => {
                            return (
                                <TouchableOpacity key={item.id} onPress={() => filterDataFunc(item.id, item.value, item.type)}
                                    style={{
                                        flex: 0,
                                        height: 50,
                                        paddingLeft: 25,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        backgroundColor: item.id === filterId ? "#dcdcdc" : null
                                    }}>
                                    <Text style={{
                                        alignSelf: 'flex-start',
                                        fontSize: 15,
                                        color: item.id === filterId ? pColorOne : null
                                    }}>
                                        {item.name}

                                    </Text>
                                </TouchableOpacity>
                            )
                        }
                    )}
                </ScrollView>
                <ScrollView style={{ padding: 10, }}>
                    {filterValue.map(val => {
                        return (
                            <TouchableOpacity style={{ flexDirection: "row", paddingHorizontal: 5, paddingVertical: 10 }} onPress={() => setFilterData(filterType, val)}>
                                <CheckBox onPress={() => setFilterData(filterType, val)} style={{ borderColor: "black", backgroundColor: filterData[filterType] && filterData[filterType].includes(val) ? "black" : "#fff" }} checked={filterData[filterType] && filterData[filterType].includes(val)} />
                                <Text style={{ paddingHorizontal: 20, }}>
                                    {val.name}
                                    {val.title}
                                    {val.text}
                                    {filterType === 'price_range_facet' && `${val["min-price"]} - ${val["max-price"]}`}
                                </Text>
                            </TouchableOpacity>
                        )
                    })}
                </ScrollView>
            </View>

            <View style={{ backgroundColor: "white", borderTopColor: '#999', borderTopWidth: 1, }}>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", borderTopWidth: 0, marginVertical: 10, marginHorizontal: 15 }}>
                    <Button onPress={() => resetFilter()} style={{ backgroundColor: pColorOne, width: sWidth, height: 40, justifyContent: "center" }}>
                        <Text style={{ fontSize: 13, }}>RESET</Text>
                    </Button>
                    <Button onPress={() => { fatchFilteredDataApi(); navigation.navigate('ProductListScreen') }} style={{ backgroundColor: pColorOne, width: sWidth, height: 40, justifyContent: "center" }}>
                        <Text style={{ fontSize: 13, }}>APPLY</Text>
                    </Button>
                </View>
            </View>
        </View>
    )
}



export default withNavigation(Filter);
